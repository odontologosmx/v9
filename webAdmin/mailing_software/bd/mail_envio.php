<?php
include ("../../includes/inicio.php");
include ("../../includes/config.php");
?>
<?php
include ("modelo.php");
include ("modeloMailing.php");
$obj = new modelo();
$objMailing = new modeloMailing();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">
    <title></title>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
      <?php
      /* CONFIGURACIONES GENERALES */
      $origen_nombre = 'Todo Pets';
      $email = 'contacto@todopets.mx';
      $subject='Últimas novedades de Pipo ¡Consejos y entretenimiento para ti y tus mascotas!';
      $fecha_envio=$_GET['fecha_envio'];

        if ($fecha_envio!='') {
            $mail_results=$obj->getEmails();
            //var_dump($mail_results);
            echo "Enviando recordatorio de la sesión: ".$subject.'<br>';
            foreach ($mail_results as $mail_results) {
              $destino=$mail_results['email'];
              //$destino = 'hcardosot@odontologos.mx';
              $emails_destino.=$destino.'<br>';

              /* ARCHIVO DE RECORDATORIO */
              ob_start();
              $file="../../boletines/".$fecha_envio.".php";
              include($file);
              $salida = ob_get_clean();
              //var_dump($salida);
              $message = $salida;

              $envio_mail=$objMailing->enviar_email($origen_nombre, $email, $subject, $destino, $message, 'SMTP', false);
              $emails_enviados+=1;
            }

          echo "EMAILS DESTINO:<br>".$emails_destino;
          echo "EMAILS ENVIADOS: ".$emails_enviados;

          /* NOTIFICACION DE FINALIZADO A CORREO */
          $destino = 'hcardosot@todopets.mx';
          $subject = 'Mailing Finalizado';
          $message = 'Se ha enviado mailing: '.$subject.'<br>';
          $message.= 'Envío: '.$fecha_envio.'<br>';
          $message .= 'Se enviaron '.$emails_enviados.' emails<br>';
          $message .= $emails_destino;
          if(count($mail_results)>1)
            $envio_mail=$objMailing->enviar_email($origen_nombre, $email, $subject, $destino, $message, 'SMTP', false);
        } else {
          echo "NO HAY NADA QUE ENVIAR, FUERA DE HORARIO O PARÁMETROS INVÁLIDOS";
        }
      ?>
    </div>
  </body>
</html>
