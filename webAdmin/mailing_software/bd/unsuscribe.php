<?php
include ("../../includes/inicio.php");
include ("../../includes/config.php");
?>
<?php
include ("modelo.php");
include ("modeloMailing.php");
$obj = new modelo();
$objMailing = new modeloMailing();
$email=$_GET['email'];
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">
    <title></title>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <? include ("../../includes/tags_styles.php"); ?>
    </head>
  <body>
    <?php include ('../../header.php');?>

    <div class="container">
      <?php $unsuscribe=$obj->unsuscribeEmail($email); //echo 'UNSUSCRIBE: '.$unsuscribe;?>
      <?php if ($unsuscribe=='1') { ?>
        <h1 style="margin:150px 0px 100px 0px;" class="text-center">El correo <?=$email?> se ha dado de baja correctamente</h1>
      <?php } else { ?>
        <h1 style="margin:100px 0px;" class="text-center">El correo <?=$email?> NO  se pudo dar de baja, contáctanos</h1>
      <?php } ?>

    </div>

    <?php $fondo_footer='img-footer'; include ('../../footer.php');?>
  </body>
</html>
