
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<!-- NAME: 1:2 COLUMN -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Boletín Semanal | TodoPets</title>

    <style type="text/css">
		body,#bodyTable,#bodyCell{
			height:100% !important;
			margin:0;
			padding:0;
			width:100% !important;
		}
		table{
			border-collapse:collapse;
		}
		img,a img{
			border:0;
			outline:none;
			text-decoration:none;
		}
		h1,h2,h3,h4,h5,h6{
			margin:0;
			padding:0;
		}
		p{
			margin:1em 0;
			padding:0;
		}
		a{
			word-wrap:break-word;
		}
		.mcnPreviewText{
			display:none !important;
		}
		.ReadMsgBody{
			width:100%;
		}
		.ExternalClass{
			width:100%;
		}
		.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{
			line-height:100%;
		}
		table,td{
			mso-table-lspace:0pt;
			mso-table-rspace:0pt;
		}
		#outlook a{
			padding:0;
		}
		img{
			-ms-interpolation-mode:bicubic;
		}
		body,table,td,p,a,li,blockquote{
			-ms-text-size-adjust:100%;
			-webkit-text-size-adjust:100%;
		}
		#templatePreheader,#templateHeader,#templateBody,#templateColumns,.templateColumn,#templateFooter{
			min-width:100%;
		}
		#bodyCell{
			padding:20px;
		}
		.mcnImage,.mcnRetinaImage{
			vertical-align:bottom;
		}
		.mcnTextContent img{
			height:auto !important;
		}
		body,#bodyTable{
			background-color:#ececec;
		}
		#bodyCell{
			border-top:0;
		}
		#templateContainer{
			border:0;
		}
		h1{
			color:#606060 !important;
			display:block;
			font-family:Helvetica;
			font-size:40px;
			font-style:normal;
			font-weight:bold;
			line-height:125%;
			letter-spacing:-1px;
			margin:0;
			text-align:left;
		}
		h2{
			color:#404040 !important;
			display:block;
			font-family:Helvetica;
			font-size:26px;
			font-style:normal;
			font-weight:bold;
			line-height:125%;
			letter-spacing:-.75px;
			margin:0;
			text-align:left;
		}
		h3{
			color:#606060 !important;
			display:block;
			font-family:Helvetica;
			font-size:15px;
			font-style:normal;
			font-weight:normal;
			line-height:125%;
			letter-spacing:-.5px;
			margin:0;
			text-align:left;
		}
		h4{
			color:#808080 !important;
			display:block;
			font-family:Helvetica;
			font-size:16px;
			font-style:normal;
			font-weight:bold;
			line-height:125%;
			letter-spacing:normal;
			margin:0;
			text-align:left;
		}
		#templatePreheader{
			background-color:#ff5003;
			border-top:0;
			border-bottom:0;
		}
		.preheaderContainer .mcnTextContent,.preheaderContainer .mcnTextContent p{
			color:#ffffff;
			font-family:Helvetica;
			font-size:12px;
			line-height:125%;
			text-align:left;
		}
		.preheaderContainer .mcnTextContent a{
			color:#ffffff;
			font-weight:normal;
			text-decoration:underline;
		}
		#templateHeader{
			background-color:#f2f2f2;
			border-top:0;
			border-bottom:5px solid #3f2a83;
		}
		.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
			color:#606060;
			font-family:Helvetica;
			font-size:15px;
			line-height:150%;
			text-align:left;
		}
		.headerContainer .mcnTextContent a{
			color:#008ad0;
			font-weight:normal;
			text-decoration:underline;
		}
		#templateBody{
			background-color:#ffffff;
			border-top:0;
			border-bottom:0;
		}
		.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
			color:#606060;
			font-family:Helvetica;
			font-size:16px;
			line-height:125%;
			text-align:left;
		}
		.bodyContainer .mcnTextContent a{
			color:#117dbb;
			font-weight:normal;
			text-decoration:underline;
		}
		#templateColumns{
			background-color:#FFFFFF;
			border-top:0;
			border-bottom:0;
		}
		.leftColumnContainer .mcnTextContent,.leftColumnContainer .mcnTextContent p{
			color:#606060;
			font-family:Helvetica;
			font-size:15px;
			line-height:150%;
			text-align:left;
		}
		.leftColumnContainer .mcnTextContent a{
			color:#008ad0;
			font-weight:normal;
			text-decoration:underline;
		}
		.rightColumnContainer .mcnTextContent,.rightColumnContainer .mcnTextContent p{
			color:#606060;
			font-family:Helvetica;
			font-size:15px;
			line-height:150%;
			text-align:left;
		}
		.rightColumnContainer .mcnTextContent a{
			color:#008ad0;
			font-weight:normal;
			text-decoration:underline;
		}
		#templateFooter{
			background-color:#e0a839;
			border-top:0;
			border-bottom:0;
		}
		.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
			color:#ffffff;
			font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;
			font-size:11px;
			line-height:125%;
			text-align:center;
		}
		.footerContainer .mcnTextContent a{
			color:#ffffff;
			font-weight:normal;
			text-decoration:underline;
		}
	@media only screen and (max-width: 480px){
		body,table,td,p,a,li,blockquote{
			-webkit-text-size-adjust:none !important;
		}

}	@media only screen and (max-width: 480px){
		body{
			width:100% !important;
			min-width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnRetinaImage{
			max-width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		td[id=bodyCell]{
			padding:10px !important;
		}

}	@media only screen and (max-width: 480px){
		table[class=mcnTextContentContainer]{
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnBoxedTextContentContainer{
			max-width:100% !important;
			min-width:100% !important;
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		table[class=mcpreview-image-uploader]{
			width:100% !important;
			display:none !important;
		}

}	@media only screen and (max-width: 480px){
		img[class=mcnImage]{
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		table[class=mcnImageGroupContentContainer]{
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnImageGroupContent]{
			padding:9px !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnImageGroupBlockInner]{
			padding-bottom:0 !important;
			padding-top:0 !important;
		}

}	@media only screen and (max-width: 480px){
		tbody[class=mcnImageGroupBlockOuter]{
			padding-bottom:9px !important;
			padding-top:9px !important;
		}

}	@media only screen and (max-width: 480px){
		table[class=mcnCaptionTopContent],table[class=mcnCaptionBottomContent]{
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		table[class=mcnCaptionLeftTextContentContainer],table[class=mcnCaptionRightTextContentContainer],table[class=mcnCaptionLeftImageContentContainer],table[class=mcnCaptionRightImageContentContainer],table[class=mcnImageCardLeftTextContentContainer],table[class=mcnImageCardRightTextContentContainer],.mcnImageCardLeftImageContentContainer,.mcnImageCardRightImageContentContainer{
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnImageCardLeftImageContent],td[class=mcnImageCardRightImageContent]{
			padding-right:18px !important;
			padding-left:18px !important;
			padding-bottom:0 !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnImageCardBottomImageContent]{
			padding-bottom:9px !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnImageCardTopImageContent]{
			padding-top:18px !important;
		}

}	@media only screen and (max-width: 480px){
		table[class=mcnCaptionLeftContentOuter] td[class=mcnTextContent],table[class=mcnCaptionRightContentOuter] td[class=mcnTextContent]{
			padding-top:9px !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnCaptionBlockInner] table[class=mcnCaptionTopContent]:last-child td[class=mcnTextContent],.mcnImageCardTopImageContent,.mcnCaptionBottomContent:last-child .mcnCaptionBottomImageContent{
			padding-top:18px !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnBoxedTextContentColumn]{
			padding-left:18px !important;
			padding-right:18px !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=columnsContainer]{
			display:block !important;
			max-width:600px !important;
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=mcnTextContent]{
			padding-right:18px !important;
			padding-left:18px !important;
		}

}	@media only screen and (max-width: 480px){
		table[id=templateContainer],table[id=templatePreheader],table[id=templateHeader],table[id=templateColumns],table[class=templateColumn],table[id=templateBody],table[id=templateFooter]{
			max-width:600px !important;
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		h1{
			font-size:24px !important;
			line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
		h2{
			font-size:20px !important;
			line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
		h3{
			font-size:18px !important;
			line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
		h4{
			font-size:16px !important;
			line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
		table[class=mcnBoxedTextContentContainer] td[class=mcnTextContent],td[class=mcnBoxedTextContentContainer] td[class=mcnTextContent] p{
			font-size:18px !important;
			line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
		table[id=templatePreheader]{
			display:block !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=preheaderContainer] td[class=mcnTextContent],td[class=preheaderContainer] td[class=mcnTextContent] p{
			font-size:14px !important;
			line-height:115% !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=headerContainer] td[class=mcnTextContent],td[class=headerContainer] td[class=mcnTextContent] p{
			font-size:18px !important;
			line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=bodyContainer] td[class=mcnTextContent],td[class=bodyContainer] td[class=mcnTextContent] p{
			font-size:18px !important;
			line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=leftColumnContainer] td[class=mcnTextContent],td[class=leftColumnContainer] td[class=mcnTextContent] p{
			font-size:18px !important;
			line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=rightColumnContainer] td[class=mcnTextContent],td[class=rightColumnContainer] td[class=mcnTextContent] p{
			font-size:18px !important;
			line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=footerContainer] td[class=mcnTextContent],td[class=footerContainer] td[class=mcnTextContent] p{
			font-size:14px !important;
			line-height:115% !important;
		}

}	@media only screen and (max-width: 480px){
		td[class=footerContainer] a[class=utilityLink]{
			display:block !important;
		}

}</style><!--[if !mso]><!--><link href="https://fonts.googleapis.com/css?family=Arvo:400,400i,700,700i|Cabin:400,400i,700,700i|Catamaran:400,400i,700,700i|Hind+Guntur:400,400i,700,700i|Karla:400,400i,700,700i|Lato:400,400i,700,700i|Lora:400,400i,700,700i|Merriweather:400,400i,700,700i|Merriweather+Sans:400,400i,700,700i|Noticia+Text:400,400i,700,700i|Open+Sans:400,400i,700,700i|Playfair+Display:400,400i,700,700i|Roboto:400,400i,700,700i|Rubik:400,400i,700,700i|Source+Sans+Pro:400,400i,700,700i|Source+Serif+Pro:400,400i,700,700i|Work+Sans:400,400i,700,700i" rel="stylesheet"><!--<![endif]--><link rel="stylesheet" type="text/css" href="https://us7.admin.mailchimp.com/release/1.1.1409ec6f2cf648b254912315af394209e147f8f4a/css/stable/template-editor.css" />
<style type="text/css">/* Enable image placeholders */@-moz-document url-prefix(http), url-prefix(file) { img:-moz-broken{-moz-force-broken-image-icon:1; width:24px;height:24px;
}
}</style></head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
        <!--*|IF:MC_PREVIEW_TEXT|*-->
        <!--[if !gte mso 9]><!----><span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">*|MC_PREVIEW_TEXT|*</span><!--<![endif]-->
        <!--*|END:IF|*-->
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top" id="bodyCell">
                        <!-- BEGIN TEMPLATE // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN PREHEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templatePreheader">
                                        <tr>
                                        	<td valign="top" class="preheaderContainer tpl-container" style="padding-top:9px;" mc:container="preheader_container" mccontainer="preheader_container"><div mc:block="2706167" mc:blocktype="text" mcblock="2706167" mcblocktype="text" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->

				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>

                        <td valign="top" class="mcnTextContent" style="padding: 0px 18px 9px; text-align: center;">

                            <a href="http://www.todopets.mx/boletines/2018-09-12.php" target="_blank">Ver este correo en mi navegador</a>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->

				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></div></td>
                                        </tr>
                                    </table>
                                    <!-- // END PREHEADER -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">
                                        <tr>
                                            <td valign="top" class="headerContainer tpl-container" mc:container="header_container" mccontainer="header_container"><div mc:block="2706163" mc:blocktype="image" mcblock="2706163" mcblocktype="image" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td valign="top" style="padding:0px" class="mcnImageBlockInner">
                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
                        <tbody><tr>
                            <td class="mcnImageContent" valign="top" style="padding-right: 0px; padding-left: 0px; padding-top: 0; padding-bottom: 0; text-align:center;">

                                    <a href="http://todopets.mx/" title="" class="" target="_blank">
                                        <img align="center" alt="" src="https://gallery.mailchimp.com/5b140bc2eac72b4028ac10f26/images/1b7c65b3-c54c-4956-88f3-7e1a376e1e0f.jpg" width="600" style="max-width:950px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
                                    </a>

                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></div></td>
                                        </tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">
                                        <tr>
                                            <td valign="top" class="bodyContainer tpl-container" mc:container="body_container" mccontainer="body_container"><div mc:block="2706171" mc:blocktype="divider" mcblock="2706171" mcblocktype="divider" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px solid #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706139" mc:blocktype="image" mcblock="2706139" mcblocktype="image" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td valign="top" style="padding:9px" class="mcnImageBlockInner">
                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
                        <tbody><tr>
                            <td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">

                                    <a href="http://www.todopets.mx/calendario/11/1er-encuentro-de-acuarios-el-acuario-de-veracruz-en-el-acuario-inbursa" title="" class="" target="_blank">
                                        <img align="center" alt="" src="https://gallery.mailchimp.com/5b140bc2eac72b4028ac10f26/images/8739b879-67ab-4aaa-96b3-902a069984de.jpg" width="564" style="max-width:1200px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
                                    </a>

                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></div><div mc:block="2706183" mc:blocktype="text" mcblock="2706183" mcblocktype="text" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->

				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>

                        <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody>
		<tr>
			<td style="text-align: center;" valign="top"><a href="http://www.todopets.mx/calendario/11/1er-encuentro-de-acuarios-el-acuario-de-veracruz-en-el-acuario-inbursa" target="_blank"><span style="font-size:18px"><strong>1er Encuentro de acuarios. <br>El acuario de Veracruz en el acuario Inbursa</strong></span></a><br>
			&nbsp;</td>
		</tr>
	</tbody>
</table>

                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->

				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706147" mc:blocktype="button" mcblock="2706147" mcblocktype="button" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock" style="min-width:100%;">
     <tbody class="mcnButtonBlockOuter">
        <tr>
            <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" valign="top" align="center" class="mcnButtonBlockInner">
                <a href="http://www.todopets.mx/calendario/11/1er-encuentro-de-acuarios-el-acuario-de-veracruz-en-el-acuario-inbursa" target="_blank"> <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 5px;background-color: #202B67;">
                    <tbody>
                        <tr>
                            <td align="center" valign="middle" class="mcnButtonContent" style="font-family: Arial; font-size: 16px; padding: 8px;">
                                <span class="mcnButton " title="Ver más información" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Ver más información</span>
                            </td>
                        </tr>
                    </tbody>
                </table></a>
            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706499" mc:blocktype="button" mcblock="2706499" mcblocktype="button" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock" style="min-width:100%;">
    <tbody class="mcnButtonBlockOuter">
        <tr>
            <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" valign="top" align="center" class="mcnButtonBlockInner">
                <a href="https://www.encuentrodeacuarios.com/" target="_blank"> <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 5px;background-color: #2971CF;">
                    <tbody>
                        <tr>
                            <td align="center" valign="middle" class="mcnButtonContent" style="font-family: Arial; font-size: 16px; padding: 8px;">
                                <a class="mcnButton " title="Visita nuestro sitio web" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Visita nuestro sitio web</a>
                            </td>
                        </tr>
                    </tbody>
                </table></a>
            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706187" mc:blocktype="divider" mcblock="2706187" mcblocktype="divider" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px solid #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706203" mc:blocktype="text" mcblock="2706203" mcblocktype="text" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->

				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>

                        <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                            <h1 class="null" style="text-align: center;"><span style="font-size:27px"><span style="font-family:open sans,helvetica neue,helvetica,arial,sans-serif">- EVENTOS -</span></span></h1>

                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->

				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706179" mc:blocktype="divider" mcblock="2706179" mcblocktype="divider" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px solid #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706459" mc:blocktype="image" mcblock="2706459" mcblocktype="image" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td valign="top" style="padding:9px" class="mcnImageBlockInner">
                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
                        <tbody><tr>
                            <td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">

                                    <a href="http://www.todopets.mx/calendario/15/3er-exposicion-internacional-felina" title="" class="" target="_blank">
                                        <img align="center" alt="" src="https://gallery.mailchimp.com/5b140bc2eac72b4028ac10f26/_compresseds/6147ac40-f765-41cc-8b5b-898da448b8ea.jpg" width="564" style="max-width:750px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
                                    </a>

                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></div><div mc:block="2706467" mc:blocktype="text" mcblock="2706467" mcblocktype="text" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->

				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>

                        <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                            <h4 class="null" style="text-align: center;"><a href="http://www.todopets.mx/calendario/15/3er-exposicion-internacional-felina" target="_blank">3er Exposición Internacional Felina</a></h4>

<h3 style="text-align: center;"><span style="font-size:15px"><span style="font-family:arial,helvetica neue,helvetica,sans-serif">Fecha: 29 a&nbsp;30 de Septiembre de 2018</span></span></h3>

<h3 style="text-align: center;">Sede:&nbsp;<a href="https://www.google.com/maps/search/?api=1&amp;query=19.0493165,-98.2200061&amp;query_place_id=ChIJO32bUNrAz4UR7RcPsXcfeUU" target="_blank">Centro de Vinculación UPAEP</a></h3>
&nbsp;

<div style="text-align: center;"><a href="http://www.todopets.mx/calendario/15/3er-exposicion-internacional-felina" target="_blank">Ver más información</a></div>

<div style="text-align: center;"><a href="https://www.ticademexico.org/" target="_blank">Visita nuestro sitio web</a></div>

<div style="text-align: center;">&nbsp;</div>

                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->

				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706463" mc:blocktype="divider" mcblock="2706463" mcblocktype="divider" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px solid #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706207" mc:blocktype="imageCaption" mcblock="2706207" mcblocktype="imageCaption" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock">
    <tbody class="mcnCaptionBlockOuter">
        <tr>
            <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;">


<table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionBottomContent" width="282">
    <tbody><tr>
        <td class="mcnCaptionBottomImageContent" align="center" valign="top" style="padding:0 9px 9px 9px;">


            <a href="http://www.todopets.mx/calendario/12/curso-de-lenguaje-canino" title="" class="" target="_blank">


            <img alt="" src="https://gallery.mailchimp.com/5b140bc2eac72b4028ac10f26/images/87a92285-e05f-457c-b700-fb07e85198ff.jpg" width="250" style="max-width:250px;" class="mcnImage">
            </a>

        </td>
    </tr>
    <tr>
        <td class="mcnTextContent" valign="top" style="padding:0 9px 0 9px;" width="282">
            <h4 class="null"><a href="http://www.todopets.mx/calendario/12/curso-de-lenguaje-canino" target="_blank">Curso de Lenguaje Canino</a></h4>

<h3>Fecha: 23 de Septiembre de 2018</h3>

<h3><span class="mc-toc-title">Horario: 11:00 a&nbsp;13:00 hrs</span></h3>

<h3>Sede:&nbsp;<a href="http://maps.google.com/maps?ll=19.2628881,-99.5669158" target="_blank">Plaza El Castaño</a></h3>
<br>
<a href="http://www.todopets.mx/calendario/12/curso-de-lenguaje-canino" target="_blank">Ver más información</a><br>
<a href="http://fundaciontomy.org/" target="_blank">Visita nuestro sitio web</a>
        </td>
    </tr>
</tbody></table>

<table align="right" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionBottomContent" width="282">
    <tbody><tr>
        <td class="mcnCaptionBottomImageContent" align="center" valign="top" style="padding:0 9px 9px 9px;">


            <a href="http://www.todopets.mx/calendario/13/gran-feria-gigante" title="" class="" target="_blank">


            <img alt="" src="https://gallery.mailchimp.com/5b140bc2eac72b4028ac10f26/images/4618cb03-9cc2-4b8a-aa4a-741effdb6963.jpg" width="250" style="max-width:250px;" class="mcnImage">
            </a>

        </td>
    </tr>
    <tr>
        <td class="mcnTextContent" valign="top" style="padding:0 9px 0 9px;" width="282">
            <h4 class="null"><a href="http://www.todopets.mx/calendario/13/gran-feria-gigante" target="_blank">Feria de Adopción de razas grandes.</a></h4>

<h3>Fecha: 23 de Septiembre de 2018</h3>

<h3>Horario: 10:00 a&nbsp;14:00 hrs</h3>

<h3>Sede:&nbsp;<a href="http://maps.google.com/maps?ll=19.2498311,-99.5821143" target="_blank">Parque Bicentenario</a></h3>
<br>
<a href="http://www.todopets.mx/calendario/13/gran-feria-gigante" target="_blank">Ver más información</a><br>
<a href="http://fundaciontomy.org/" target="_blank">Visita nuestro sitio web</a><br>
&nbsp;
        </td>
    </tr>
</tbody></table>





            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706231" mc:blocktype="divider" mcblock="2706231" mcblocktype="divider" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px solid #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706227" mc:blocktype="image" mcblock="2706227" mcblocktype="image" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td valign="top" style="padding:9px" class="mcnImageBlockInner">
                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
                        <tbody><tr>
                            <td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">

                                    <a href="http://todopets.mx/directorio/?url=directorio" title="" class="" target="_blank">
                                        <img align="center" alt="" src="https://gallery.mailchimp.com/5b140bc2eac72b4028ac10f26/_compresseds/c60f0c2b-fa1f-4e80-815b-3877863d8527.jpg" width="564" style="max-width:750px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
                                    </a>

                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></div><div mc:block="2706211" mc:blocktype="divider" mcblock="2706211" mcblocktype="divider" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px solid #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706223" mc:blocktype="text" mcblock="2706223" mcblocktype="text" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->

				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>

                        <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                            <h1 class="null" style="text-align: center;"><span style="font-size:27px"><span style="font-family:open sans,helvetica neue,helvetica,arial,sans-serif">- SECCIÓN DIRECTORIO -</span></span></h1>

                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->

				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706247" mc:blocktype="divider" mcblock="2706247" mcblocktype="divider" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px solid #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706243" mc:blocktype="imageCaption" mcblock="2706243" mcblocktype="imageCaption" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock">
    <tbody class="mcnCaptionBlockOuter">
        <tr>
            <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;">


<table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionBottomContent" width="282">
    <tbody><tr>
        <td class="mcnCaptionBottomImageContent" align="center" valign="top" style="padding:0 9px 9px 9px;">


            <a href="http://www.todopets.mx/fundacion-tomy" title="" class="" target="_blank">


            <img alt="" src="https://gallery.mailchimp.com/5b140bc2eac72b4028ac10f26/images/fdbf608c-e380-4d3e-930a-d9d8ca746805.jpg" width="250" style="max-width:250px;" class="mcnImage">
            </a>

        </td>
    </tr>
    <tr>
        <td class="mcnTextContent" valign="top" style="padding:0 9px 0 9px;" width="282">
            <h4 class="null"><a href="http://www.todopets.mx/fundacion-tomy" target="_blank">Fundación Tomy</a></h4>
&nbsp;

<div style="text-align: left;">Fundación Tomy es un vinculo donde cada persona transmita y actue con un buen ejemplo hacia los animales, esperamos ser utiles en tu busqueda y que a la vez juntos podamos desarrollar algo importante para la sociedad y nuestros animales.<br>
<br>
<a href="http://www.todopets.mx/fundacion-tomy" target="_blank">Ver más información</a></div>

        </td>
    </tr>
</tbody></table>

<table align="right" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionBottomContent" width="282">
    <tbody><tr>
        <td class="mcnCaptionBottomImageContent" align="center" valign="top" style="padding:0 9px 9px 9px;">


            <a href="http://www.todopets.mx/Ankara-Kirov" title="" class="" target="_blank">


            <img alt="" src="https://gallery.mailchimp.com/5b140bc2eac72b4028ac10f26/_compresseds/2a38dc05-190f-4071-b6d3-e960af43fa1e.jpg" width="250" style="max-width:250px;" class="mcnImage">
            </a>

        </td>
    </tr>
    <tr>
        <td class="mcnTextContent" valign="top" style="padding:0 9px 0 9px;" width="282">
            <a href="http://www.todopets.mx/Ankara-Kirov" target="_blank">Criadero. Gatos Angora Turco de Ankara Kirov.</a><br>
<br>
Somos un criadero&nbsp;ubicado en Guadalajara Jalisco Mexico, El Angora Turco es una raza exclusiva y emblemática&nbsp;de&nbsp;su país natal Turquía, es el origen de otras razas de gatos domésticos de pelo largo y semi largo, un gato natural sin intervención del hombre...<br>
<br>
<a href="http://www.todopets.mx/Ankara-Kirov" target="_blank">Ver más información</a>
        </td>
    </tr>
</tbody></table>





            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706239" mc:blocktype="divider" mcblock="2706239" mcblocktype="divider" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px solid #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706235" mc:blocktype="image" mcblock="2706235" mcblocktype="image" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td valign="top" style="padding:9px" class="mcnImageBlockInner">
                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
                        <tbody><tr>
                            <td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">

                                    <a href="http://todopets.mx/contacto/?url=contacto" title="" class="" target="_blank">
                                        <img align="center" alt="" src="https://gallery.mailchimp.com/5b140bc2eac72b4028ac10f26/_compresseds/9ef4c470-fd6c-4617-b7ac-ec116c89c673.jpg" width="564" style="max-width:750px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
                                    </a>

                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></div><div mc:block="2706219" mc:blocktype="divider" mcblock="2706219" mcblocktype="divider" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px solid #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706263" mc:blocktype="text" mcblock="2706263" mcblocktype="text" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->

				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>

                        <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                            <h1 class="null" style="text-align: center;"><span style="font-size:27px"><span style="font-family:open sans,helvetica neue,helvetica,arial,sans-serif">- INFORMACIÓN DE INTERÉS -</span></span></h1>

                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->

				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706267" mc:blocktype="divider" mcblock="2706267" mcblocktype="divider" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px solid #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706271" mc:blocktype="imageCaption" mcblock="2706271" mcblocktype="imageCaption" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock">
    <tbody class="mcnCaptionBlockOuter">
        <tr>
            <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;">


<table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionBottomContent" width="282">
    <tbody><tr>
        <td class="mcnCaptionBottomImageContent" align="center" valign="top" style="padding:0 9px 9px 9px;">


            <a href="http://www.todopets.mx/infografias/13/cosas-que-no-debes-darle-a-tu-perro" title="" class="" target="_blank">


            <img alt="" src="https://gallery.mailchimp.com/5b140bc2eac72b4028ac10f26/images/c5099f23-6d2b-4d4d-9c40-06d1b948f9a9.jpg" width="250" style="max-width:250px;" class="mcnImage">
            </a>

        </td>
    </tr>
    <tr>
        <td class="mcnTextContent" valign="top" style="padding:0 9px 0 9px;" width="282">
            <h4 class="null"><a href="http://www.todopets.mx/infografias/13/cosas-que-no-debes-darle-a-tu-perro" target="_blank">Cosas que NO DEBES darle a tu Perro</a></h4>

        </td>
    </tr>
</tbody></table>

<table align="right" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionBottomContent" width="282">
    <tbody><tr>
        <td class="mcnCaptionBottomImageContent" align="center" valign="top" style="padding:0 9px 9px 9px;">


            <a href="http://www.todopets.mx/informacion/30/la-agresion-inducida-por-miedo" title="" class="" target="_blank">


            <img alt="" src="https://gallery.mailchimp.com/5b140bc2eac72b4028ac10f26/images/3045bab5-c97b-4675-8f62-2cd5caaa787b.jpg" width="250" style="max-width:250px;" class="mcnImage">
            </a>

        </td>
    </tr>
    <tr>
        <td class="mcnTextContent" valign="top" style="padding:0 9px 0 9px;" width="282">
            <h4 class="null"><a href="http://www.todopets.mx/informacion/30/la-agresion-inducida-por-miedo" target="_blank">La agresión inducida por miedo</a></h4>
<br>
La agresión inducida por miedo se puede presentar tanto en hembras como...<br>
<br>
<a href="http://www.todopets.mx/informacion/30/la-agresion-inducida-por-miedo" target="_blank">Ver más información</a>
        </td>
    </tr>
</tbody></table>





            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706279" mc:blocktype="divider" mcblock="2706279" mcblocktype="divider" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px solid #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706275" mc:blocktype="imageCaption" mcblock="2706275" mcblocktype="imageCaption" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock">
    <tbody class="mcnCaptionBlockOuter">
        <tr>
            <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;">


<table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionBottomContent" width="282">
    <tbody><tr>
        <td class="mcnCaptionBottomImageContent" align="center" valign="top" style="padding:0 9px 9px 9px;">


            <a href="http://www.todopets.mx/informacion/6/mascotas-los-nuevos-clientes-de-lujo-de-las-companias" title="" class="" target="_blank">


            <img alt="" src="https://gallery.mailchimp.com/5b140bc2eac72b4028ac10f26/images/a7eadf3d-fc1c-4717-a680-027b1a10f573.jpg" width="250" style="max-width:250px;" class="mcnImage">
            </a>

        </td>
    </tr>
    <tr>
        <td class="mcnTextContent" valign="top" style="padding:0 9px 0 9px;" width="282">
            <h4 class="null"><a href="http://www.todopets.mx/informacion/6/mascotas-los-nuevos-clientes-de-lujo-de-las-companias" target="_blank">Los perros en época de calor se contagian de rabia, ¿Mito o realidad?</a></h4>
<br>
Esta semana nuestro país ha alcanzado temperaturas récord en diferentes estados de la República, ¡y el calor está sofocándonos a todos!...<br>
<br>
<a href="http://www.todopets.mx/informacion/6/mascotas-los-nuevos-clientes-de-lujo-de-las-companias" target="_blank">Ver más información</a>
        </td>
    </tr>
</tbody></table>

<table align="right" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionBottomContent" width="282">
    <tbody><tr>
        <td class="mcnCaptionBottomImageContent" align="center" valign="top" style="padding:0 9px 9px 9px;">


            <a href="http://www.todopets.mx/informacion/11/5-mascotas-exoticas-que-puedes-adoptar" title="" class="" target="_blank">


            <img alt="" src="https://gallery.mailchimp.com/5b140bc2eac72b4028ac10f26/images/9ef13907-118e-4b1d-b30b-da0311378880.jpg" width="250" style="max-width:250px;" class="mcnImage">
            </a>

        </td>
    </tr>
    <tr>
        <td class="mcnTextContent" valign="top" style="padding:0 9px 0 9px;" width="282">
            <h4 class="null"><a href="http://www.todopets.mx/informacion/11/5-mascotas-exoticas-que-puedes-adoptar" target="_blank">5 Mascotas Exóticas Que Puedes Adoptar </a></h4>
<br>
Las mascotas exóticas siempre son apasionantes para los amantes de los animales, el trabajo de lograr un hábitat lo más parecido al natural...<br>
<br>
<a href="http://www.todopets.mx/informacion/11/5-mascotas-exoticas-que-puedes-adoptar" target="_blank">Ver más información</a>
        </td>
    </tr>
</tbody></table>





            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706307" mc:blocktype="divider" mcblock="2706307" mcblocktype="divider" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px solid #EAEAEA;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706323" mc:blocktype="image" mcblock="2706323" mcblocktype="image" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td valign="top" style="padding:9px" class="mcnImageBlockInner">
                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
                        <tbody><tr>
                            <td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">

                                    <a href="http://todopets.mx/contacto/?url=contacto" title="" class="" target="_blank">
                                        <img align="center" alt="" src="https://gallery.mailchimp.com/5b140bc2eac72b4028ac10f26/_compresseds/2172c018-0b1b-4282-83b2-b47a06f25805.jpg" width="564" style="max-width:750px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
                                    </a>

                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></div><div mc:block="2706487" mc:blocktype="divider" mcblock="2706487" mcblocktype="divider" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 5px solid #E2E2E2;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706471" mc:blocktype="image" mcblock="2706471" mcblocktype="image" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td valign="top" style="padding:9px" class="mcnImageBlockInner">
                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
                        <tbody><tr>
                            <td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">


                                        <img align="center" alt="" src="https://gallery.mailchimp.com/5b140bc2eac72b4028ac10f26/images/81f46bc9-f3e7-4cab-85eb-9441a52562d1.jpg" width="300" style="max-width:300px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">


                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></div><div mc:block="2706319" mc:blocktype="text" mcblock="2706319" mcblocktype="text" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->

				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>

                        <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                            <div style="text-align: center;">ENCUENTRA Y COMPARTE&nbsp;TODO PARA LAS MASCOTAS<br>
<strong>Síguenos&nbsp;</strong>en nuestras redes sociales:</div>

                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->

				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706155" mc:blocktype="socialFollow" mcblock="2706155" mcblocktype="socialFollow" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width:100%;">
    <tbody class="mcnFollowBlockOuter">
        <tr>
            <td align="center" valign="top" style="padding:9px" class="mcnFollowBlockInner">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer" style="min-width:100%;">
    <tbody><tr>
        <td align="center" style="padding-left:9px;padding-right:9px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnFollowContent">
                <tbody><tr>
                    <td align="center" valign="top" style="padding-top:9px; padding-right:9px; padding-left:9px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tbody><tr>
                                <td align="center" valign="top">
                                    <!--[if mso]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                    <![endif]-->

                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->

                                            <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnFollowStacked" style="display:inline;">

                                                <tbody><tr>
                                                    <td align="center" valign="top" class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;">
                                                        <a href="http://todopets.mx/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-96.png" alt="Website" class="mcnFollowBlockIcon" width="48" style="width:48px; max-width:48px; display:block;"></a>
                                                    </td>
                                                </tr>


                                            </tbody></table>


                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->

                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->

                                            <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnFollowStacked" style="display:inline;">

                                                <tbody><tr>
                                                    <td align="center" valign="top" class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;">
                                                        <a href="https://www.instagram.com/todopetsmx/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-96.png" alt="Instagram" class="mcnFollowBlockIcon" width="48" style="width:48px; max-width:48px; display:block;"></a>
                                                    </td>
                                                </tr>


                                            </tbody></table>


                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->

                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->

                                            <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnFollowStacked" style="display:inline;">

                                                <tbody><tr>
                                                    <td align="center" valign="top" class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:9px;">
                                                        <a href="https://www.facebook.com/todopetsmx/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-96.png" alt="Facebook" class="mcnFollowBlockIcon" width="48" style="width:48px; max-width:48px; display:block;"></a>
                                                    </td>
                                                </tr>


                                            </tbody></table>


                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->

                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->

                                            <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnFollowStacked" style="display:inline;">

                                                <tbody><tr>
                                                    <td align="center" valign="top" class="mcnFollowIconContent" style="padding-right:0; padding-bottom:9px;">
                                                        <a href="mailto:contacto@todopets.mx" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-forwardtofriend-96.png" alt="Email" class="mcnFollowBlockIcon" width="48" style="width:48px; max-width:48px; display:block;"></a>
                                                    </td>
                                                </tr>


                                            </tbody></table>


                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->

                                    <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>

            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706495" mc:blocktype="divider" mcblock="2706495" mcblocktype="divider" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 5px solid #E2E2E2;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></div><div mc:block="2706311" mc:blocktype="boxedText" mcblock="2706311" mcblocktype="boxedText" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
    <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
	<tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td valign="top" class="mcnBoxedTextBlockInner">

				<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnBoxedTextContentContainer">
                    <tbody><tr>

                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                            <table border="0" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #F1F1F1;border: 10px solid #78ACF0;">
                                <tbody><tr>
                                    <td valign="top" class="mcnTextContent" style="padding: 18px;color: #2C2C2C;font-family: Helvetica;font-size: 14px;font-weight: normal;text-align: center;">
                                        <div style="color: #FFFFFF;text-align: justify;"><a href="*|FORWARD|*" target="_blank">
<br>
<a class="text-decoration: none;" href="http://www.todopets.mx/" target="_blank"><span style="color:#000000"></span></a><span style="color:#000000">
<span style="color:#000000">Está recibiendo esta información porque es una persona responsable y consciente de la importancia de estar al tanto de lo que pasa en el medio de las MASCOTAS.<br>
<br>
A usted dedicamos nuestro trabajo todos los días. Si no desea enterarse de lo más novedoso, puede dejar de recibir nuestra información respondiendo&nbsp;</span><a href="mailto:contacto@todopets.mx?subject=UNSUSCRIBE" target="_blank"><span style="color:#000000">REMOVER</span></a><span style="color:#000000"> a este correo o&nbsp;​</span><a href="http://todopets.mx/mailing_software/bd/unsuscribe.php?email=<?=$email;?>" target="_blank"><span style="color:#000000">DARSE DE BAJA DIRECTAMENTE EN LA PÁGINA</span></a><br>
<br>
<a href="http://www.todopets.mx/" target="_blank"><span style="color:#000000">TodoPets®</span></a><span style="color:#000000"> es un medio de difusión, por lo que solo se limita a enviar y promover información del medio de las MASCOTAS provista por sus contratantes, no siendo responsable de la calidad, veracidad o cualquier otra inconformidad o incumplimiento que pudiera existir.<br>
Usted ha recibido esta información porque: 1) Es cliente del <a href="http://todopets.mx" target="_blank">TodoPets®</a> 2) Se registró para recibirla o 3) Fue registrado(a) por un amigo(a). Este correo cumple con lo establecido por la Ley Federal de Protección al Consumidor, Capítulo VIII bis, artículo 76 bis, inciso VI. Si no desea recibir más información de este tipo, contestar este correo, dando clic aquí:</span><br>
<a href="mailto:contacto@todopets.mx" target="_blank"><span style="color:#000000">contacto@todopets.mx</span></a><br>
<br>
<a href="http://www.todopets.mx/" target="_blank"><span style="color:#000000">TodoPets®</span></a><span style="color:#000000"> es una marca registrada y es propiedad de&nbsp;G-13<br>
<br>
Gracias, Staff de Atención a Usuarios del </span><a href="http://www.todopets.mx/" target="_blank"><span style="color:#000000">TodoPets®</span></a><br>
<span style="color:#000000">No se podrá efectuar copia o transcripción total o parcial del presente documento sin la autorización expresa del </span><a href="http://www.todopets.mx/" target="_blank"><span style="color:#000000">TodoPets®</span></a><br>
<br>
<a href="http://www.todopets.mx/" target="_blank"><span style="color:#000000">www.todopets.mx</span></a></div>

                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if gte mso 9]>
				</td>
				<![endif]-->

				<!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></div></td>
                                        </tr>
                                    </table>
                                    <!-- // END BODY -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN COLUMNS // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateColumns">
                                        <tr>
                                            <td align="left" valign="top" class="columnsContainer" width="50%">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateColumn">
                                                    <tr>
                                                        <td valign="top" class="leftColumnContainer tpl-container" mc:container="left_column_container" mccontainer="left_column_container"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="left" valign="top" class="columnsContainer" width="50%">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateColumn">
                                                    <tr>
                                                        <td valign="top" class="rightColumnContainer tpl-container" mc:container="right_column_container" mccontainer="right_column_container"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END COLUMNS -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateFooter">
                                        <tr>
                                            <td valign="top" class="footerContainer tpl-container" mc:container="footer_container" style="padding-bottom:9px;" mccontainer="footer_container"><div mc:block="2706159" mc:blocktype="image" mcblock="2706159" mcblocktype="image" class="tpl-block"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td valign="top" style="padding:0px" class="mcnImageBlockInner">
                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
                        <tbody><tr>
                            <td class="mcnImageContent" valign="top" style="padding-right: 0px; padding-left: 0px; padding-top: 0; padding-bottom: 0; text-align:center;">

                                    <a href="http://todopets.mx/" title="" class="" target="_blank">
                                        <img align="center" alt="" src="https://gallery.mailchimp.com/5b140bc2eac72b4028ac10f26/images/8b6ad6a5-2a46-46e9-9b34-f497f8e9d836.jpg" width="600" style="max-width:1200px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
                                    </a>

                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></div></td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                                </td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>
