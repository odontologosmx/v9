<?php
if(empty($_FILES['file']))
{
	exit();
}
$errorImgFile = "../img/img_upload_error.jpg";
$destinationFolderPath="informacion";
$destinationFilePath = '../img/'.$destinationFolderPath.'/'.$_FILES['file']['name'];
$imageFileType = strtolower(pathinfo($destinationFilePath,PATHINFO_EXTENSION));
$uploadOk = 1;
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
    $uploadOk = 0;
}
if(!move_uploaded_file($_FILES['file']['tmp_name'], $destinationFilePath) && $uploadOk!=1){
	echo $errorImgFile;
}
else{
	echo $destinationFilePath;
}
?>
