<?php
include ("../../includes/inicio.php");
include ("../../includes/config.php");
?>
<?php
include ("../includes/modelo.php");
include ("../../includes/modeloGlobal.php");
$obj = new modelo();
$objMailing = new modeloGlobal();
$id=$_GET['id'];
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/summernote.css">
    <link rel="stylesheet" href="../css/main.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <?php
      if ($id!='') {
        $result_contenido=$obj->getContenido('calendario', $id);
        /*echo '<pre>';
        var_dump($result_contenido);
        echo '</pre>';*/
        $id_contenido=$id;
        $id_cat_calendario=$result_contenido['id_cat_calendario'];
        $id_cliente=$result_contenido['id_cliente'];
        $fecha_hora_inicio=$result_contenido['fecha_hora_inicio'];
        $fecha_hora_final=$result_contenido['fecha_hora_final'];
        $titulo=$result_contenido['titulo'];
        $imagen=$result_contenido['imagen'];
        $contenido=$result_contenido['contenido'];
        $link=$result_contenido['link'];
        $link_registro=$result_contenido['link_registro'];
        $sinopsis=$result_contenido['sinopsis'];
        $id_conferencista=$result_contenido['id_conferencista'];
        $id_ubicacion=$result_contenido['id_ubicacion'];
        $status=$result_contenido['status'];
      }
    ?>
    <div class="container">
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="../calendario">Calendarios <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../directorios">Directorios</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../Información">Información</a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
    <div class="container">
      <form id="alta_calendario">
        <h1>ALTA EVENTOS</h1>
        <?php if($id_contenido!='') { ?>
        <div class="row">
          <label for="id_contenido">ID: <?=$id_contenido;?></label>
            <input type="hidden" name="id_contenido" id="id_contenido" value="<?=$id_contenido;?>" class="datos">
        </div>
        <?php } ?>
        <div class="row">
          <label for="seccion_calendario">Clasificación</label>
          <?php $calendario=$obj->getCatalogos('s_cat_calendario'); ?>
          <select name="seccion_calendario" id="seccion_calendario" class="datos">
            <option value="" disabled="disabled" <?php if ($id_cat_calendario=='') echo "selected=selected"; ?>>Seleccione...</option>
            <?php foreach ($calendario as $calendario) { ?>
              <option value="<?=$calendario['ID'];?>" <?php if ($calendario['ID']==$id_cat_calendario) echo "selected=selected"; ?>><?=utf8_encode($calendario['evento']);?></option>
            <?php } ?>
          </select>
        </div>
        <div class="row">
          <label for="clientes">Cliente</label>
          <?php $clientes=$obj->getCatalogos('s_clientes'); ?>
          <select name="clientes" id="clientes" class="datos">
            <option value="" disabled="disabled" <?php if ($id_cliente=='') echo "selected=selected"; ?>>Seleccione...</option>
            <?php foreach ($clientes as $clientes) { ?>
              <option value="<?=$clientes['ID'];?>" <?php if ($clientes['ID']==$id_cliente) echo "selected=selected"; ?>><?=utf8_encode($clientes['nombre_comercial']);?></option>
            <?php } ?>
          </select>
        </div>
        <div class="row">
          <label for="fecha_hora_publicacion">Fecha Inicio</label>
          <input type="text" name="fecha_hora_publicacion" id="fecha_hora_publicacion" <?php if($fecha_hora_inicio!='') echo 'value="'.$fecha_hora_inicio.'"'; ?> class="datos" data-obligatorio="true">
        </div>
        <div class="row">
          <label for="fecha_hora_vigencia">Fecha Final</label>
          <input type="text" name="fecha_hora_vigencia" id="fecha_hora_vigencia" <?php if($fecha_hora_final!='') echo 'value="'.$fecha_hora_final.'"'; ?> class="datos" data-obligatorio="true">
        </div>
        <div class="row">
          <label for="titulo">Título</label>
          <input type="text" name="titulo" id="titulo" <?php if($titulo!='') echo 'value="'.$titulo.'"'; ?> class="datos" data-obligatorio="true">
        </div>
        <div class="row">
          <label for="imagen">Imagen</label>
          <input type="text" name="imagen" id="imagen" <?php if($imagen!='') echo 'value="'.$imagen.'"'; ?> class="datos" data-obligatorio="true">
        </div>
        <div class="row">
          <label for="contenido">Contenido</label>
          <textarea name="contenido_editor" id="contenido_editor" cols="40" rows="5" class="summernote"><?php if($contenido!='') echo $contenido; ?></textarea>
          <input type="hidden" name="contenido" id="contenido" <?php if($contenido!='') echo 'value="'.$contenido.'"'; ?> class="datos">
        </div>
        <div class="row">
          <label for="link">Link</label>
          <input type="text" name="link" id="link" <?php if($link!='') echo 'value="'.$link.'"'; ?> class="datos">
        </div>
        <div class="row">
          <label for="link_registro">Link Registro</label>
          <input type="text" name="link_registro" id="link_registro" <?php if($link_registro!='') echo 'value="'.$link_registro.'"'; ?> class="datos">
        </div>
        <div class="row">
          <label for="sinopsis">Sinopsis</label>
          <textarea name="sinopsis" id="sinopsis" cols="40" rows="5" class="datos" data-obligatorio="true"><?php if($sinopsis!='') echo $sinopsis; ?></textarea>
        </div>
        <div class="row">
          <label for="ubicaciones">Ubicación</label>
          <?php $ubicaciones=$obj->getCatalogos('ubicaciones'); ?>
          <select name="ubicaciones" id="ubicaciones" class="datos" data-obligatorio="true">
            <option value="" disabled="disabled" selected="selected">Seleccione...</option>
            <?php foreach ($ubicaciones as $ubicaciones) { ?>
              <option value="<?=$ubicaciones['ID'];?>" <?php if ($ubicaciones['ID']==$id_ubicacion) echo "selected=selected"; ?>><?=utf8_encode($ubicaciones['nombre_ubicacion']);?></option>
            <?php } ?>
          </select>
        </div>
        <div class="row">
          <label for="id_conferencista">Conferencistas</label>
          <input type="text" name="id_conferencista" id="id_conferencista" <?php if($id_conferencista!='') echo 'value="'.$id_conferencista.'"'; ?> class="datos" data-obligatorio="true">
        </div>
        <div class="row">
          <label for="tags_list">Etiquetas</label>
          <?php $tags=$obj->getCatalogos('s_cat_tags'); ?>
          <?php foreach ($tags as $tags) { ?>
            <input type="checkbox" name="tags_list" value="<?=$tags['ID'];?>" class="tags_list"><?=utf8_encode($tags['tag']);?><br>
          <?php } ?>
          <input type="hidden" name="tags" id="tags" class="datos">
        </div>
        <div class="row">
          <label for="categorias_list">Categorías</label>
          <?php $categorias=$obj->getCatalogos('s_cat_categorias'); ?>
          <?php foreach ($categorias as $categorias) { ?>
            <input type="checkbox" name="categorias_list" value="<?=$categorias['ID'];?>" class="categorias_list"><?=utf8_encode($categorias['categoria']);?><br>
          <?php } ?>
          <input type="hidden" name="categorias" id="categorias" class="datos">
        </div>
        <div class="row">
          <label for="status">Status</label>
          <select name="status" id="status" class="datos">
            <option value="1" <?php if ($status=='' || $status=='1') echo "selected=selected"; ?>>ACTIVO</option>
            <option value="0" <?php if ($status=='0') echo "selected=selected"; ?>>INACTIVO</option>
          </select>
        </div>
        <?php if ($id=='') { ?>
        <button name="envio" type="button" class="btn-form" onClick="ABC('calendario','alta_calendario','insert')" id="btnAction">INSERTAR</button>
        <?php } else { ?>
          <button name="envio" type="button" class="btn-form" onClick="ABC('calendario','alta_calendario','update')" id="btnAction">EDITAR</button>
        <?php } ?>
      </form>
      <div id="respuesta_form_ok" style="display:none">
        <h1 class="text-center text-success">ACCIÓN REALIZADA CORRECTAMENTE</h1>
        </h3>
      </div>
      <div id="respuesta_form_error" style="display:none;">
          <h1 class="text-center text-danger">ERROR AL EJECUTAR LA ACCIÓN</h1>
      </div>
      <div class="result"></div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="../js/animatescroll.min.js"></script>
    <script src="../js/summernote.js"></script>
    <script src="../js/main.js"></script>
  </body>
</html>
