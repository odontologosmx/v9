<?php
include ("../../../includes/inicio.php");
include ("../../../includes/config.php");
?>
<?php
include ("../includes/modelo.php");
include ("../../../includes/modeloGlobal.php");
$obj = new modelo();
$objMailing = new modeloGlobal();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/summernote.css">
    <link rel="stylesheet" href="../css/main.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
      <form id="alta_informacion">
        <h1>ALTA INFORMACIÓN</h1>
        <div class="row">
          <label for="clientes">Cliente</label>
          <?php $clientes=$obj->getCatalogos('s_clientes'); ?>
          <select name="clientes" id="clientes" class="datos" data-obligatorio="true">
            <option value="" disabled="disabled" selected="selected">Seleccione...</option>
            <?php foreach ($clientes as $clientes) { ?>
              <option value="<?=$clientes['ID'];?>"><?=utf8_encode($clientes['nombre_comercial']);?></option>
            <?php } ?>
          </select>
        </div>
        <div class="row">
          <label for="seccion_informativa">Sección</label>
          <?php $seccion_informativa=$obj->getCatalogos('s_cat_seccion_informativa'); ?>
          <select name="seccion_informativa" id="seccion_informativa" class="datos">
            <option value="" disabled="disabled" selected="selected">Seleccione...</option>
            <?php foreach ($seccion_informativa as $seccion_informativa) { ?>
              <option value="<?=$seccion_informativa['ID'];?>"><?=utf8_encode($seccion_informativa['seccion']);?></option>
            <?php } ?>
          </select>
        </div>
        <div class="row">
          <label for="fecha_hora_publicacion">Fecha Publicación</label>
          <input type="text" name="fecha_hora_publicacion" id="fecha_hora_publicacion" class="datos" data-obligatorio="true">
        </div>
        <div class="row">
          <label for="fecha_hora_vigencia">Vigencia</label>
          <input type="text" name="fecha_hora_vigencia" id="fecha_hora_vigencia" class="datos" data-obligatorio="true">
        </div>
        <div class="row">
          <label for="titulo">Título</label>
          <input type="text" name="titulo" id="titulo" class="datos" data-obligatorio="true">
        </div>
        <div class="row">
          <label for="imagen">Imagen</label>
          <input type="text" name="imagen" id="imagen" class="datos" data-obligatorio="true">
        </div>
        <div class="row">
          <label for="contenido">Contenido</label>
          <textarea name="contenido_editor" id="contenido_editor" cols="40" rows="5" class="summernote" data-obligatorio="true"></textarea>
          <input type="hidden" name="contenido" id="contenido" value="" class="datos">
        </div>
        <div class="row">
          <label for="prioridad">Prioridad/Orden</label>
          <input type="text" name="prioridad" id="prioridad" class="datos" data-obligatorio="true">
        </div>
        <div class="row">
          <label for="tags_list">Etiquetas</label>
          <?php $tags=$obj->getCatalogos('s_cat_tags'); ?>
          <?php foreach ($tags as $tags) { ?>
            <input type="checkbox" name="tags_list" value="<?=$tags['ID'];?>" class="tags_list"><?=utf8_encode($tags['tag']);?><br>
          <?php } ?>
          <input type="hidden" name="tags" id="tags" value="" class="datos">
        </div>
        <div class="row">
          <label for="categorias_list">Categorías</label>
          <?php $categorias=$obj->getCatalogos('s_cat_categorias'); ?>
          <?php foreach ($categorias as $categorias) { ?>
            <input type="checkbox" name="categorias_list" value="<?=$categorias['ID'];?>" class="categorias_list"><?=utf8_encode($categorias['categoria']);?><br>
          <?php } ?>
          <input type="hidden" name="categorias" id="categorias" value="" class="datos">
        </div>
        <button name="envio" type="button" class="btn-form" onClick="ABC('informacion','alta_informacion','insert')" id="btnAction">INSERTAR</button>
      </form>
      <div id="respuesta_form_ok" style="display:none">
        <h1 class="text-center text-success">ACCIÓN REALIZADA CORRECTAMENTE</h1>
        </h3>
      </div>
      <div id="respuesta_form_error" style="display:none;">
          <h1 class="text-center text-danger">ERROR AL EJECUTAR LA ACCIÓN</h1>
      </div>
      <div class="result"></div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="../../js/animatescroll.min.js"></script>
    <script src="../../js/summernote.js"></script>
    <script src="../../js/main.js"></script>
  </body>
</html>
