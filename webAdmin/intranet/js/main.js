$( document ).ready(function() {
  $( ".tags_list" ).change(function() {
    var tags_list = [];
    $(".tags_list:checked").each(function() {
      tags_list.push($(this).val());
    });
    var selected = tags_list.join(',');
    $("#tags").val(selected);
  });
  $( ".categorias_list" ).change(function() {
    var categorias_list = [];
    $(".categorias_list:checked").each(function() {
      categorias_list.push($(this).val());
    });
    var selected = categorias_list.join(',');
    $("#categorias").val(selected);
  });

  $('.summernote').summernote({
    height: 200,
    callbacks: {
      onChange: function() {
        var summernoteContent = $('#contenido_editor').summernote('code');
        $('#contenido').val(summernoteContent);
      },
      onImageUpload: function(files)
      {
        var $editor = $(this);
        var data = new FormData();
        data.append('file', files[0]);
        $.ajax({
          url: "../includes/editor-upload.php",
          method: 'POST',
          data: data,
          processData: false,
          contentType: false,
          success: function(response) {
            $editor.summernote('insertImage', response);
          }
        });
      },
      onImageUploadError: null
    },
  });
});


function ABC(tabla,form,accion){
  var tabla;
  var form;
  var accion;

  $(".text-danger").remove(); //quita la clase de textos rojos

  var sendMail = "true"; //variable que se inicia para hacer la validaacion "NUNCA MODIFICAR ESTA VARIABLE"

  $( "#" + form + " .datos" ).each(function() {
    //console.log($(this).data("obligatorio"));
    if ($(this).data("obligatorio")==true) {
      if ($(this).val() == '' ){
        $(this).after( "<span class='text-danger'>*Campo Obligatorio. <i class='fa fa-arrow-up' aria-hidden='true'></i></span>" );
        $(".text-danger").animatescroll({scrollSpeed:1000, padding:70});
        sendMail = "false";
      }
    }
  });

  if(sendMail == "true"){

    datos=$( "#" + form + " .datos" ).serializeArray();
    console.log(datos);
    /* envio al archivo por ajax, solo cambiar si es necesario el valor de url por el nombre del archivo de envio */
    datos = JSON.stringify(datos);
    console.log(datos);
    //alert("Listo para enviar datos...");

    $.ajax({
      data: "datos=" + datos + "&tabla=" + tabla + "&accion=" + accion,
      url: '../includes/abc.php',
      type: 'POST',
      beforeSend: function () {
      $("#" + form + " #btnAction").val("ENVIANDO...");
    },
    success: function (result) {
       //alert("Datos recibidos...");
       //jQuery(".result").html(result);
       if (result==1) {
         $("#respuesta_form_ok").slideDown().delay(5000).slideUp();
         $("#" + form).slideUp().delay(5000).slideDown();
         $("#" + form)[0].reset();//limpiamos el formulario
         $("#" + form + " #btnAction").val("ENVIAR");//ponemos la palabra Enviar en el boton
       } else {
         $("#respuesta_form_error").slideDown().delay(5000).slideup();
       }
    }
  });

  }
}
