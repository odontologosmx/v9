<?php
if(empty($_FILES['file']))
{
	exit();
}

$imageFileType = strtolower(pathinfo($destinationFilePath,PATHINFO_EXTENSION));
$uploadOk = 1;
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
    $uploadOk = 0;
}
$check = getimagesize($_FILES["file"]["tmp_name"]);
if($check !== false) {
    //echo "File is an image - " . $check["mime"] . ".";
    $uploadOk = 1;
} else {
    //echo "File is not an image.";
    $uploadOk = 0;
}

$errorImgFile = "../img/img_upload_error.jpg";
$destinationFolderPath="informacion";
$destinationFilePath = '../img/'.$destinationFolderPath.'/'.$_FILES['file']['name'];
if ($uploadOk==1) {
	if(!move_uploaded_file($_FILES['file']['tmp_name'], $destinationFilePath)){
		echo $errorImgFile;
	} else {
		echo $destinationFilePath;
	}
} else {
	echo $errorImgFile;
}

?>
