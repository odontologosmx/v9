<?php
class modelo{

  private function getobjGlobal(){
    return $getobjGlobal = new modeloGlobal();
  }

  /* ----------------------------- GET CATÁLOGOS ----------------------------- */
  public function getContenido($tabla, $parametro){
    $getobjGlobal=$this->getobjGlobal();
    $select = "SELECT * ";
    $from = "FROM `".$tabla."`";
    $where = "WHERE";
    $clause="`ID`= '$parametro'";
    switch ($tabla) {
      case 'calendario':

      break;
    }
    $query=$select." ".$from." ".$where." ".$clause." ".$order." ".$limit;
    return $getobjGlobal->ejecutaConsulta($query,'select',$debug);
  }

  public function getCatalogos($parametro){
    $getobjGlobal=$this->getobjGlobal();
    $select = "SELECT * ";
    $from = "FROM `".$parametro."`";
    $where = "WHERE";
    $clause="`status`='1'";
    switch ($parametro) {
      case 's_clientes':
        $order = "ORDER BY `nombre_comercial`";
      break;
      case 's_cat_origen':
        $order = "ORDER BY `origen`";
      break;
      case 's_cat_clasificaciones':
        $order = "ORDER BY `clasificacion`";
      break;
      case 's_cat_seccion_informativa':
        $order = "ORDER BY `seccion`";
      break;
      case 's_cat_calendario':
        $order = "ORDER BY `evento`";
      break;
      case 's_cat_categorias':
        $order = "ORDER BY `categoria`";
      break;
      case 's_cat_tags':
        $order = "ORDER BY `tag`";
      break;
      case 's_cat_estados':
        $order = "ORDER BY `estado`";
      break;
      case 's_cat_medios_contacto':
        $order = "ORDER BY `medio_contacto`";
      break;
      case 's_cat_paises':
        $order = "ORDER BY `pais`";
      break;
      case 's_cat_ubicaciones':
        $order = "ORDER BY `ubicacion`";
      break;
      case 'ubicaciones':
        $order = "ORDER BY `nombre_ubicacion`";
      break;
      default:
      if (is_array($parametro)) {
        $from = "FROM `s_cat_".$parametro['tabla']."`";
        $clause.= 'AND `id_area`="'.$parametro['area'].'"';
        $order = "ORDER BY `nombre`";
      }
    }
    $query=$select." ".$from." ".$where." ".$clause." ".$order." ".$limit;
    return $getobjGlobal->ejecutaConsulta($query,'selectmultiple',$debug);
  }

  /* ----------------------------- A,B,C ----------------------------- */
  public function ABC($tabla,$parametro,$accion){
    var_dump($parametro);
    $getobjGlobal=$this->getobjGlobal();

    $fecha_hora_registro=date('Y-m-d H:i:s');
    $usuario=$parametro['usuario'];
    $fecha_hora_ultima_modificacion=date('Y-m-d H:i:s');
    $status=$parametro['status'];

    switch ($tabla) {
      case 's_clientes':
        $origen=$parametro['origen'];
        $clasificacion=$parametro['clasificacion'];
        $asesor=$parametro['asesor'];
        $responsable=$parametro['responsable'];
        $nombre_comercial=$parametro['nombre_comercial'];
        $nickname=$parametro['nickname'];
        $avatar=$parametro['avatar'];
        $imagen_perfil=$parametro['imagen_perfil'];
        $cv=$parametro['cv'];
        $contenido=$parametro['contenido'];
        $permalink=$parametro['permalink'];
        $tags=$parametro['tags'];
        $categorias=$parametro['categorias'];
        $username=$parametro['username'];
        $password=$parametro['password'];

        $insert="INSERT INTO `todopetsmx_web`.`s_clientes` (`ID`, `fecha_hora_registro`, `id_cat_origen`, `id_cat_clasificacion_cliente`, `id_asesor`, `id_responsable`, `nombre_comercial`, `nickname`, `avatar`, `imagen_perfil`, `cv_empresarial`, `contenido_personalizado`, `permalink`, `id_cat_tags`, `id_cat_categorias`, `username`, `password`, `id_usuario`, `fecha_hora_ultima_modificacion`, `status`)";
        $values="VALUES (NULL, '$fecha_hora_registro', '$origen', '$clasificacion', '$asesor', '$responsable', '$nombre_comercial', '$nickname', '$avatar', '$imagen_perfil', '$cv', '$contenido', '$permalink', '$tags', '$categorias', '$username', '$password', '$usuario', '$fecha_hora_ultima_modificacion', '$status');";
        $query=$insert.$values;
      break;

      case 'ubicaciones':
        $cliente=$parametro['clientes'];
        $id_ubicacion=$parametro['ubicaciones'];
        $nombre_ubicacion=$parametro['nombre_ubicacion'];
        $calle=$parametro['calle'];
        $numero=$parametro['numero'];
        $colonia=$parametro['colonia'];
        $ciudad=$parametro['ciudad'];
        $estado=$parametro['estados'];
        $cp=$parametro['cp'];
        $lat=$parametro['lat'];
        $long=$parametro['long'];
        $placeID=$parametro['placeID'];

        $insert="INSERT INTO `todopetsmx_web`.`ubicaciones` (`ID`, `fecha_hora_registro`, `id_cliente`, `id_cat_ubicaciones`, `nombre_ubicacion`, `calle`, `numero`, `colonia`, `ciudad`, `id_estado`, `cp`, `lat`, `long`, `placeID`, `id_usuario`, `fecha_hora_ultima_modificacion`, `status`)";
        $values="VALUES (NULL, '$fecha_hora_registro', '$cliente', '$id_ubicacion', '$nombre_ubicacion', '$calle', '$numero', '$colonia', '$ciudad', '$estado', '$cp', '$lat', '$long', '$placeID', '$usuario', '$fecha_hora_ultima_modificacion', '$status');";
        $query=$insert.$values;
      break;
      case 'medios_contacto':
        $cliente=$parametro['clientes'];
        $id_medio_contacto=$parametro['medios_contacto'];
        $medio_contacto=$parametro['medio_contacto'];

        $insert="INSERT INTO `todopetsmx_web`.`medios_contacto` (`ID`, `fecha_hora_registro`, `id_cliente`, `id_cat_medios_contacto`, `datos_medio_contacto`, `id_usuario`, `fecha_hora_ultima_modificacion`, `status`)";
        $values="VALUES (NULL, '$fecha_hora_registro', '$cliente', '$id_medio_contacto', '$medio_contacto', '$usuario', '$fecha_hora_ultima_modificacion', $status);";
        $query=$insert.$values;
      break;

      case 'informacion':
        $cliente=$parametro['clientes'];
        $fecha_hora_publicacion=$parametro['fecha_hora_publicacion'];
        $fecha_hora_vigencia=$parametro['fecha_hora_vigencia'];
        $seccion_informativa=$parametro['seccion_informativa'];
        $titulo=$parametro['titulo'];
        $imagen=$parametro['imagen'];
        $contenido=$parametro['contenido'];
        $prioridad=$parametro['prioridad'];
        $tags=$parametro['tags'];
        $categorias=$parametro['categorias'];

        $insert="INSERT INTO `todopetsmx_web`.`informacion` (`ID`, `fecha_hora_registro`, `id_cliente`, `fecha_hora_publicacion`, `fecha_hora_vigencia`, `id_cat_seccion_informativa`, `titulo`, `imagen`, `contenido`, `prioridad`, `id_cat_tags`, `id_cat_especialidades`, `id_usuario`, `fecha_hora_ultima_modificacion`, `status`)";
        $values="VALUES (NULL, '$fecha_hora_registro', '$cliente', '$fecha_hora_publicacion', '$fecha_hora_vigencia', '$seccion_informativa', '$titulo', '$imagen', '$contenido', '$prioridad',  '$tags', '$categorias', '$usuario', '$fecha_hora_ultima_modificacion', $status);";
        $query=$insert.$values;
      break;

      case 'calendario':
        $id_contenido=$parametro['id_contenido'];
        $cliente=$parametro['clientes'];
        $fecha_hora_publicacion=$parametro['fecha_hora_publicacion'];
        $fecha_hora_vigencia=$parametro['fecha_hora_vigencia'];
        $seccion_calendario=$parametro['seccion_calendario'];
        $titulo=$parametro['titulo'];
        $imagen=$parametro['imagen'];
        $contenido=$parametro['contenido'];
        $link=$parametro['link'];
        $link_registro=$parametro['link_registro'];
        $sinopsis=$parametro['sinopsis'];
        $ubicaciones=$parametro['ubicaciones'];
        $id_conferencista=$parametro['id_conferencista'];
        $tags=$parametro['tags'];
        $categorias=$parametro['categorias'];

        switch ($accion) {
          case 'insert':
          $insert="INSERT INTO `todopetsmx_web`.`calendario` (`ID`, `fecha_hora_registro`, `id_cat_calendario`, `id_cliente`, `fecha_hora_inicio`, `fecha_hora_final`, `titulo`, `imagen`, `contenido`, `link`, `link_registro`, `sinopsis`, `id_ubicacion`, `id_conferencista`, `id_cat_especialidades`, `id_cat_tags`, `id_usuario`, `fecha_hora_ultima_modificacion`, `status`)";
          $values="VALUES (NULL, '$fecha_hora_registro', '$seccion_calendario', '$cliente', '$fecha_hora_publicacion', '$fecha_hora_vigencia', '$titulo', '$imagen', '$contenido', '$link', '$link_registro', '$sinopsis', '$ubicaciones', '$id_conferencista', '$categorias', '$tags', '$usuario', '$fecha_hora_ultima_modificacion', $status);";
          $query=$insert.$values;
          break;
          case 'update':
          $insert="UPDATE  `todopetsmx_web`.`calendario`";
          $values=" SET `id_cat_calendario` = '$seccion_calendario', `id_cliente` = '$cliente', `fecha_hora_inicio` = '$fecha_hora_publicacion', `fecha_hora_final` = '$fecha_hora_vigencia', `titulo` = '$titulo', `imagen` = '$imagen', `contenido` = '$contenido', `link` = '$link', `link_registro` = '$link_registro', `sinopsis` = '$sinopsis', `id_ubicacion` = '$ubicaciones', `id_conferencista` = '$id_conferencista', `id_cat_especialidades` = '$categorias', `id_cat_tags` = '$tags', `fecha_hora_ultima_modificacion` = '$fecha_hora_ultima_modificacion', `status` = '$status'";
          $where=" WHERE `calendario`.`ID` = '$id_contenido';";
          $query=$insert.$values.$where;
          break;
        }

      break;
    }
    echo $getobjGlobal->ejecutaConsulta($query,$accion,$debug);
  }

}
?>
