<?php
include ("../../includes/inicio.php");
include ("../../includes/config.php");
?>
<?php
include ("../includes/modelo.php");
include ("../../includes/modeloGlobal.php");
$obj = new modelo();
$objMailing = new modeloGlobal();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/summernote.css">
    <link rel="stylesheet" href="../css/main.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
      <form id="alta_cliente">
        <h1>ALTA CLIENTE</h1>
        <div class="row">
          <label for="origen">Orígen</label>
          <?php $origen=$obj->getCatalogos('s_cat_origen'); ?>
          <select name="origen" id="origen" class="datos">
            <option value="" disabled="disabled" selected="selected">Seleccione...</option>
            <?php foreach ($origen as $origen) { ?>
              <option value="<?=$origen['ID'];?>"><?=utf8_encode($origen['origen']);?></option>
            <?php } ?>
          </select>
        </div>
        <div class="row">
          <label for="clasificacion">Clasificación Cliente</label>
          <?php $clasificaciones=$obj->getCatalogos('s_cat_clasificaciones'); ?>
          <select name="clasificacion" id="clasificacion" class="datos">
            <option value="" disabled="disabled" selected="selected">Seleccione...</option>
            <?php foreach ($clasificaciones as $clasificaciones) { ?>
              <option value="<?=$clasificaciones['ID'];?>"><?=utf8_encode($clasificaciones['clasificacion']);?></option>
            <?php } ?>
          </select>
        </div>
        <div class="row">
          <label for="asesor">Asesor</label>
          <?php $param_asesor=array("tabla"=>"usuarios", "area"=>"1"); $asesor=$obj->getCatalogos($param_asesor); ?>
          <select name="asesor" id="asesor" class="datos">
            <option value="" disabled="disabled" selected="selected">Seleccione...</option>
            <?php foreach ($asesor as $asesor) { ?>
              <option value="<?=$asesor['ID'];?>"><?=utf8_encode($asesor['nombre']);?></option>
            <?php } ?>
          </select>
        </div>
        <div class="row">
          <label for="responsable">Responsable</label>
          <?php $param_responsable=array("tabla"=>"usuarios", "area"=>"2"); $responsable=$obj->getCatalogos($param_responsable); ?>
          <select name="responsable" id="responsable" class="datos">
            <option value="" disabled="disabled" selected="selected">Seleccione...</option>
            <?php foreach ($responsable as $responsable) { ?>
              <option value="<?=$responsable['ID'];?>"><?=utf8_encode($responsable['nombre']);?></option>
            <?php } ?>
          </select>
        </div>
        <div class="row">
          <label for="nombre_comercial">Nombre Comercial</label>
          <input type="text" name="nombre_comercial" id="nombre_comercial" class="datos" data-obligatorio="true">
        </div>
        <div class="row">
          <label for="nickname">Nickname</label>
          <input type="text" name="nickname" id="nickname" class="datos">
        </div>
        <div class="row">
          <label for="avatar">Avatar</label>
          <input type="text" name="avatar" id="avatar" class="datos">
        </div>
        <div class="row">
          <label for="imagen_perfil">Imagen Perfil</label>
          <input type="text" name="imagen_perfil" id="imagen_perfil" class="datos">
        </div>
        <div class="row">
          <label for="cv">CV Empresarial</label>
          <textarea name="cv" id="cv" cols="40" rows="5" class="datos"></textarea>
        </div>
        <div class="row">
          <label for="contenido">Contenido Personalizado</label>
          <textarea name="contenido_editor" id="contenido_editor" cols="40" rows="5" class="summernote"></textarea>
          <input type="hidden" name="contenido" id="contenido" value="" class="datos">
        </div>
        <div class="row">
          <label for="permalink">Permalink</label>
          <input type="text" name="permalink" id="permalink" class="datos" data-obligatorio="true">
        </div>
        <div class="row">
          <label for="username">Username</label>
          <input type="text" name="username" id="username" class="datos">
        </div>
        <div class="row">
          <label for="password">Password</label>
          <input type="text" name="password" id="password" class="datos">
        </div>
        <div class="row">
          <label for="tags_list">Etiquetas</label>
          <?php $tags=$obj->getCatalogos('s_cat_tags'); ?>
          <?php foreach ($tags as $tags) { ?>
            <input type="checkbox" name="tags_list" value="<?=$tags['ID'];?>" class="tags_list"><?=utf8_encode($tags['tag']);?><br>
          <?php } ?>
          <input type="hidden" name="tags" id="tags" value="" class="datos">
        </div>
        <div class="row">
          <label for="categorias_list">Categorías</label>
          <?php $categorias=$obj->getCatalogos('s_cat_categorias'); ?>
          <?php foreach ($categorias as $categorias) { ?>
            <input type="checkbox" name="categorias_list" value="<?=$categorias['ID'];?>" class="categorias_list"><?=utf8_encode($categorias['categoria']);?><br>
          <?php } ?>
          <input type="hidden" name="categorias" id="categorias" value="" class="datos">
        </div>
        <button name="envio" type="button" class="btn-form" onClick="ABC('s_clientes','alta_cliente','insert')" id="btnAction">INSERTAR</button>
      </form>
      <form id="alta_ubicaciones">
        <h1>UBICACIONES</h1>
        <div class="row">
          <label for="clientes">Cliente</label>
          <?php $clientes=$obj->getCatalogos('s_clientes'); ?>
          <select name="clientes" id="clientes" class="datos" data-obligatorio="true">
            <option value="" disabled="disabled" selected="selected">Seleccione...</option>
            <?php foreach ($clientes as $clientes) { ?>
              <option value="<?=$clientes['ID'];?>"><?=utf8_encode($clientes['nombre_comercial']);?></option>
            <?php } ?>
          </select>
        </div>
        <div class="row">
          <label for="ubicaciones">Ubicaciones</label>
          <?php $estados=$obj->getCatalogos('s_cat_ubicaciones'); ?>
          <select name="ubicaciones" id="ubicaciones" class="datos" data-obligatorio="true">
            <option value="" disabled="disabled" selected="selected">Seleccione...</option>
            <?php foreach ($estados as $estados) { ?>
              <option value="<?=$estados['ID'];?>"><?=utf8_encode($estados['ubicacion']);?></option>
            <?php } ?>
          </select>
        </div>
        <div class="row">
          <label for="nombre_ubicacion">Nombre Ubicación</label>
          <input type="text" name="nombre_ubicacion" id="nombre_ubicacion" class="datos" data-obligatorio="true">
        </div>
        <div class="row">
          <label for="calle">Calle</label>
          <input type="text" name="calle" id="calle" class="datos" data-obligatorio="true">
        </div>
        <div class="row">
          <label for="numero">No.</label>
          <input type="text" name="numero" id="numero" class="datos" data-obligatorio="true">
        </div>
        <div class="row">
          <label for="colonia">Colonia</label>
          <input type="text" name="colonia" id="colonia" class="datos" data-obligatorio="true">
        </div>
        <div class="row">
          <label for="ciudad">Ciudad</label>
          <input type="text" name="ciudad" id="ciudad" class="datos" data-obligatorio="true">
        </div>
        <div class="row">
          <label for="estados">Estado</label>
          <?php $estados=$obj->getCatalogos('s_cat_estados'); ?>
          <select name="estados" id="estados" class="datos" data-obligatorio="true">
            <option value="" disabled="disabled" selected="selected">Seleccione...</option>
            <?php foreach ($estados as $estados) { ?>
              <option value="<?=$estados['ID'];?>"><?=utf8_encode($estados['estado']);?></option>
            <?php } ?>
          </select>
        </div>
        <div class="row">
          <label for="cp">CP</label>
          <input type="text" name="cp" id="cp" class="datos">
        </div>
        <div class="row">
          <label for="lat">Lat</label>
          <input type="text" name="lat" id="lat" class="datos">
        </div>
        <div class="row">
          <label for="long">Long</label>
          <input type="text" name="long" id="long" class="datos">
        </div>
        <div class="row">
          <label for="placeID">PlaceID</label>
          <input type="text" name="placeID" id="placeID" class="datos">
        </div>
        <!--<div class="row">
          <label for="paises">Pais</label>
          <?php $estados=$obj->getCatalogos('s_cat_paises'); ?>
          <select name="paises" id="paises" class="datos">
            <option value="" disabled="disabled" selected="selected">Seleccione...</option>
            <?php foreach ($estados as $estados) { ?>
              <option value="<?=$estados['ID'];?>"><?=utf8_encode($estados['pais']);?></option>
            <?php } ?>
          </select>
        </div>-->
        <button name="envio" type="button" class="btn-form" onClick="ABC('ubicaciones','alta_ubicaciones','insert')" id="btnAction">INSERTAR</button>
      </form>
      <form id="alta_medios_contacto">
        <h1>MEDIOS DE CONTACTO</h1>
        <div class="row">
          <label for="clientes">Cliente</label>
          <?php $clientes=$obj->getCatalogos('s_clientes'); ?>
          <select name="clientes" id="clientes" class="datos" data-obligatorio="true">
            <option value="" disabled="disabled" selected="selected">Seleccione...</option>
            <?php foreach ($clientes as $clientes) { ?>
              <option value="<?=$clientes['ID'];?>"><?=utf8_encode($clientes['nombre_comercial']);?></option>
            <?php } ?>
          </select>
        </div>
        <div class="row">
          <label for="medios_contacto">Medios de Contacto</label>
          <?php $medios_contacto=$obj->getCatalogos('s_cat_medios_contacto'); ?>
          <select name="medios_contacto" id="medios_contacto" class="datos" data-obligatorio="true">
            <option value="" disabled="disabled" selected="selected">Seleccione...</option>
            <?php foreach ($medios_contacto as $medios_contacto) { ?>
              <option value="<?=$medios_contacto['ID'];?>"><?=utf8_encode($medios_contacto['medio_contacto']);?></option>
            <?php } ?>
          </select>
        </div>
        <div class="row">
          <label for="medio_contacto">Dato de Contacto</label>
          <input type="text" name="medio_contacto" id="medio_contacto" class="datos" data-obligatorio="true">
        </div>
        <button name="envio" type="button" class="btn-form" onClick="ABC('medios_contacto','alta_medios_contacto','insert')" id="btnAction">INSERTAR</button>
      </form>
      <div id="respuesta_form_ok" style="display:none">
        <h1 class="text-center text-success">ACCIÓN REALIZADA CORRECTAMENTE</h1>
        </h3>
      </div>
      <div id="respuesta_form_error" style="display:none;">
          <h1 class="text-center text-danger">ERROR AL EJECUTAR LA ACCIÓN</h1>
      </div>
      <div class="result"></div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="../js/animatescroll.min.js"></script>
    <script src="../js/summernote.js"></script>
    <script src="../js/main.js"></script>
  </body>
</html>
