<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
<title></title>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
<meta property="fb:pages" content=""/>
<meta property="fb:app_id" content=""/>

<meta name="author" content="Portal Odontólogos S.A. de C.V:">
<meta name="subject" content="Rediseñando tu imagen">
<meta http-equiv="Expires" content="never">
<meta name="Revisit-After" content="1">
<meta name="distribution" content="Global">
<meta name="Robots" content="INDEX,FOLLOW">
<meta name="country" content="México">
<meta name="Language" content="ES">


<!-- https://realfavicongenerator.net/ -->
<link rel="apple-touch-icon" sizes="180x180" href="//portalodontologos.com.mx/img/native/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="//portalodontologos.com.mx/img/native/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="//portalodontologos.com.mx/img/native/favicon/favicon-16x16.png">
<link rel="manifest" href="//portalodontologos.com.mx/img/native/favicon/site.webmanifest">
<link rel="mask-icon" href="//portalodontologos.com.mx/img/native/favicon/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">
<!-- <link rel="shortcut icon" href="//portalodontologos.com.mx/img/native/favicon/favicon.png"> -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
<link rel="stylesheet" href="//portalodontologos.com.mx/css/animations.min.css">
<link href="https://fonts.googleapis.com/css?family=Poppins:300,500,600,700&display=swap" rel="stylesheet">

<link href="//portalodontologos.com.mx/libs/owl-carousel/owl.carousel.min.css" rel="stylesheet">
<link href="//portalodontologos.com.mx/libs/owl-carousel/owl.theme.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="//portalodontologos.com.mx/custom/css/style.css?v=2">

<style>
.btn:focus, .btn:active, button:focus, button:active {
  outline: none !important;
  box-shadow: none !important;
}

#image-gallery .modal-footer{
  display: block;
}

.thumb{
  margin-top: 15px;
  margin-bottom: 15px;
}
</style>
</head>
<body>
<header>
  <div class="container-fluid fondo-color-especial-3 padding0">
    <div class="container menu-top">
      <div class="row padding0">
      <div class="col-md-8 text-left">
        <ul class="list-inline texto-small">
          <li class="list-inline-item"><a class="" href="">Asistente Digital</a></li>
          <li class="list-inline-item"><a class="" href="">Denta Tips</a></li>
          <li class="list-inline-item"><a class="" href="">Inter-medio</a></li>
          <li class="list-inline-item"><a class="" href="">Odontoticket</a></li>
          <li class="list-inline-item"><a class="" href="">Sesiones On-line</a></li>
          <li class="list-inline-item"><a class="" href="">Marketplace</a></li>
        </ul>
      </div>
      <div class="col-md-4 text-right">           
        <ul class="list-inline icon-sociales">
                              <li class="list-inline-item">
                  <a href="//facebook.com/odontologosmx/" class="nounderline" target="_blank">
                    <i class="fab fa-facebook"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="//twitter.com/odontologosmx" class="nounderline" target="_blank">
                    <i class="fab fa-twitter"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="//youtube.com/user/odontologosmx" class="nounderline" target="_blank">
                    <i class="fab fa-youtube"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="//instagram.com/odontologosmx/" class="nounderline" target="_blank">
                    <i class="fab fa-instagram"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="//pinterest.es/odontologosmx/" class="nounderline" target="_blank">
                    <i class="fab fa-pinterest"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="//periscope.tv/odontologosmx" class="nounderline" target="_blank">
                    <i class="fab fa-periscope"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="contacto@odontologos.mx" class="nounderline" target="_blank">
                    <i class="far fa-envelope"></i>                  </a>
                </li>
          </ul>       
      </div>
    </div>
    </div>
  </div>
  <div class="container-fluid fondo-color-especial-4">
    <div class="container menu-top">
      <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light">
          <div class="col-md-3 col-ms">
            <a class="navbar-brand" href="#"><img src="../../img/native/logo-posa-bco-01.png" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          </div>
          <div class="col-md-9">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <form class="form-inline my-2 my-lg-0 float-derecha">
                <input class="form-control mr-sm-2" type="search" placeholder="Buscar" aria-label="Search">
                <ul class="list-group none-style">
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <img src="../../img/native/logo-mexico.png" alt="" style="width: 20px;"> México
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <!-- <a class="dropdown-item" href="#">México</a> -->
                      <a class="dropdown-item" href="#" style="color:black"><img src="../../img/native/logo-argentina.png" alt="" style="width: 20px;"> Argentina</a>
                    </div>
                  </li>
                </ul>
              </form>
            </div>
          </div>
        </nav>
    </div>
  </div>
  <div class="container-fluid fondo-color-especial-5" id="menu" style="z-index: 1;">
    <div class="container menu-top">
      <nav class="navbar navbar-expand-md navbar-light">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Directorios</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Publicaciones</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Eventos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Productos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Clasificados</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Sesiones On-Line</a>
                </li>
            </ul>
        </div>
      </nav>
    </div>
  </div>
</header>

<section class="padding0 margin0">
      <div class="container-fluid img-seccion-interna">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <h1>Publicaciones</h1>
            </div>
          </div>
        </div>
      </div>
      <div class="container padding-top20">
        <div class="row">
          <div class="col-md-12">
              <ol class="breadcrumb">
                <li><small><a href="#"><i class="fas fa-home"></i> /</a></small></li>
                <li><small><a href="#"> Publicaciones /</a></small></li>
                <li class="active"><small>Todos</small></li>
              </ol>
          </div>
        </div>
      </div>
      <div class="container">        
        <div class="row">
          <div class="col-md-9">
            <!-- <h1 name="titulo">Noticias</h1>
            <a href="//portalodontologos.com.mx/sections/informacion/index.php">
              <button type="button" class="btn con-fondo float-right der">Ver Todos</button>
            </a> -->
            <div class="padding20 fondo-color-especial-7">
              <div class="col-sm-12">
                <div class="row">
                  <div class="col-md-12 col-lg-12">
                    <h1 name="titulo">Todos</h1>
                  </div>
                  <div class="col-md-12 col-lg-0 position-derecha">
                    <button class="btn btn-primary active filter-button" data-filter="todo">Todo</button>
                    <button class="btn btn-primary filter-button" data-filter="retratos">Retratos</button>
                    <button class="btn btn-primary filter-button" data-filter="paisajes">Paisajes</button>
                    <button class="btn btn-primary filter-button" data-filter="disenos">Diseños</button>
                    
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-12 col-lg-12">
                  <div class="row">
                      <div class="col-lg-3 col-md-4 col-xs-6 thumb filter retratos">
                         <a class="thumbnail" href="">
                          <article>
                          <div class="container-informacion">
                             <img class="img-thumbnail img-responsive no-border-circulo" src="../../img/contents/informacion/noticias/noticias/interna-dia-cancer-bucal.jpg" alt="retratos">
                           </div>
                         </article>
                         <div class="" style="margin:10px 0;">
                           <button type="button" class="btn con-fondo btn-sm" style="width: 100%;">Comprar</button>
                         </div>
                       </a>
                      </div>
                      <div class="col-lg-3 col-md-4 col-xs-6 thumb filter paisajes">
                          <a class="thumbnail" href="">
                            <article>
                              <div class="container-informacion">
                                 <img class="img-thumbnail img-responsive no-border-circulo" src="../../img/contents/informacion/noticias/noticias/interna-dia-cancer-bucal.jpg" alt="paisajes">
                               </div>
                             </article>
                             <div class="" style="margin:10px 0;">
                               <button type="button" class="btn con-fondo btn-sm" style="width: 100%;">Comprar</button>
                             </div>
                         </a>
                      </div>
                      <div class="col-lg-3 col-md-4 col-xs-6 thumb filter disenos">
                          <a class="thumbnail" href="">
                            <article>
                              <div class="container-informacion">
                                 <img class="img-thumbnail img-responsive no-border-circulo" src="../../img/contents/informacion/noticias/noticias/interna-dia-cancer-bucal.jpg" alt="disenos">
                               </div>
                             </article>
                             <div class="" style="margin:10px 0;">
                               <button type="button" class="btn con-fondo btn-sm" style="width: 100%;">Comprar</button>
                             </div>
                         </a>
                      </div>
                      <div class="col-lg-3 col-md-4 col-xs-6 thumb filter paisajes">
                          <a class="thumbnail" href="">
                            <article>
                              <div class="container-informacion">
                                 <img class="img-thumbnail img-responsive no-border-circulo" src="../../img/contents/informacion/noticias/noticias/interna-dia-cancer-bucal.jpg" alt="paisajes">
                               </div>
                             </article>
                             <div class="" style="margin:10px 0;">
                               <button type="button" class="btn con-fondo btn-sm" style="width: 100%;">Comprar</button>
                             </div>
                         </a>
                      </div>
                      <div class="col-lg-3 col-md-4 col-xs-6 thumb filter disenos">
                          <a class="thumbnail" href="">
                            <article>
                              <div class="container-informacion">
                                 <img class="img-thumbnail img-responsive no-border-circulo" src="../../img/contents/informacion/noticias/noticias/interna-dia-cancer-bucal.jpg" alt="disenos">
                               </div>
                             </article>
                             <div class="" style="margin:10px 0;">
                               <button type="button" class="btn con-fondo btn-sm" style="width: 100%;">Comprar</button>
                             </div>
                         </a>
                      </div>
                      <!-- <div class="col-sm-6 fondo-color-especial-7 col-md-4 col-lg-4">
                         <article>
                          <div class="container-informacion">
                             <img class="img-responsive" src="../../img/contents/informacion/noticias/noticias/interna-dia-cancer-bucal.jpg" alt="">
                           </div>
                         </article>
                         <div class="" style="margin:10px 0;">
                           <button type="button" class="btn con-fondo btn-sm" style="width: 100%;">Comprar</button>
                         </div>
                      </div> -->
                  </div>
              </div>
              <!-- <div class="col-sm-6 col-md-4 col-lg-12">
                  <div class="row">
                      <div class="col-sm-6 fondo-color-especial-7 col-md-4 col-lg-6">
                         <article>
                          <div class="container-informacion img-hover-zoom1 shadow">
                             <img class="img-responsive" src="../../img/contents/informacion/noticias/noticias/interna-dia-cancer-bucal.jpg" alt="">
                           </div>
                           <div class="informacion-all">
                                 <h1>Odontología</h1>
                                 <a href="//portalodontologos.com.mx/sections/informacion/contenido.php?id=2997">
                                   <h2>Día Latinoamericano de la Lucha Contra el ...</h2>
                                 </a>
                                 <ol class="breadcrumb">
                                   <li><small> <i class="fas fa-eye"></i> 4860</small> </li>
                                   <li><small> <i class="fas fa-calendar-alt"> </i> 29 de Noviembre de 2019 </small></li>
                                 </ol>
                                 <p>&nbsp;La&nbsp;Federaci&oacute;n Odontol&oacute;gica Latinoamericana&nbsp;(FOLA) ha establecido en R&iacute;o de Janeiro, en septiembre de 2009, el 5 de diciembre como&nbsp;D&iacute;a Latinoamericano de la Lucha contra el C&aacute;ncer Bucal. Se ...</p>
                            </div>
                         </article>
                         <hr>
                      </div>
                  </div>
              </div> -->
            </div>
          </div><!-- col-9-->
          <div class="col-md-3 fondo-color-especial-7">
              <div id="banners_right" class="img-cuadradas">
                  <!-- BANNER DOBLE -->                  
                  <!-- - - - - - - - - - - - - - - - - - BANNER SLIDER HOME  - - - - - - - - - - - - - - - - -  -->
                    <!-- <section> -->
                  <div class="">
                      <div class="publicidad">
                        <span>PUBLICIDAD</span>
                          <a href="https://www.odontologos.mx/dentidesk" target="_blank">
                            <img src="../../img/contents/publicidad/boxbanner/2019/DENTIDESK/Banner-DDLite.png" alt="">
                          </a>
                      </div>
                      <hr>
                  </div>
                  <!--  </section> -->
                  <!-- - - - - - - - - - - - - - - - - - BANNER LEADERBOARD  - - - - - - - - - - - - - - - - -  -->
                  <div>
                     <img src="../../img/native/publicidad/half_page.jpg" alt="">
                  </div>
                  <hr>
                  <div>
                    <img src="../../img/native/publicidad/box_banner.jpg" alt="">
                  </div>
              </div>
          </div>
        </div><!-- /row-->
      </div><!-- /container-->




</section>

<footer id="footer" class="footer text-white">
  <div class="container-fluid fondo-color-especial-6">
    <div class="container menu-top">
      <div class="row">
        <div class="col-md-12 border-width-bottom margin0">
          <div class="row">
            <div class="col col-md-6 col-lg-8">
              <img src="../../img/native/logo-posa-bco-01.png" alt="" style="width: 200px;">
            </div>
            <div class="col col-md-6 col-lg-4">
              <ul class="text-right text-white">
                <li><strong>Conecta con nocotros</strong></li>
                <li>
        <ul class="list-inline icon-sociales">
                              <li class="list-inline-item">
                  <a href="//facebook.com/odontologosmx/" class="nounderline" target="_blank">
                    <i class="fab fa-facebook"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="//twitter.com/odontologosmx" class="nounderline" target="_blank">
                    <i class="fab fa-twitter"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="//youtube.com/user/odontologosmx" class="nounderline" target="_blank">
                    <i class="fab fa-youtube"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="//instagram.com/odontologosmx/" class="nounderline" target="_blank">
                    <i class="fab fa-instagram"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="//pinterest.es/odontologosmx/" class="nounderline" target="_blank">
                    <i class="fab fa-pinterest"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="//periscope.tv/odontologosmx" class="nounderline" target="_blank">
                    <i class="fab fa-periscope"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="contacto@odontologos.mx" class="nounderline" target="_blank">
                    <i class="far fa-envelope"></i>                  </a>
                </li>
                      </ul>
       
</li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-8 col-lg-8 bloque-a margin0">
          <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-4">
            <h4>Contenido</h4>
            <ul>
              <li><a href="">Noticias</a></li>
              <li><a href="">Proveedores</a></li>
              <li><a href="">Promociones</a></li>
              <li><a href="">Capacitacion</a></li>
              <li><a href="">Sesiones Online</a></li>
              <li><a href="">Bolsa de Trabajo</a></li>
              
            </ul>
          </div>
          <div class="col-sm-12 col-md-6 col-lg-4">
            <h4>Acerca de</h4>
            <ul>
              <li><a href="">Perfiles Portal Odontologos</a></li>
              <li><a href="">Etiquetas</a></li>
              <li><a href="">Especialidades</a></li>
              <li><a href="">Prolíticas de Privacidad</a></li>
              <li><a href="">Políticas de Uso</a></li>
              <li><a href="">Términos y Condiciones</a></li>
            </ul>
          </div>
          <div class="col-sm-12 col-md-6 col-lg-4">
            <h4>Contáctanos</h4>
            <ul>
              <li><a href="">Envíanos un mail</a></li>
              <li><a href="">¿Quienes Somos?</a></li>
              <li><a href="">Servicios</a></li>
              <li><a href="">Anúnciate con Nosotros</a></li>
              <li><a href="">Mail:contacto@odontologos.mx</a></li>
              <li><a href="">Teléfono (55)3449-2468</a></li>
            </ul>
          </div>
          </div>
        </div>
        <div class="col col-md-4 col-lg-4 border-width-left margin0"><br>
          <p class="$margin10-0">Ya somos más de <strong>341,265 odontólogos conectados.</strong>Gracias por formar parte de la mayoria red de profesionales en odontología.</p>
          <p>Recibe Información en tu correo aqui</p>
          <form action="footer_submit" method="get" accept-charset="utf-8">
            <input type="text" placeholder="Tu correo Aquí" style="width: 100%;">
          </form>
        </div>
      </div>    
    </div>
  </div>
  <div class="container-fluid fondo-color-especial-3">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-8">
          <small>Portal Odontológos es una marca registrada y es propiedad de Portal Odontologos S.A. de C.V. Derecho Reservados</small>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
          <a href="">Desarrollado por Portal Odontólogos</a>
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- GOOGLE ANALYTICS -->

<!-- <script>
       $('.portfolio-item').isotope({
          itemSelector: '.item',
          layoutMode: 'fitRows'
         });
         $('.portfolio-menu ul li').click(function(){
          $('.portfolio-menu ul li').removeClass('active');
          $(this).addClass('active');
          
          var selector = $(this).attr('data-filter');
          $('.portfolio-item').isotope({
            filter:selector
          });
          return  false;
         });
         $(document).ready(function() {
         var popup_btn = $('.popup-btn');
         popup_btn.magnificPopup({
         type : 'image',
         gallery : {
          enabled : true
         }
         });
         });
</script> -->
<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="//portalodontologos.com.mx/libs/owl-carousel/owl.carousel.min.js"></script>
<script src="//portalodontologos.com.mx/libs/sticky/sticky.min.js"></script>
<script src="//portalodontologos.com.mx/js/main.min.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-11455291-7', 'auto');
  ga('send', 'pageview');

</script>

<!-- /GOOGLE ANALYTICS -->
<script>
  let modalId = $('#image-gallery');

$(document)
  .ready(function () {

    loadGallery(true, 'a.thumbnail');

    //This function disables buttons when needed
    function disableButtons(counter_max, counter_current) {
      $('#show-previous-image, #show-next-image')
        .show();
      if (counter_max === counter_current) {
        $('#show-next-image')
          .hide();
      } else if (counter_current === 1) {
        $('#show-previous-image')
          .hide();
      }
    }

    /**
     *
     * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
     * @param setClickAttr  Sets the attribute for the click handler.
     */

    function loadGallery(setIDs, setClickAttr) {
      let current_image,
        selector,
        counter = 0;

      $('#show-next-image, #show-previous-image')
        .click(function () {
          if ($(this)
            .attr('id') === 'show-previous-image') {
            current_image--;
          } else {
            current_image++;
          }

          selector = $('[data-image-id="' + current_image + '"]');
          updateGallery(selector);
        });

      function updateGallery(selector) {
        let $sel = selector;
        current_image = $sel.data('image-id');
        $('#image-gallery-title')
          .text($sel.data('title'));
        $('#image-gallery-image')
          .attr('src', $sel.data('image'));
        disableButtons(counter, $sel.data('image-id'));
      }

      if (setIDs == true) {
        $('[data-image-id]')
          .each(function () {
            counter++;
            $(this)
              .attr('data-image-id', counter);
          });
      }
      $(setClickAttr)
        .on('click', function () {
          updateGallery($(this));
        });
    }
  });

// build key actions
$(document)
  .keydown(function (e) {
    switch (e.which) {
      case 37: // left
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
          $('#show-previous-image')
            .click();
        }
        break;

      case 39: // right
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
          $('#show-next-image')
            .click();
        }
        break;

      default:
        return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
  });

//Filter Button

$(document).ready(function(){

  $(".filter-button").click(function(){
      var value = $(this).attr('data-filter');
      
      if(value == "todo")
      {
          //$('.filter').removeClass('hidden');
          $('.filter').show('1000');
      }
      else
      {
//            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
//            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
          $(".filter").not('.'+value).hide('3000');
          $('.filter').filter('.'+value).show('3000');
          
      }
  });

});

</script>
</body>
</html>