<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
<title>Conferencias y Seminarios</title>
<meta name="description" content="
&nbsp;
La&nbsp;Federaci&oacute;n Odontol&oacute;gica Latinoamericana&nbsp;(FOLA) ha establecido en R&iacute;o de Janeiro, en septiembre de 2009, el 5 de diciembre como&nbsp;D&iacute;a Latinoamericano de la Lucha contra el C&aacute;ncer Bucal. Se ..." />
<meta name="keywords" content="" />
<meta property="og:type" content="website" />
<meta property="og:title" content='DÃ­a Latinoamericano de la Lucha Contra el CÃ¡ncer Bucal'/>
<meta property="og:url" content="http://portalodontologos.com.mx/sections/informacion/contenido.php?id=2997" />
<meta property="og:image" content="http://portalodontologos.com.mx/img/noticias/interna-dia-cancer-bucal.jpg"/>
<meta property="og:image:secure_url" content="http://portalodontologos.com.mx/img/noticias/interna-dia-cancer-bucal.jpg"/>
<meta name="og:description" content='
&nbsp;
La&nbsp;Federaci&oacute;n Odontol&oacute;gica Latinoamericana&nbsp;(FOLA) ha establecido en R&iacute;o de Janeiro, en septiembre de 2009, el 5 de diciembre como&nbsp;D&iacute;a Latinoamericano de la Lucha contra el C&aacute;ncer Bucal. Se ...' />
<meta itemprop="image" content="http://portalodontologos.com.mx/img/noticias/interna-dia-cancer-bucal.jpg" />
<meta name="twitter:card" content="photo" />
<meta name="twitter:site" content="" />
<meta name="twitter:title" content='DÃ­a Latinoamericano de la Lucha Contra el CÃ¡ncer Bucal' />
<meta name="twitter:description" content='
&nbsp;
La&nbsp;Federaci&oacute;n Odontol&oacute;gica Latinoamericana&nbsp;(FOLA) ha establecido en R&iacute;o de Janeiro, en septiembre de 2009, el 5 de diciembre como&nbsp;D&iacute;a Latinoamericano de la Lucha contra el C&aacute;ncer Bucal. Se ...' />
<meta name="twitter:image" content="http://portalodontologos.com.mx/img/noticias/interna-dia-cancer-bucal.jpg" />
<meta name="twitter:url" content="http://portalodontologos.com.mx/sections/informacion/contenido.php?id=2997" />
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
<meta property="fb:pages" content=""/>
<meta property="fb:app_id" content=""/>

<meta name="author" content="Portal OdontÃ³logos S.A. de C.V:">
<meta name="subject" content="RediseÃ±ando tu imagen">
<meta http-equiv="Expires" content="never">
<meta name="Revisit-After" content="1">
<meta name="distribution" content="Global">
<meta name="Robots" content="INDEX,FOLLOW">
<meta name="country" content="MÃ©xico">
<meta name="Language" content="ES">


<!-- https://realfavicongenerator.net/ -->
<link rel="apple-touch-icon" sizes="180x180" href="//portalodontologos.com.mx/img/native/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="//portalodontologos.com.mx/img/native/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="//portalodontologos.com.mx/img/native/favicon/favicon-16x16.png">
<link rel="manifest" href="//portalodontologos.com.mx/img/native/favicon/site.webmanifest">
<link rel="mask-icon" href="//portalodontologos.com.mx/img/native/favicon/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">
<!-- <link rel="shortcut icon" href="//portalodontologos.com.mx/img/native/favicon/favicon.png"> -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
<link rel="stylesheet" href="//portalodontologos.com.mx/css/animations.min.css">
<link href="https://fonts.googleapis.com/css?family=Poppins:300,500,600,700&display=swap" rel="stylesheet">

<link href="//portalodontologos.com.mx/libs/owl-carousel/owl.carousel.min.css" rel="stylesheet">
<link href="//portalodontologos.com.mx/libs/owl-carousel/owl.theme.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="//portalodontologos.com.mx/custom/css/style.css?v=2">

<!--[if lt IE 9]>
<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
<![endif]-->

<!--[if lt IE 9]>
<script src="//cdn.jsdelivr.net/css3-mediaqueries/0.1/css3-mediaqueries.min.js"></script>
<![endif]-->
</head>
<body>
<header>
  <div class="container-fluid fondo-color-especial-3 padding0">
    <div class="container menu-top">
      <div class="row padding0">
      <div class="col-md-8 text-left">
        <ul class="list-inline texto-small">
          <!-- <li class="list-inline-item"><a class="" href="">Portal OdontolÃ³gos</a></li> -->
          <li class="list-inline-item"><a class="" href="">Asistente Digital</a></li>
          <li class="list-inline-item"><a class="" href="">Denta Tips</a></li>
          <li class="list-inline-item"><a class="" href="">Inter-medio</a></li>
          <li class="list-inline-item"><a class="" href="">Odontoticket</a></li>
          <li class="list-inline-item"><a class="" href="">Sesiones On-line</a></li>
          <li class="list-inline-item"><a class="" href="">Marketplace</a></li>
        </ul>
      </div>
      <div class="col-md-4 text-right">

        <ul class="list-inline icon-sociales">
                              <li class="list-inline-item">
                  <a href="//facebook.com/odontologosmx/" class="nounderline" target="_blank">
                    <i class="fab fa-facebook"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="//twitter.com/odontologosmx" class="nounderline" target="_blank">
                    <i class="fab fa-twitter"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="//youtube.com/user/odontologosmx" class="nounderline" target="_blank">
                    <i class="fab fa-youtube"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="//instagram.com/odontologosmx/" class="nounderline" target="_blank">
                    <i class="fab fa-instagram"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="//pinterest.es/odontologosmx/" class="nounderline" target="_blank">
                    <i class="fab fa-pinterest"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="//periscope.tv/odontologosmx" class="nounderline" target="_blank">
                    <i class="fab fa-periscope"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="contacto@odontologos.mx" class="nounderline" target="_blank">
                    <i class="far fa-envelope"></i>                  </a>
                </li>
                      </ul>

      </div>
      </div>
    </div>
  </div>
  <div class="container-fluid fondo-color-especial-4">
    <div class="container menu-top">
      <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light">
          <div class="col-md-3 col-ms">
            <a class="navbar-brand" href="#"><img src="../../img/native/logo-posa-bco-01.png" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          </div>
          <div class="col-md-9">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <form class="form-inline my-2 my-lg-0 float-derecha">
                <input class="form-control mr-sm-2" type="search" placeholder="Buscar" aria-label="Search">
                <ul class="list-group none-style">
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <img src="../../img/native/logo-mexico.png" alt="" style="width: 20px;"> MÃ©xico
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <!-- <a class="dropdown-item" href="#">MÃ©xico</a> -->
                      <a class="dropdown-item" href="#" style="color:black"><img src="../../img/native/logo-argentina.png" alt="" style="width: 20px;"> Argentina</a>
                    </div>
                  </li>
                </ul>
              </form>
            </div>
          </div>
        </nav>
    </div>
  </div>
  <div class="container-fluid fondo-color-especial-5" id="menu" style="z-index: 1;">
    <div class="container menu-top">
      <nav class="navbar navbar-expand-md navbar-light">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Directorios</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Publicaciones</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Eventos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Productos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Clasificados</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Sesiones On-Line</a>
                </li>
            </ul>
        </div>
      </nav>
    </div>
  </div>
</header>


  <!-- - - - - - - - - - - - - - - - - - VISTA HOME  - - - - - - - - - - - - - - - - - -->
  <section class="padding0 margin0">
      <div class="container-fluid img-seccion-interna">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <h1>Conferencias y Seminarios</h1>
            </div>
          </div>
        </div>
      </div>
      <div class="container"><!--padding-top20-->
        <div class="row">
          <div class="col-md-12">
              <ol class="breadcrumb">
                <li><small><a href="#"><i class="fas fa-home"></i> /</a></small></li>
                <li><small><a href="#"> Publicaciones /</a></small></li>
                <li class="active"><small>Todos</small></li>
              </ol>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-9">
              <div class="padding30 fondo-color-especial-7">
                  <div class="col-sm-6 col-md-4 col-lg-12">
                    <article>
                        <div class="informacion-all">
                          <h1 name="titulo" title="titulo-subseccion">Expo Med 2020</h1>
                                <div class="row margin0 padding0">
                                  <div class="col-lg-12 margin0 padding0">
                                    <ol class="breadcrumb">
                                      <li><small><i class="fas fa-calendar-alt"></i> 09 de Junio de 2020 - 11 de junio de 2020 </small></li>
                                      <li><small> | </small></li>
                                      <li><small><i class="fas fa-map-marker-alt"> </i> Centro Citibanamex </small></li>
                                    </ol>
                                  </div>
                                  <div class="col-lg-12 margin0 padding0 "><!--float-right-->
                                    <h1><a href="" class="text-uppercase">Conferencias y Seminarios</a></h1>
                                  </div>
                                </div>
                          </div>
                    </article>
                  </div>
                  <div class="col-sm-6 col-md-4 col-lg-12">
                    <div class="row">
                      <a href="">
                        <img  class="no-border-circulo img-responsive" title="primera" src="../../img/contents/informacion/noticias/noticias/tecnologia-salud-inter.jpg" alt="">
                      </a>
                      <br>
                      <button  type="button" class="fondo-color-especial-8 btn con-fondo text-uppercase tam-completo margin30 no-border-circulo">Link de Registro</button>
                      <!-- <h3 class="text-left">Expomed te invita a nuestra exposición líder del Sector Salud en México y Centroamérica.</h3> -->
                      <p class="text-left p-font-medio">Expomed te invita a nuestra exposición líder del Sector Salud en México y Centroamérica.</p><br>
                      <p><strong class="text-uppercase">datos de contacto</strong></p><br>
                      <p>E-mail:<a href="mailto:expomed@ubm.com">expomed@ubm.com</a></p>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-4 col-lg-12">
                    <div class="row">
                       <div class="col-lg-12 text-center">
                          <button type="button" class="btn con-fondo btn-sm"><i class="fas fa-calendar-alt"></i> Enviar E-mail </button>
                       </div>
                       <hr>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <ul class="list-inline icon-sociales ">
                      <li class="list-inline-item color-contrario">
                        Compartirt
                      </li>
                      <li class="list-inline-item color-contrario">
                        <a href="//facebook.com/odontologosmx/" class="nounderline" target="_blank">
                          <i class="fab fa-facebook"></i>                  </a>
                      </li>
                      <li class="list-inline-item color-contrario">
                        <a href="//twitter.com/odontologosmx" class="nounderline" target="_blank">
                          <i class="fab fa-twitter"></i>                  </a>
                      </li>
                      <li class="list-inline-item color-contrario">
                        <a href="contacto@odontologos.mx" class="nounderline" target="_blank">
                          <i class="far fa-envelope"></i>                  </a>
                      </li>
                    </ul>
                  </div>
                  <div class="col-lg-12">
                      <div class="row">
                            <div class="">
                              TAGS
                            </div>
                            <ul class="list-inline">
                              <li href="#">
                                <button type="button" class="btn sin_fondo">CÃ¡ncer Bucal</button>
                              </li>
                            </ul>
                      </div>
                  </div>
                  <div class="col-lg-12 border rounded fondo-color-especial-5">
                      <div class="row">
                        <div class="col-md-3">
                          <img class="rounded-circle" src="http://lorempixel.com/60/60/" alt="">
                        </div>
                        <div class="col-md-9">
                          <div class="informacion-all">
                              <a href="//portalodontologos.com.mx/sections/informacion/contenido.php?id=3219">
                                  <h2>Federación Odontologica de Buenos Aires</h2>
                              </a>
                              <p></p>
                              <ul class="list-inline icon-sociales ">
                                  <li class="list-inline-item color-contrario">
                                    Contáctanos
                                  </li>
                                  <li class="list-inline-item color-contrario">
                                    <a href="//facebook.com/odontologosmx/" class="nounderline" target="_blank">
                                      <i class="fab fa-facebook"></i></a>
                                  </li>
                                  <li class="list-inline-item color-contrario">
                                    <a href="//twitter.com/odontologosmx" class="nounderline" target="_blank">
                                      <i class="fab fa-twitter"></i></a>
                                  </li>
                                  <li class="list-inline-item color-contrario">
                                    <a href="contacto@odontologos.mx" class="nounderline" target="_blank">
                                      <i class="far fa-envelope"></i></a>
                                  </li>
                              </ul>
                                           <!-- <p><small>13 de Noviembre de 2019</small></p> -->
                            </div>
                        </div>
                      </div>
                    </div>

            </div>
          </div>
          <div class="col-md-3 fondo-color-especial-7">

          </div>
        </div>
      </div>
</section>


<footer id="footer" class="footer text-white">
  <div class="container-fluid fondo-color-especial-6">
    <div class="container menu-top">
      <div class="row">
        <div class="col-md-12 border-width-bottom margin0">
          <div class="row">
            <div class="col col-md-6 col-lg-8">
              <img src="../../img/native/logo-posa-bco-01.png" alt="" style="width: 200px;">
            </div>
            <div class="col col-md-6 col-lg-4">
              <ul class="text-right text-white">
                <li><strong>Conecta con nocotros</strong></li>
                <li>
        <ul class="list-inline icon-sociales">
                              <li class="list-inline-item">
                  <a href="//facebook.com/odontologosmx/" class="nounderline" target="_blank">
                    <i class="fab fa-facebook"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="//twitter.com/odontologosmx" class="nounderline" target="_blank">
                    <i class="fab fa-twitter"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="//youtube.com/user/odontologosmx" class="nounderline" target="_blank">
                    <i class="fab fa-youtube"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="//instagram.com/odontologosmx/" class="nounderline" target="_blank">
                    <i class="fab fa-instagram"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="//pinterest.es/odontologosmx/" class="nounderline" target="_blank">
                    <i class="fab fa-pinterest"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="//periscope.tv/odontologosmx" class="nounderline" target="_blank">
                    <i class="fab fa-periscope"></i>                  </a>
                </li>
                              <li class="list-inline-item">
                  <a href="contacto@odontologos.mx" class="nounderline" target="_blank">
                    <i class="far fa-envelope"></i>                  </a>
                </li>
                      </ul>

</li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-8 col-lg-8 bloque-a margin0">
          <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-4">
            <h4>Contenido</h4>
            <ul>
              <li><a href="">Noticias</a></li>
              <li><a href="">Proveedores</a></li>
              <li><a href="">Promociones</a></li>
              <li><a href="">Capacitacion</a></li>
              <li><a href="">Sesiones Online</a></li>
              <li><a href="">Bolsa de Trabajo</a></li>

            </ul>
          </div>
          <div class="col-sm-12 col-md-6 col-lg-4">
            <h4>Acerca de</h4>
            <ul>
              <li><a href="">Perfiles Portal Odontologos</a></li>
              <li><a href="">Etiquetas</a></li>
              <li><a href="">Especialidades</a></li>
              <li><a href="">ProlÃ­ticas de Privacidad</a></li>
              <li><a href="">PolÃ­ticas de Uso</a></li>
              <li><a href="">TÃ©rminos y Condiciones</a></li>
            </ul>
          </div>
          <div class="col-sm-12 col-md-6 col-lg-4">
            <h4>ContÃ¡ctanos</h4>
            <ul>
              <li><a href="">EnvÃ­anos un mail</a></li>
              <li><a href="">Â¿Quienes Somos?</a></li>
              <li><a href="">Servicios</a></li>
              <li><a href="">AnÃºnciate con Nosotros</a></li>
              <li><a href="">Mail:contacto@odontologos.mx</a></li>
              <li><a href="">TelÃ©fono (55)3449-2468</a></li>
            </ul>
          </div>
          </div>
        </div>
        <div class="col col-md-4 col-lg-4 border-width-left margin0"><br>
          <p class="$margin10-0">Ya somos mÃ¡s de <strong>341,265 odontÃ³logos conectados.</strong>Gracias por formar parte de la mayoria red de profesionales en odontologÃ­a.</p>
          <p>Recibe InformaciÃ³n en tu correo aqui</p>
          <form action="footer_submit" method="get" accept-charset="utf-8">
            <input type="text" placeholder="Tu correo AquÃ­" style="width: 100%;">
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid fondo-color-especial-3">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-8">
          <small>Portal OdontolÃ³gos es una marca registrada y es propiedad de Portal Odontologos S.A. de C.V. Derecho Reservados</small>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
          <a href="">Desarrollado por Portal OdontÃ³logos</a>
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- GOOGLE ANALYTICS -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-11455291-7', 'auto');
  ga('send', 'pageview');

</script>

<!-- /GOOGLE ANALYTICS -->

<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="//portalodontologos.com.mx/libs/owl-carousel/owl.carousel.min.js"></script>
<script src="//portalodontologos.com.mx/libs/sticky/sticky.min.js"></script>
<script src="//portalodontologos.com.mx/js/main.min.js"></script>
</body>
</html>
