<?php
include ("../includes/inicio.php");
include ("../includes/config.php");
include ("../includes/modelo.php");
include ("../includes/modeloGlobal.php");
$obj = new modelo();
$objGlobal=new modeloGlobal();
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
<?php
$id_seccion="8";
$SEO=$obj->SEO($id_seccion,false);
foreach($SEO as $SEO){ ?>
<title><?=utf8_encode($SEO['Titulo']); ?></title>
<meta name="description" content="<?=utf8_encode($SEO['Descripcion']); ?>" />
<meta name="keywords" content="<?=utf8_encode($SEO['Keywords']); ?>" />
<?php  } ?>
<? include ("../includes/tags_styles.php"); ?>
</head>
<body>
<?php include '../header.php';?>

      <!-- SECCIÓN CONTACTO -->
        <div class="container fondo-blanco">
          <div class="row">
            <section class="col-md-9 margin100botton">
              <h1 class="contacto text-center"><img src="../img/ave-contacto.png" alt="">CONTACTO</h1>
              <div class="interno">
                <h1>ESCRÍBENOS, TU OPINIÓN NOS INTERESA</h1>
              </div>
              <h4>En breve nos pondrémos en contacto contigo</h4>
              <h3><?=$tel_sitio;?></h3>
              <!-- Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. -->
              <hr>
              <div class="margin40top" style="background: #d2d2d2; padding:30px;">
                <?php
                $destino=$link_email_sitio;
                $subject='Envío de Formulario de Contacto '.$nombre_sitio;
                $log_gc='0';
                $respuesta_email='0';
                include ("../prints/form_contacto.php");
                ?>
              </div>
            </section>
            <aside class="col-md-3">
              <?php include ('../aside.php');?>
            </aside>
          </div>
        </div>
      <!-- SECCIÓN CONTACTO-->

      <script src="../js/formulario.js"></script>
      <script src="https://www.google.com/recaptcha/api.js"></script>
      <script type="text/javascript">
      var onReturnCallback = function(response) {
      			$.ajax({
      					type: 'POST',
      					url: "../includes/googlevalidate.php", // The file we're making the request to
      					dataType: 'html',
      					async: true,
      					data: {
      							captchaResponse: $("#g-recaptcha-response").val() // The generated response from the widget sent as a POST parameter
      					},
      					success: function (data) {
      						//alert("validaciones correctas de captcha");
      						$("#btnEnviar").show();
      					},
      					error: function (XMLHttpRequest, textStatus, errorThrown) {
      							alert("Debes validar que no eres un robot");
      					}
      			});
      }; // end of onReturnCallback
      </script>

<?php $fondo_footer='img-footer3'; include ('../footer.php');?>
<?php include ("../includes/tags_functions.php"); ?>
</body>
<?php
include ("../includes/fin.php");
?>
</html>
