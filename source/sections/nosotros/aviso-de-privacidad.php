<?php
include ("../includes/inicio.php");
include ("../includes/config.php");
include ("../includes/modelo.php");
include ("../includes/modeloGlobal.php");
$obj = new modelo();
$objGlobal=new modeloGlobal();
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
<?php
$id_seccion="5";
$SEO=$obj->SEO($id_seccion);
foreach($SEO as $SEO){ ?>
<title><?=utf8_encode($SEO['Titulo']); ?></title>
<meta name="description" content="<?=utf8_encode($SEO['Descripcion']); ?>" />
<meta name="keywords" content="<?=utf8_encode($SEO['Keywords']); ?>" />
<?php  } ?>
<? include ("../includes/tags_styles.php"); ?>
</head>
<body>
<?php include '../header.php';?>

      <!-- SECCIÓN CONTACTO -->
        <div class="container fondo-blanco">
          <div class="row">
            <section class="col-md-9 margin100botton">
              <div class="interno">
                <h1>AVISO DE PRIVACIDAD</h1>
              </div>
              <hr>
              <div class="margin40top">
                <p>          El aviso de  privacidad forma parte del uso del sitio web <a href="<?=$protocolo.$path;?>"><?=$protocolo.$path;?></a>.<br />
                  </p>
                <p><strong>RESPONSABLE</strong> <br />
                  Somos una Sociedad Anónima de Capital Variable.<br>
                  Nuestro correo de contacto es<a href="mailto:<?=$link_email_sitio;?>"> <?=$link_email_sitio;?></a> y nuestro teléfono de contacto (55) 5349-2468 <br />
                  Una de las  prioridades de <?=$nombre_sitio;?> <?=$nombre_sitio_empresa;?> (en adelante, <?=$nombre_sitio;?>) es respetar la  privacidad de sus usuarios y mantener segura la información y los datos  personales que recolecta.<br />
                  Asimismo, <?=$nombre_sitio;?> informará al  usuario qué tipo de datos recolecta, cómo los almacena, la finalidad del  archivo, cómo los protege, el alcance de su compromiso de confidencialidad y  los derechos que éste posee como titular de la información.<br />
                  </p>
                <p><strong>DATOS PERSONALES</strong> <br />
                  En <?=$nombre_sitio;?> recogemos  información desde varias áreas de nuestros sitios web. Para cada uno de estos  sitios, la información que se solicita es distinta y se almacena en bases de  datos separadas. ésta deberá ser veraz y completa. <?=$nombre_sitio;?> no recaba datos sensibles en ningún momento, ni vía telefónica ni en formularios.<br />
                  El usuario  responderá en todo momento por los datos proporcionados y en ningún caso&nbsp; <?=$nombre_sitio;?> será responsable de la veracidad de los mismos. A continuación se enlista de  forma concisa y completa los datos personales que <?=$nombre_sitio;?> recaba.<br />
                  En el  registro se pide: </p>
                <ul>
                  <li>Sexo</li>
                  <li>Edad</li>
                  <li>Nombre y Apellidos </li>
                  <li>Email<br />
                    Teléfono</li>
                  <li>Estado</li>
                  <li>Delegación/Municipio</li>
                  <li>País</li>
                </ul>
                <p><br />
                  Contacto:</p>
                <ul>
                  <li>Nombre</li>
                  <li>E-mail</li>
                  <li>Teléfono</li>
                </ul>
                <p>En el formulario de Registro para recibir información: </p>
                <ul>
                  <li>Profesión o Giro de la Empresa</li>
                  <li>Sexo</li>
                  <li>Edad</li>
                  <li>Nombre Completo</li>
                  <li>Email</li>
                  <li>Teléfono</li>
                  <li>Estado</li>
                  <li>Delegación/Municipio</li>
                  <li>País</li>
                  <li>Medios de promoción</li>
                  <li>Semestre Universidad</li>
                </ul>
                <p><strong>QUÉ SON LOS COOKIES Y CÓMO SE UTILIZAN</strong> <br />
                  Los cookies son pequeñas piezas de información que son enviadas por el  sitio Web a su navegador y se almacenan en el disco duro de su equipo y se  utilizan para determinar sus preferencias cuando se conecta a los servicios de  nuestros sitios, así como para rastrear determinados comportamientos o  actividades llevadas a cabo por usted dentro de nuestros sitios.<br />
                  Los cookies nos permiten: a) reconocerlo al momento de entrar a  nuestros sitios y ofrecerle de una experiencia personalizada, b) conocer la  configuración personal del sitio especificada por usted, por ejemplo, los  cookies nos permiten detectar el ancho de banda que usted ha seleccionado al  momento de ingresar al home page de nuestros sitios, de tal forma que sabemos  qué tipo de información es aconsejable descargar, c) calcular el tamaño de  nuestra audiencia y medir algunos parámetros de tráfico, pues cada navegador  que obtiene acceso a nuestros sitios adquiere un cookie que se usa para  determinar la frecuencia de uso y las secciones de los sitios visitadas,  reflejando así sus hábitos y preferencias, información que nos es útil para  mejorar el contenido, los titulares y las promociones para los usuarios.<br />
                  Los cookies también nos ayudan a rastrear algunas actividades, por  ejemplo, en algunas de las encuestas que lanzamos en línea, podemos utilizar  cookies para detectar si el usuario ya ha llenado la encuesta y evitar  desplegarla nuevamente, en caso de que lo haya hecho. Sin embargo, las cookies  le permitirán tomar ventaja de las características más benéficas que le  ofrecemos, por lo que le recomendamos que las deje activadas.<br />
                  La utilización de cookies no será utilizada para identificar a los  usuarios, con excepción de los casos en que se investiguen posibles actividades  fraudulentas.<br />
                  Se utilizan web beacons en los correos electrónicos que mandamos para  poder medir el impacto de nuestras campañas y así poder entregarle al cliente  resultados estadísticos fidedignos. La información que se le otorga al cliente  es meramente estadística sin proporcionar información de los usuarios que se  arrojaron en el resultado. </p>
                <p>Para desactivar las cookies, las pueden bloquear desde el navegador que utilicen.</p>
                <p><strong>USO DE LA INFORMACIÓN</strong> <br />
                  La información solicitada permite a <?=$nombre_sitio;?> contactar a los  usuarios y potenciales clientes además de agregarlos a la base de datos para  que reciban información acerca de los anunciantes de <?=$nombre_sitio;?>; no  haciéndonos responsables por el contenido de la información, promociones, etcétera,  enviada por ellos. El correo utilizado para mandar información acerca de los anunciantes es: <?=$link_email_sitio;?>.</p>
        <p>Asimismo <?=$nombre_sitio;?> utilizará la información obtenida para:</p>
                <ul>
                  <li>Informar sobre nuevos productos o servicios que estén relacionados con  el medio <?=$info_medio;?></li>
                  <li>Envío de Boletínes Semanales y de Próximos Eventos.</li>
                  <li>Envíos patrocinados por los anunciantes.</li>
                  <li>Informar sobre cambios de nuestros productos o servicios.</li>
                  <li>Promover productos y servicios de la empresa.</li>
                  <li>Proveer una mejor atención al usuario.</li>
                  <li>Realización de Encuestas.</li>
                  <li>Comercialización de productos en llinea.</li>
                </ul>
                <p><strong>PROTECCIÓN</strong><br />
                  <br />
                  La empresa implementará las  herramientas necesarias para proteger la información. Los datos están  asegurados por un número de identificación personal, al cual sólo el usuario  tendrá acceso.</p>
                <p><strong>LIMITACIÓN  DE USO Y DIVULGACIÓN DE INFORMACIÓN</strong> <br />
                  En nuestro programa de notificación de promociones, ofertas y servicios  a través de correo electrónico, sólo <?=$nombre_sitio;?> tiene acceso a la  información recabada. Este tipo de publicidad se realiza mediante avisos y  mensajes promocionales de correo electrónico, los cuales sólo serán enviados a  usted y a aquellos contactos registrados para tal propósito, esta indicación  podrá usted modificarla en cualquier momento o enviando un correo a <a href="mailto:<?=$link_email_sitio;?>"><?=$link_email_sitio;?></a>. En los correos  electrónicos enviados, pueden incluirse ocasionalmente ofertas de terceras  partes que sean nuestros socios comerciales.<br />
                  </p>
                <p><strong>DERECHOS ARCO (ACCESO, RECTIFICACIÓN, CANCELACIÓN Y OPOSICIÓN)</strong> <br />
                Los datos personales proporcionados por el usuario formarán parte de un  archivo que contendrá su perfil. El usuario podrá  modificar su perfil en cualquier  momento enviándonos un correo a <a href="mailto:<?=$link_email_sitio;?>"><?=$link_email_sitio;?></a> Atenderemos su solicitud en no más de 20 dí­as hábiles.</p>
                <p>Para revocar el consentimiento al tratamiento de sus datos póngase en contacto mandando un correo a <a href="mailto:<?=$link_email_sitio;?>"><?=$link_email_sitio;?></a> o  comunicándose al teléfono (55) 2628-3216 con Humberto Cardoso Tinoco y en un lapso no mayor a 24 hrs hábiles, será dado de baja del sistema.<br />
                  <?=$nombre_sitio;?> aconseja al usuario que actualice sus datos cada vez  que éstos sufran alguna modificación, ya que esto permitirá brindarle un  servicio más personalizado.<br />
                </p>
                <p><strong>TRANSFERENCIAS DE INFORMACIÓN CON TERCEROS</strong> <br />
                  La empresa NO venderá, cederá ni transferirá la información obtenida de  los usuarios. Pero si podrá utilizarla para resolver disputas y problemas.</p>
                <p>TODA la información que  pudieran recibir será enviada através de la cuenta de correo: <?=$link_email_sitio;?>. Los anunciantes, nunca tienen acceso a la información de los usuarios.<br />
                  <br />
                La empresa NO revelará información a terceros salvo cuando: </p>
                <ul type="square">
                  <li>El usuario lo autorice expresamente. </li>
                  <li>Sea requerido por ley, decreto o resolución       administrativa. </li>
                  <li>Sea en cumplimiento de una resolución       judicial. </li>
                  <li>Se utilice el sitio web para desarrollar       actividades ilegales, causar daños o perjuicios a la empresa, a sus       bienes, terceros o a otros usuarios. </li>
                </ul>
                <p><strong>CAMBIOS EN EL AVISO DE PRIVACIDAD</strong> <br />
                  <?=$nombre_sitio;?> se reserva el derecho de efectuar en cualquier  momento modificaciones o actualizaciones al presente aviso de privacidad, para  la atención de novedades legislativas o jurisprudenciales, políticas internas,  nuevos requerimientos para la prestación u ofrecimiento de nuestros servicios o  productos y prácticas del mercado. <br />
                  <br />
                  Estas modificaciones estarán  disponibles al público a través de los siguientes medios: (i) en nuestra página  de Internet [<a href="<?=$protocolo.$path;?>"><?=$protocolo.$path;?></a>, sección <strong><em>aviso de privacidad</em></strong>]; (ii) o se  las haremos llegar al último correo electrónico que nos haya proporcionado. <br />
                  <br />
                  <strong>ACEPTACIÓN DE LOS TÉRMINOS</strong> <br />
                  Esta declaración de Confidencialidad / Privacidad está sujeta a los  términos y condiciones de todos los sitios web de <?=$nombre_sitio;?> antes descritos,  lo cual constituye un acuerdo legal entre el usuario y <?=$nombre_sitio;?>.<br />
                Si el usuario utiliza los servicios en cualquiera de los sitios de  <?=$nombre_sitio;?>, significa que ha leído, entendido y acordado los términos  antes expuestos. </p>
                <p>Si el usuario considera que han sido vulnerados sus derechos respecto de la protección de datos personales, tiene el derecho de acudir a la autoridad correspondiente para defender su ejercicio. La autoridad es el Instituto Nacional de Transparencia, Acceso a la Información y Protección de Datos Personales (INAI), su sitio web es: www.ifai.mx </p>
        <p>Estimado usuario, se le notifica que el Aviso de Privacidad ha sido  modificado el día 19 de julio de 2017. </p>
              </div>
            </section>
            <aside class="col-md-3">
              <?php include ('../aside.php');?>
            </aside>
          </div>
        </div>

      <!-- SECCIÓN CONTACTO-->

      <script src="../js/formulario.js"></script>
      <script src="https://www.google.com/recaptcha/api.js"></script>
      <script type="text/javascript">
      var onReturnCallback = function(response) {
      			$.ajax({
      					type: 'POST',
      					url: "../includes/googlevalidate.php", // The file we're making the request to
      					dataType: 'html',
      					async: true,
      					data: {
      							captchaResponse: $("#g-recaptcha-response").val() // The generated response from the widget sent as a POST parameter
      					},
      					success: function (data) {
      						//alert("validaciones correctas de captcha");
      						$("#btnEnviar").show();
      					},
      					error: function (XMLHttpRequest, textStatus, errorThrown) {
      							alert("Debes validar que no eres un robot");
      					}
      			});
      }; // end of onReturnCallback
      </script>

<?php $fondo_footer='img-footer3'; include ('../footer.php');?>
<?php include ("../includes/tags_functions.php"); ?>
</body>
<?php
include ("../includes/fin.php");
?>
</html>
