<?php
include ("../../includes/inicio.php");
include ("../../includes/config.php");
require ("../../custom/configAPP.php");
include ("../../includes/modelo.php");
include ("../../includes/modeloGlobal.php");
$obj = new modelo();
$objGlobal=new modeloGlobal();
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
<?php
$id=$_GET['id'];
$parametro=array("id_cat"=>'1', "id"=>$id);
$debug_informacion=false;
$informacion=$obj->getInformacion($bd_site,'info',$parametro,$limit,$paginador,$debug_informacion);
$totalrowsinformacion=count($informacion);
?>
<?php foreach ($informacion as $informacion) { ?>
<?php include ($nivel_ruta."custom/prints/data_productos.php"); ?>
<title><?=$titulo;?></title>
<meta name="description" content="<?=$descripcion?>" />
<meta name="keywords" content="" />
<meta property="og:type" content="website" />
<meta property="og:title" content='<?=$titulo;?>'/>
<meta property="og:url" content="<?=$protocolo;?><?=$link_informacion;?>" />
<meta property="og:image" content="<?=$protocolo;?><?=$path;?>/img/noticias/<?=$informacion['imagen']?>"/>
<meta property="og:image:secure_url" content="<?=$protocolo;?><?=$path;?>/img/noticias/<?=$informacion['imagen']?>"/>
<meta name="og:description" content='<?=$descripcion;?>' />
<meta itemprop="image" content="<?=$protocolo;?><?=$path;?>/img/noticias/<?=$informacion['imagen']?>" />
<meta name="twitter:card" content="photo" />
<meta name="twitter:site" content="<?=$link_twitter_user_sitio;?>" />
<meta name="twitter:title" content='<?=$titulo;?>' />
<meta name="twitter:description" content='<?=$descripcion;?>' />
<meta name="twitter:image" content="<?=$protocolo;?><?=$path;?>/img/noticias/<?=$informacion['imagen']?>" />
<meta name="twitter:url" content="<?=$protocolo;?><?=$link_informacion;?>" />
<?php } ?>
<? include ("../../includes/tags_styles.php"); ?>
</head>
<body>
<?php include ('../../custom/header.php');?>

<?php
$print_view='info';
include ('../../custom/prints/producto.php');
?>

<?php include ('../../custom/footer.php');?>
<?php include ("../../includes/tags_functions.php"); ?>
</body>
<?php
include ("../../includes/fin.php");
?>
</html>
