<?php
include ("../../includes/inicio.php");
include ("../../includes/config.php");
require ("../../custom/configAPP.php");
include ("../../includes/modelo.php");
include ("../../includes/modeloGlobal.php");
$obj = new modelo();
$objGlobal=new modeloGlobal();
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
<?php
$id_seccion="4";
$SEO=$obj->SEO($bd_site,$id_seccion,false);
foreach($SEO as $SEO){ ?>
<title><?=utf8_encode($SEO['Titulo']); ?></title>
<meta name="description" content="<?=utf8_encode($SEO['Descripcion']); ?>" />
<meta name="keywords" content="<?=utf8_encode($SEO['Keywords']); ?>" />
<?php  } ?>
<? include ("../../includes/tags_styles.php"); ?>
</head>
<body>
<?php include ('../../custom/header.php');?>

<?php
$limit="20";
$paginador=1;
$debug_informacion=false;
$productos=$obj->getProductos($bd_site,'list',$parametro,$limit,$paginador,$debug_informacion);
$print_view='section';
include ('../../custom/prints/productos.php');
?>

<?php include ('../../custom/footer.php');?>
<?php include ("../../includes/tags_functions.php"); ?>
</body>
<?php
include ("../../includes/fin.php");
?>
</html>
