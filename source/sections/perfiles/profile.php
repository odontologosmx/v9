<?php
include ("../includes/inicio.php");
include ("../includes/config.php");
include ("../includes/modelo.php");
include ("../includes/modeloGlobal.php");
$obj = new modelo();
$objGlobal=new modeloGlobal();
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<?php
$parametro=$_GET['url'];
$perfil=$obj->getPerfiles($parametro,false);

foreach($perfil as $perfil){
  $id_perfil=utf8_encode($perfil['ID']);
  $nombre_comercial=utf8_encode($perfil['nombre_comercial']);
  $avatar=utf8_encode($perfil['avatar']);
  $imagen_perfil=utf8_encode($perfil['imagen_perfil']);
  $cv_empresarial=utf8_encode($perfil['cv_empresarial']);
  $contenido_personalizado=utf8_encode($perfil['contenido_personalizado']);
  if ($perfil['avatar']!='') {
    $empresa_logo='../img/perfiles/logos/'.$perfil['avatar'];
  } else {
    $empresa_logo='../img/perfiles/logos/sin-logo.jpg';
  }
  if($perfil['imagen_perfil']!='') {
    $background_perfil='../img/perfiles/portadas/'.utf8_encode($perfil['imagen_perfil']);
  } else {
    $rand_img=array('1','2','3','4','5','6'); shuffle($rand_img); $i_img=0;
    $background_perfil='../img/perfiles/portadas/fondo-perfiles-0'.$rand_img[0].'.jpg';
  }
  /*if($perfil['imagen_perfil']==''){
    if($clasificacion=='3')
      $img_biografia=$path."/perfiles/fondo_biografias/pattern_prof".rand(1,5).".jpg";
    else if($clasificacion=='18')
      $img_biografia=$path."/perfiles/fondo_biografias/pattern_exp".rand(1,5).".jpg";
    else
      $img_biografia=$path."/perfiles/fondo_biografias/pattern".rand(1,5).".jpg";
  }else
    $img_biografia=$path."/perfiles/fondo_biografias/".$perfil['imagen_perfil'];
  if($perfil['logo']!=''){
    $img_avatar=$path."/perfiles/logos/".$perfil['logo'];
    $empresa_logo=1;
  }
  $categos=$perfil['id_cat_categos'];
  $tags=$perfil['id_cat_tags'];
  $file=$perfil['file'];
  $link_mitvo=$perfil['link_mitvo'];

  $categos=$obj->getTags_Categos('s_clientes','tags',$id);
  $total_tags=count($categos);
  $count_tags=0;
  foreach($categos as $categos){
    $keywords.=utf8_encode($categos['nombre_tag']);
    $count_tags+=1;
    if ($count_tags1!=$total_tags) $keywords.=', ';
  }
  $categos=$obj->getTags_Categos('s_clientes','categos',$id);
  $total_tags=count($categos);
  $count_tags=0;
  foreach($categos as $categos){
    $keywords.=utf8_encode($categos['nombre_categoria']);
    $count_tags+=1;
    if ($count_tags!=$total_tags) $keywords.=', ';
  }

  $descripcion=strip_tags($cv_empresarial);
  $descripcion=$objGlobal->trim_string($cv_empresarial,100);
  }
  $cliente=$id;*/
  ?>
  <head>
  <title><?=$nombre_comercial; ?> | Perfiles <?=$nombre_sitio;?></title>
  <meta name="description" content="<?=utf8_encode($perfil['Descripcion']); ?>" />
  <meta name="keywords" content="<?=utf8_encode($perfil['Keywords']); ?>" />
  <? include ("../includes/tags_styles.php"); ?>
  <style media="screen">
     .fondo-perfiles {
       background-image: url("<?=$background_perfil;?>");
     }
  </style>
  </head>
  <body>
        <!-- SECCIÓN NOTICIAS -->

        <div class="container fondo-blanco">
          <div class="row">
            <div class="fondo-perfiles">
            </div>
            <div class="container">
              <div class="row">
                <div class="col-md-3">
                  <a href="#"><img class="img-responsive borde-image centrado logo-perfiles fondo-effect" src="<?=$empresa_logo;?>" alt=""></a>
                </div>
                <div class="col-md-9 perfiles">
                  <h1><?=$nombre_comercial;?></h1>
                  <h4>Contáctanos: <?php $datos_contacto=$obj->getMediosContacto($id_perfil,$medio); include ("../prints/datos_contacto.php"); ?></h4>
                  <a onclick="$('#contacto').animatescroll({scrollSpeed:1000, padding:80});"><div class="btn1">
                    Enviar mensaje
                  </div></a>
                </div>
              </div>
            </div>
            <section class="margin100botton">
              <hr>
              <?php if ($cv_empresarial!='' || $contenido_personalizado!='') { ?>
              <div class="text-center margin40top perfiles">
                <div class="title text-center">
                  <h2>Acerca de</h2>
                  <span class="border"></span>
                </div>
              </div>
              <div class="container margin50top">
                <div class="row descripcion">
                  <?=$cv_empresarial;?>
                </div>
                <div class="row descripcion">
                  <?=$contenido_personalizado;?>
                </div>
              </div>
            <?php } ?>
            <hr>
              <section class="container">
                <?php
                $parametro=array("id_cat_info"=>'1', "parametro"=>$parametro);
                $informacion=$obj->getInformacion('profile',$parametro,$limit,$paginador);
                $print_view='list';
                include ("../prints/informacion.php");
                ?>
              </section>
              <section class="container">
                <?php $parametro=array("id_organizador"=>$id_perfil); $calendario=$obj->getCalendario('profile',$parametro,$limit,$paginador,false); $print_view='list'; include ("../prints/calendario.php"); ?>
              </section>
              <section class="container">
                <?php $print_view='list'; $productos=$obj->getProductos('profile',$parametro,$limit,$paginador,false); include ("../prints/productos.php"); ?>
              </section>
              <section class="container">
                <?php $print_view='list'; $promocines=$obj->getPromociones('profile',$parametro,$limit,$paginador,false); include ("../prints/productos.php"); ?>
              </section>
              <section class="container" id="contacto">
               <div class="container text-center">
                 <div class="title text-center">
                   <h2>Contáctanos</h2>
                   <span class="border"></span>
                 </div>
              </div>

              <?php $ubicaciones=$obj->getUbicaciones($id_perfil,'1'); //var_dump($ubicaciones);?>
              <?php foreach ($ubicaciones as $ubicaciones){ $lat=$ubicaciones['lat']; $long=$ubicaciones['long']; $c_map+=1; ?>
                <div class="text-center perfiles">
                  <h3><i class="fas fa-map-marker-alt"></i> <?=utf8_encode($ubicaciones['calle']);?> <?=utf8_encode($ubicaciones['numero']);?> <?=utf8_encode($ubicaciones['colonia']);?> <?=utf8_encode($ubicaciones['ciudad']);?></h3>
                </div>
                <div id="map<?=$c_map?>" class="map"></div>
              <?php } ?>
              <div class="margin20top">
                <?php
                  $subject="Formulario Perfiles ".$nombre_sitio." | ".$nombre_comercial;
                  $datos_contacto=$obj->getMediosContacto($id_perfil,'1');
                  $destino=$datos_contacto[0]['datos_medio_contacto'];
                  $log_gc='1';
                  $respuesta_email='1';
                  $empresa=$nombre_comercial;
                  $id_cliente=$id_perfil;
                  include ("../prints/form_contacto.php");
                ?>
              </div>
              </section>
          </div>
        </div>

      <!-- SECCIÓN NOTICIAS -->

  <footer class="margin50top">
    <div class="area-footer">
      <div class="container margin20top">
        <div class="footer-content d-flex flex-column align-items-center">
          <div class="logo">
            <a href="<?=$path;?>"><img src="<?=$path;?>/img/logo.png" alt=""></a>
          </div>
          <div class="footer-social">
            <a href="<?=$link_facebook_sitio;?>" target="_blank"><i class="fab fa-facebook-square"></i></a>
          </div>
          <div class="copy-right-text">Desarrollado por G13</div>

        </div>
      </div>
    </div>
  </footer>
  <script src="../js/formulario.js"></script>
  <script src="https://www.google.com/recaptcha/api.js"></script>
  <script type="text/javascript">
  var onReturnCallback = function(response) {
        $.ajax({
            type: 'POST',
            url: "../includes/googlevalidate.php", // The file we're making the request to
            dataType: 'html',
            async: true,
            data: {
                captchaResponse: $("#g-recaptcha-response").val() // The generated response from the widget sent as a POST parameter
            },
            success: function (data) {
              //alert("validaciones correctas de captcha");
              $("#btnEnviar").show();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("Debes validar que no eres un robot");
            }
        });
  }; // end of onReturnCallback
  </script>

  <?php include ("../includes/tags_map.php"); ?>
  <?php include ("../includes/tags_functions.php"); ?>
  <script>
  $( document ).ready(function() {
    $('a[data-toggle="tooltip"]').tooltip({
    animated: 'fade',
    placement: 'bottom',
    html:true,
    //trigger:'click'
});
  });
  </script>
  </body>
<?php  } ?>
<?php
include ("../includes/fin.php");
?>
</html>
