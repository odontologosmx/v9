<?php
include ("../includes/inicio.php");
include ("../includes/config.php");
include ("../includes/modelo.php");
include ("../includes/modeloGlobal.php");
$obj = new modelo();
$objGlobal=new modeloGlobal();
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
<?php
$id_info=$_GET['id'];
$parametro=array("id_cat_info"=>'2', "parametro"=>$id_info);
$informacion=$obj->getInformacion('info',$parametro,$limit,$paginador,false);
$totalrowsinformacion=count($informacion);
?>
<?php foreach ($informacion as $informacion) { ?>
<?php include ("../prints/data_informacion.php"); ?>
<title><?=$titulo;?></title>
<meta name="description" content="<?=$descripcion?>" />
<meta name="keywords" content="" />
<?php } ?>
<? include ("../includes/tags_styles.php"); ?>
</head>

<?php include ('../header.php');?>

      <!-- SECCIÓN NOTICIAS INTERNO -->
      <div class="container interno fondo-blanco paddin20top">
        <div class="row">
          <section class="col-md-9 margin100botton">
            <?php $print_view='infografia_interna'; include ("../prints/informacion.php"); ?>
          </section>
          <aside class="col-md-3">
            <?php include '../aside.php';?>
          </aside>
        </div>
      </div>

      <!-- SECCIÓN NOTICIAS -->

<?php $fondo_footer='img-footer'; include ('../footer.php');?>
<?php include ("../includes/tags_functions.php"); ?>
</body>
<?php
include ("../includes/fin.php");
?>
</html>
