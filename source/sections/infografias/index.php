<?php
include ("../includes/inicio.php");
include ("../includes/config.php");
include ("../includes/modelo.php");
include ("../includes/modeloGlobal.php");
$obj = new modelo();
$objGlobal=new modeloGlobal();
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
<?php
$id_seccion="2";
$SEO=$obj->SEO($id_seccion,false);
foreach($SEO as $SEO){ ?>
<title><?=utf8_encode($SEO['Titulo']); ?></title>
<meta name="description" content="<?=utf8_encode($SEO['Descripcion']); ?>" />
<meta name="keywords" content="<?=utf8_encode($SEO['Keywords']); ?>" />
<?php  } ?>
<? include ("../includes/tags_styles.php"); ?>
</head>
<body>
<?php include ('../header.php');?>

      <!-- SECCIÓN NOTICIAS -->

      <div class="container fondo-blanco">
        <div class="row">
          <section class="col-md-9 margin100botton">
              <?php
              $parametro=array("id_cat_info"=>'2', "parametro"=>$parametro);
              $informacion=$obj->getInformacion('list',$parametro,$limit,$paginador,false);
              $print_view='infografia';
              include ("../prints/informacion.php"); ?>
          </section>
          <aside class="col-md-3">
            <?php include '../aside.php';?>
          </aside>
        </div>
      </div>

    <!-- SECCIÓN NOTICIAS -->

<?php $fondo_footer='img-footer'; include ('../footer.php');?>
<?php include ("../includes/tags_functions.php"); ?>
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<script src="../js/wow.min.js"></script>
<script type="text/javascript">
  $( '.wow' ).load(function() {
    $('.row_grid').delay(2000).masonry({
    itemSelector: '.item_grid'
    });
  });
  new WOW().init();
  $(document).scroll(function() {
    $('.row_grid').masonry({
    itemSelector: '.item_grid'
    });
  });
</script>
</body>
<?php
include ("../includes/fin.php");
?>
</html>
