<?php
include ("../includes/inicio.php");
include ("../includes/config.php");
require ("../configAPP.php");
include ("../includes/modelo.php");
include ("../includes/modeloGlobal.php");
$obj = new modelo();
$objGlobal=new modeloGlobal();
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
<?php
$id_seccion="6";
$SEO=$obj->SEO($id_seccion,false);
foreach($SEO as $SEO){ ?>
<title><?=utf8_encode($SEO['Titulo']); ?></title>
<meta name="description" content="<?=utf8_encode($SEO['Descripcion']); ?>" />
<meta name="keywords" content="<?=utf8_encode($SEO['Keywords']); ?>" />
<?php  } ?>
<? include ("../includes/tags_styles.php"); ?>
</head>
<body>
<?php include ('../header.php');?>

<!-- SECCIÓN CAPACITACION -->

      <div class="container fondo-blanco">
        <div class="row">
          <div class="col-md-9 margin100botton">
            <?php $directorio=$obj->getDirectorio('list',$parametro,$limit,$paginador,false); $print_view='list'; include ("../prints/directorio.php"); ?>
          </div>
          <aside class="col-md-3">
            <?php include '../aside.php';?>
          </aside>
        </div>
      </div>

      <!-- SECCIÓN CAPACITACION -->

<?php $fondo_footer='img-footer2'; include ('../footer.php');?>
<?php include ("../includes/tags_functions.php"); ?>
</body>
<?php
include ("../includes/fin.php");
?>
</html>
