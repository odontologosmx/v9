User-agent: *
Allow: /
Disallow: /*.js$
Disallow: /*.css$
Sitemap: //portalodontologos.com.mx/sitemap.xml
Sitemap: //portalodontologos.com.mx/generate_sitemap.php
