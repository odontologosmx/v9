<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
include ("includes/inicio.php");
include ("includes/config.php");
require ("custom/configAPP.php");
include ("includes/modelo.php");
include ("includes/modeloGlobal.php");
$obj = new modelo();
$objGlobal=new modeloGlobal();
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
<title>Error 404</title>
<meta name="robots" content="noindex, nofollow">
<meta name="googlebot" content="noindex">

<? include ("includes/tags_styles.php"); ?>
</head>
<body>
<?php include ('custom/header.php');?>

<div class="container">
  <div class="row text-center">
    <h1>Página no encontrada</h1>
    <br>
    <h2>ERROR 404</h2>
  </div>
</div>

<?php include ('custom/footer.php');?>
<?php include ("includes/tags_functions.php"); ?>
</body>
<?php
include ("includes/fin.php");
?>
</html>
