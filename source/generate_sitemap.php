<?php
  include ("includes/inicio.php");
  include ("includes/config.php");
  require ("custom/configAPP.php");
  include ("includes/modelo.php");
  include ("includes/modeloGlobal.php");
  $obj = new modelo();
  $objGlobal=new modeloGlobal();
  header ("Content-Type:text/xml");
?>
<urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">
  <?php $directorio=$obj->getDirectorio('list',$parametro,$limit,$paginador, false);?>
  <?php foreach ($directorio as $directorio) { ?>
    <url>
      <loc><?=$protocolo.$path."/".$directorio['permalink']?></loc>
    </url>
  <?php } ?>
  <?php $parametro=array("id_cat_info"=>'1', "parametro"=>$parametro); $informacion=$obj->getInformacion('list',$parametro,$limit,$paginador, false);?>
  <?php foreach ($informacion as $informacion) { ?>
    <url>
      <loc><?=$protocolo.$path."/informacion/".$informacion['ID']."/".$objGlobal->prettyUrl($informacion['titulo']);?></loc>
    </url>
  <?php } ?>
  <?php $parametro=array("id_cat_info"=>'2', "parametro"=>$parametro); $informacion=$obj->getInformacion('list',$parametro,$limit,$paginador, false);?>
  <?php foreach ($informacion as $informacion) { ?>
    <url>
      <loc><?=$protocolo.$path."/infografias/".$informacion['ID']."/".$objGlobal->prettyUrl($informacion['titulo']);?></loc>
    </url>
  <?php } ?>
  <?php $calendario=$obj->getCalendario('list',$parametro,$limit,$paginador, false);?>
  <?php foreach ($calendario as $calendario) { ?>
    <url>
      <loc><?=$protocolo.$path."/calendario/".$calendario['ID']."/".$objGlobal->prettyUrl($calendario['titulo']);?></loc>
    </url>
  <?php } ?>
  <?php $productos=$obj->getProductos('list',$parametro,$limit,$paginador, false);?>
  <?php foreach ($productos as $productos) { ?>
    <url>
      <loc><?=$protocolo.$path."/productos/".$productos['ID']."/".$objGlobal->prettyUrl($productos['titulo']);?></loc>
    </url>
  <?php } ?>
  <?php $promociones=$obj->getPromociones('list',$parametro,$limit,$paginador, false);?>
  <?php foreach ($promociones as $promociones) { ?>
    <url>
      <loc><?=$protocolo.$path."/promociones/".$promociones['ID']."/".$objGlobal->prettyUrl($promociones['titulo']);?></loc>
    </url>
  <?php } ?>
  <?php $webinar=$obj->getWebinars('list',$parametro,$limit,$paginador, false);  ?>
  <?php foreach ($webinar as $webinar) { ?>
    <url>
      <loc><?=$protocolo.$path."/curzoos-online/".$webinar['ID']."/".$objGlobal->prettyUrl($webinar['titulo']);?></loc>
    </url>
  <?php } ?>
</urlset>
