<?php
class modelo{

  private function getobjGlobal(){
    return $getobjGlobal = new modeloGlobal();
  }

  /* -------------------------------- SEO -------------------------------- */
  public function SEO($bd_site,$id_seccion,$debug){
    $getobjGlobal=$this->getobjGlobal();
    $query = "SELECT * FROM $bd_site.config_secciones WHERE ID LIKE $id_seccion";
    return $getobjGlobal->ejecutaConsulta($query,'selectmultiple',$debug);
  }

  /* ----------------------------- SECCIONES ----------------------------- */
  public function getDirectorio($bd_site,$metodo,$parametro,$limit,$paginador,$debug){
    $getobjGlobal=$this->getobjGlobal();
    $select = "SELECT s_clientes.nombre_comercial, s_clientes.avatar, s_clientes.imagen_perfil, s_clientes.cv_empresarial, s_clientes.permalink, s_cat_estados.estado, ubicaciones.ciudad";
    $from = "FROM g13_general.s_clientes JOIN {$bd_site}.ubicaciones ON ubicaciones.id_cliente=s_clientes.ID JOIN {$bd_site}.s_cat_estados ON ubicaciones.id_estado=s_cat_estados.ID";
    $where = "WHERE s_clientes.status='1' AND s_clientes.permalink!=''";
    $order = "ORDER BY s_cat_estados.estado, s_clientes.nombre_comercial ASC";
    if($limit!='')
      $limit = "LIMIT ".$limit;
    switch ($metodo) {
        case 'perfiles_home':
          $select = "SELECT s_clientes.nombre_comercial, s_clientes.avatar, s_clientes.permalink";
          $from = "FROM g13_general.s_clientes";
          $where = "WHERE s_clientes.status='1' AND s_clientes.permalink!='' AND s_clientes.avatar!=''";
          $order = "ORDER BY RAND()";
        break;
        case 'list':
          $clause="AND id_cat_clasificacion_cliente='$parametro'";
        break;
        case 'buscador':
          $parametro=$getobjGlobal->quitacentos($parametro);
          $clause="AND (s_clientes.nombre_comercial LIKE '%$parametro%' OR s_clientes.cv_empresarial LIKE '%$parametro%' OR s_clientes.contenido_personalizado LIKE '%$parametro%' OR s_clientes.permalink LIKE '%$parametro%')";
        break;
    }
    $query=$select." ".$from." ".$where." ".$clause." ".$order." ".$limit;
    return $getobjGlobal->ejecutaConsulta($query,'selectmultiple',$debug);
  }

  public function getInformacion($bd_site,$metodo,$parametro,$limit,$paginador,$debug){
    $getobjGlobal=$this->getobjGlobal();
    // print_r($parametro);
    $id=$parametro['id'];
    $id_cat=$parametro['id_cat'];
    $id_cliente=$parametro['id_cliente'];
    $search_tag=$parametro['search_tag'];
    // $parametro=$parametro['parametro'];
    $select = "SELECT informacion.ID, informacion.fecha_hora_publicacion, informacion.imagen, informacion.contenido, informacion.titulo, informacion.id_cat_tags, informacion.id_cat_tema, s_cat_temas.nombre_tema, s_clientes.nombre_comercial, s_clientes.cv_empresarial, s_clientes.avatar, s_clientes.permalink";
    $from = "FROM {$bd_site}.informacion JOIN {$bd_site}.s_cat_temas ON informacion.id_cat_tema=s_cat_temas.ID JOIN g13_general.s_clientes ON informacion.id_cliente=s_clientes.ID";
    $where = "WHERE (informacion.status='1' AND s_clientes.status='1' AND informacion.fecha_hora_publicacion<=CURDATE()) AND (informacion.fecha_hora_vigencia>=CURDATE() OR informacion.fecha_hora_vigencia='0000-00-00 00:00:00')";
    $order = "ORDER BY informacion.fecha_hora_publicacion DESC, informacion.prioridad ASC";

    $paginas_por_pagina = '20';

    $p=$_GET["p"];
    if (!$p) {
      $var_limit=0;
      $p=1;
    }
    else {
      $var_limit = ($p-1) * $paginas_por_pagina;
    }

    if ($paginador==0){
      $limit = " LIMIT $limit";
    }
    else {
      $limit = " LIMIT $var_limit, $paginas_por_pagina";
    }
    if ($limit==''){
        $limit="";
    }

    switch ($metodo) {
        case 'total':
          $limit="";
        break;
        case 'list':
          $clause="";
        break;
        case 'info':
          $clause = "AND informacion.ID=$id";
          $limit="";
        break;
        case 'related':
          $clause="AND informacion.id_cat_tema=$id_cat AND informacion.ID!=$id";
          $order = "ORDER BY RAND()";
        break;
        case 'prev':
          $clause = "AND informacion.ID=$id-1";
        break;
        case 'next':
          $clause = "AND informacion.ID=$id+1";
        break;
        case 'profile':
          $clause="AND (s_clientes.permalink LIKE '$permalink')";
        break;
        case 'buscador':
          $clause="AND (informacion.titulo LIKE '%$search_tag%' OR informacion.contenido LIKE '%$search_tag%' OR s_clientes.nombre_comercial LIKE '%$search_tag%' OR s_clientes.permalink LIKE '%$search_tag%')";
        break;
    }

    $query=$select." ".$from." ".$where." ".$clause." ".$order." ".$limit;
    return $getobjGlobal->ejecutaConsulta($query,'selectmultiple',$debug);
  }

  public function getCalendario($bd_site,$metodo,$parametro,$limit,$paginador,$debug){
    $getobjGlobal=$this->getobjGlobal();
    $id=$parametro['id'];
    $id_cat=$parametro['id_cat'];
    $id_cliente=$parametro['id_cliente'];
    $select = "SELECT calendario.ID, calendario.fecha_hora_inicio, calendario.fecha_hora_final, calendario.titulo, calendario.sinopsis, s_cat_calendario.evento AS clasificacion_evento, calendario.imagen, calendario.contenido, calendario.link, s_clientes.ID AS id_organizador, s_clientes.nombre_comercial, s_clientes.avatar, s_clientes.permalink, ubicaciones.nombre_ubicacion, ubicaciones.calle, ubicaciones.numero, ubicaciones.colonia, ubicaciones.ciudad, ubicaciones.lat, ubicaciones.long, ubicaciones.placeID, s_cat_estados.estado";
    $from = "FROM {$bd_site}.calendario JOIN g13_general.s_clientes ON calendario.id_cliente=s_clientes.ID JOIN {$bd_site}.s_cat_calendario ON calendario.id_cat_calendario=s_cat_calendario.ID JOIN {$bd_site}.ubicaciones ON calendario.id_ubicacion=ubicaciones.ID JOIN g13_general.s_cat_estados ON ubicaciones.id_estado=s_cat_estados.ID";
    $where = "WHERE {$bd_site}.calendario.status='1' AND g13_general.s_clientes.status='1'";
    $order = "ORDER BY {$bd_site}.calendario.fecha_hora_inicio ASC";

    if($limit!='')
      $limit = "LIMIT ".$limit;
    switch ($metodo) {
        case 'list':
          $clause="AND (calendario.fecha_hora_inicio>=CURDATE() OR calendario.fecha_hora_final>=CURDATE() AND DATEDIFF(calendario.fecha_hora_final,calendario.fecha_hora_inicio)<90)";
        break;
        case 'info':
          $clause = "AND calendario.ID=$id";
        break;
        case 'prev':
          $clause = "AND calendario.ID=$id-1 AND s_clientes.ID=$id_cliente";
        break;
        case 'next':
          $clause = "AND calendario.ID=$id+1 AND $id_cliente";
        break;
        case 'profile':
          $clause="AND (s_clientes.ID LIKE '$id_cliente')";
        break;
        case 'buscador':
          $clause="AND (calendario.titulo LIKE '%$parametro%' OR calendario.contenido LIKE '%$parametro%' OR s_clientes.nombre_comercial LIKE '%$parametro%' OR s_clientes.permalink LIKE '%$parametro%')";
        break;
    }
    $query=$select." ".$from." ".$where." ".$clause." ".$order." ".$limit;
    return $getobjGlobal->ejecutaConsulta($query,'selectmultiple',$debug);
  }

  public function getProductos($bd_site,$metodo,$parametro,$limit,$paginador,$debug){
    $getobjGlobal=$this->getobjGlobal();
    $select = "SELECT productos.ID, productos.titulo, productos.thumb, productos.imagen, productos.contenido, s_clientes.ID AS id_fuente, s_clientes.nombre_comercial, s_clientes.permalink";
    $from = "FROM {$bd_site}.productos JOIN g13_general.s_clientes ON productos.id_cliente=s_clientes.ID";
    $where = "WHERE (productos.status='1' AND s_clientes.status='1') AND (productos.fecha_hora_publicacion>=CURDATE() OR productos.fecha_hora_vigencia>=CURDATE())";
    $order = "ORDER BY RAND()";
    if($limit!='')
      $limit = "LIMIT ".$limit;
    switch ($metodo) {
        case 'list':
          $clause="";
        break;
        case 'info':
          $clause = "AND productos.ID=$parametro";
        break;
        case 'related':
        $id_prod=$parametro['id_prod'];
        $id_fuente=$parametro['id_fuente'];
          $clause = "AND (productos.ID!=$id_prod AND s_clientes.ID=$id_fuente)";
        break;
        case 'profile':
          $clause="AND (s_clientes.permalink LIKE '$parametro')";
        break;
        case 'buscador':
          $clause="AND (productos.titulo LIKE '%$parametro%' OR productos.contenido LIKE '%$parametro%' OR productos.descripcion LIKE '%$parametro%' OR s_clientes.nombre_comercial LIKE '%$parametro%' OR s_clientes.permalink LIKE '%$parametro%')";
        break;
    }
    $query=$select." ".$from." ".$where." ".$clause." ".$order." ".$limit;
    return $getobjGlobal->ejecutaConsulta($query,'selectmultiple',$debug);
  }


  /* ----------------------------- TAGS/CATEGORIAS ------------------------*/
  public function getTags($bd_site,$metodo,$parametro,$limit,$paginador,$debug){
    $getobjGlobal=$this->getobjGlobal();
    switch ($metodo) {
        case 'informacion':
          $parametro=explode(",",$parametro);
          foreach ($parametro as $parametro) {
            $select = "SELECT * ";
            $from = "FROM {$bd_site}.s_cat_tags";
            $where = "WHERE s_cat_tags.status='1' AND s_cat_tags.ID={$parametro}";
            $order = "ORDER BY s_cat_tags.etiqueta ASC";
            $query=$select." ".$from." ".$where." ".$clause." ".$order." ".$limit;
            $tags[]=$getobjGlobal->ejecutaConsulta($query,'select',$debug);
          }
        break;
    }
    return $tags;
  }

  /* ----------------------------- PUBLICIDAD ----------------------------- */
  public function getPublicidad($bd_site,$posicion,$limit,$debug){
    $getobjGlobal=$this->getobjGlobal();
    $select =  "SELECT publicidad.titulo, publicidad.imagen, publicidad.link, publicidad.descripcion, s_cat_publicidad.posicion, s_cat_publicidad.ruta, s_clientes.nombre_comercial";
    $from = "FROM {$bd_site}.publicidad JOIN g13_general.s_clientes ON publicidad.id_cliente=s_clientes.ID JOIN {$bd_site}.s_cat_publicidad ON publicidad.id_posicion=s_cat_publicidad.ID";
    $where =  "WHERE s_clientes.status='1' AND publicidad.status='1' AND s_cat_publicidad.status='1' AND id_posicion=$posicion AND (publicidad.fecha_hora_publicacion>=CURDATE() OR publicidad.fecha_hora_vigencia>=CURDATE())";
    $order = "ORDER BY RAND()";
    $limit = "LIMIT ".$limit;
    $query=$select." ".$from." ".$where." ".$clause." ".$order." ".$limit;
    return $getobjGlobal->ejecutaConsulta($query,'selectmultiple',$debug);
  }

  /* ----------------------------- PERFILES ----------------------------- */
  public function getPerfiles($bd_site,$url,$debug){
    $getobjGlobal=$this->getobjGlobal();
    $query =  "SELECT * FROM g13_general.s_clientes WHERE s_clientes.permalink LIKE '$url' AND status= '1'";
    return $getobjGlobal->ejecutaConsulta($query,'selectmultiple',$debug);
  }

  public function getMediosContacto($id_perfil,$medio,$debug){
    $getobjGlobal=$this->getobjGlobal();
    $select = "SELECT s_cat_medios_contacto.ID, s_cat_medios_contacto.medio_contacto, medios_contacto.datos_medio_contacto";
    $from = "FROM {$bd_site}.medios_contacto JOIN {$bd_site}.s_cat_medios_contacto ON medios_contacto.id_cat_medios_contacto=s_cat_medios_contacto.ID JOIN g13_general.s_clientes ON medios_contacto.id_cliente=s_clientes.ID ";
    $where = "WHERE s_clientes.ID=$id_perfil";
    if ($medio!='') {
      $clause = "AND s_cat_medios_contacto.ID=$medio";
    }
    $query=$select." ".$from." ".$where." ".$clause." ".$order." ".$limit;
    return $getobjGlobal->ejecutaConsulta($query,'selectmultiple',$debug);
  }

  public function getUbicaciones($id_perfil,$ubicacion,$debug){
    $getobjGlobal=$this->getobjGlobal();
    $query = "SELECT ubicaciones.* FROM ubicaciones JOIN s_cat_ubicaciones ON ubicaciones.id_cat_ubicaciones=s_cat_ubicaciones.ID JOIN g13_general.s_clientes ON ubicaciones.id_cliente=s_clientes.ID WHERE s_clientes.ID=$id_perfil AND s_cat_ubicaciones.ID=$ubicacion";
    return $getobjGlobal->ejecutaConsulta($query,'selectmultiple',$debug);
  }

  /* -------------Generador de Contactos */

  public function get_tablas_GC($tabla,$campo) {
    $getobjGlobal=$this->getobjGlobal();
    $query = "SELECT * FROM  gc_fuentes";
    return $getobjGlobal->ejecutaConsulta($query,'selectmultiple',$debug);
  }

  /* ----------------------WEBINARS-------------------------- */

  public function getWebinars($metodo,$parametro,$limit,$paginador,$debug){
    $getobjGlobal=$this->getobjGlobal();
    $select = "SELECT webinars.*, calendario.titulo, calendario.fecha_hora_inicio, calendario.fecha_hora_final, s_clientes.nombre_comercial, s_clientes.permalink, s_clientes.avatar";
    $from = "FROM calendario JOIN webinars ON calendario.ID=webinars.id_calendario JOIN g13_general.s_clientes ON s_clientes.ID=calendario.id_cliente";
    $where = "WHERE webinars.status='1' AND calendario.status='1'";
    $order = "ORDER BY calendario.fecha_hora_inicio DESC";
    if($limit!='')
      $limit = "LIMIT ".$limit;
    switch ($metodo) {
        case 'list':
          $clause="";
        break;
        case 'info':
          $webinar=$parametro['parametro'];
          $clause = "AND webinars.ID='$webinar'";
        break;
        case 'related':
          //$clause = "AND (productos.ID!=$id_prod AND s_clientes.ID=$id_fuente)";
        break;
        case 'profile':
          $clause = "AND s_clientes.permalink='$parametro'";
        break;
        case 'buscador':
          $clause="AND (calendario.titulo LIKE '%$parametro%' OR s_clientes.nombre_comercial LIKE '%$parametro%' OR s_clientes.permalink LIKE '%$parametro%')";
        break;
    }
    $query=$select." ".$from." ".$where." ".$clause." ".$order." ".$limit;
    return $getobjGlobal->ejecutaConsulta($query,'selectmultiple',$debug);
  }

  public function getPreguntasWebinars($webinar,$status){
    $getobjGlobal=$this->getobjGlobal();
    $select = "SELECT webinars_preguntas.*, gc_fuentes.fuente";
    $from = "FROM webinars_preguntas JOIN webinars ON webinars.ID=webinars_preguntas.id_webinar JOIN gc_fuentes ON gc_fuentes.ID=webinars_preguntas.id_fuente";
    $where = "WHERE webinars.status='1'";
    $clause = "AND webinars_preguntas.id_webinar='$webinar'";
    if ($status!='') {
      $clause .= "AND webinars_preguntas.status='$status'";
    }
    $order = "ORDER BY webinars_preguntas.fecha_hora_registro ASC";
    $query=$select." ".$from." ".$where." ".$clause." ".$order." ".$limit;
    return $getobjGlobal->ejecutaConsulta($query,'selectmultiple',$debug);
  }

  public function insertPreguntasWebinars($sesion, $nombre, $pregunta, $id_fuente, $time) {
    $getobjGlobal=$this->getobjGlobal();
    $nombre=utf8_decode($nombre);
    $pregunta=utf8_decode($pregunta);
    if ($time=='') $time='00:00:00';
    $fecha_registro=date('Y-m-d H:i:s');
    $query="INSERT INTO webinars_preguntas (`ID`, `fecha_hora_registro`, `id_webinar`, `nombre`, `pregunta`, `time_video`, `id_fuente`, `status`) VALUES (NULL, '$fecha_registro', '$sesion', '$nombre', '$pregunta', '$time', '$id_fuente', '0')";
    echo $getobjGlobal->ejecutaConsulta($query,'insert',$debug);
  }

  public function updatePreguntasWebinars($id_pregunta,$accion,$nombre,$pregunta,$time) {
    $getobjGlobal=$this->getobjGlobal();
    $nombre=utf8_decode($nombre);
    $pregunta=utf8_decode($pregunta);
    switch ($_POST['accion']) {
      case 'status':
        $query="UPDATE  `webinars_preguntas` SET  `status` =  '1' WHERE  `webinars_preguntas`.`ID` ='$id_pregunta';";
      break;
      case 'text':
        $query="UPDATE  `webinars_preguntas` SET  `nombre` =  '$nombre', `pregunta` =  '$pregunta', `time_video` =  '$time' WHERE  `webinars_preguntas`.`ID` ='$id_pregunta';";
      break;
    }
    echo $getobjGlobal->ejecutaConsulta($query,'update',$debug);
  }

}

?>
