<script src="https://maps.googleapis.com/maps/api/js?key=<?=$api_map?>" type="text/javascript"></script>

<?php $ubicaciones=$obj->getUbicaciones($id_perfil,'1'); //var_dump($ubicaciones);?>
<?php foreach ($ubicaciones as $ubicaciones){ $lat=$ubicaciones['lat']; $long=$ubicaciones['long']; $c_map_s+=1;?>
<script type="text/javascript">
  function initialize<?=$c_map_s?>() {
  var myLatlng = new google.maps.LatLng(<?=$lat;?>,<?=$long;?>);
  var mapOptions = {
  zoom: 17,
  styles: [{"featureType":"all","elementType":"all","stylers":[{"hue":"#008eff"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"0"},{"lightness":"0"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":"-60"},{"lightness":"-20"}]}],
  center: myLatlng,
  zoomControl: true,
  scaleControl: true,
  streetViewControl: true
  }
  var map = new google.maps.Map(document.getElementById('map<?=$c_map_s?>'), mapOptions);
  var contentString =
    '<?=$nombre_comercial;?>';

    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });
  var marker = new google.maps.Marker({
    position: myLatlng,
    map: map,
    icon: '<?=$path;?>/img/marker.png',
    animation: google.maps.Animation.DROP,
    title: ''
  });
  google.maps.event.addListener(marker, 'click', function() {
  infowindow.open(map,marker);

  });

  }
  google.maps.event.addDomListener(window, 'load', initialize<?=$c_map_s?>);
</script>

<?php } ?>
