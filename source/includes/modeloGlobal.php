<?php
include "mysql.php";

class modeloGlobal{

  private function conexion(){
    include ("config.php");
    return $mysqlBD = new mysql($configGral["bd"]["host"],$configGral["bd"]["usuario"],$configGral["bd"]["password"],$configGral["bd"]["base"],$configGral["bd"]["puerto"]);
  }

  public function ejecutaConsulta($query,$tipo,$debug){
    $mysqlBD=$this->conexion();
    $resRegistros=$mysqlBD->ejecutarQuery($query);
    switch ($tipo) {
      case 'selectmultiple':
        if($resRegistros!=true){ echo $query;}
        while ($row=$mysqlBD->regresaResultados($resRegistros)){
          $result[]=$row;
        }
        if ($debug==true) {
          echo $query;
          echo "<br><br>COUNT: ".count($result)."<br><br>";
          echo "<br><br>";
          echo "<pre>"; echo print_r($result); echo "</pre>";
        }
        return $result;
      break;
      case 'select':
         $row=$mysqlBD->regresaResultados($resRegistros);
         if ($debug==true) { echo "<pre>".print_r($row)."</pre>"; }
         return $row;
      break;
      case 'insert':
        if($resRegistros){
           return true;
          } else {
           return false;
          }
      break;
      case 'insertreturn':
        $ultimoidInsert=$mysqlBD->ultimoId();
        return $ultimoidInsert;
      break;
      case 'update':
        if($resRegistros){
             return true;
            } else {
             return false;
            }
      break;
      default:
        if($resRegistros!=true){ echo $query;}
        while ($row=$mysqlBD->regresaResultados($resRegistros)){
          $result[]=$row;
        }
        return $result;
      break;
      }
      $liberar=$mysqlBD->liberarResultado($resRegistros);
      $mysqlBD=$this->closeConexion();
  }

  public function list_form_estados(){
		$query= "SELECT * FROM s_estados ";
		$mysqlBD=$this->conexion();
    	$res=$mysqlBD->ejecutarQuery($query);

		if($res==true){ echo "";}
			else{ echo "<br> Error ".mysql_error();}
		while ($fila=$mysqlBD->regresaResultados($res)){
			$estados[]=$fila;
		}
		return $estados;
	}

  public function daformatoaFecha($fecha){
    $fecha_publicacion = explode('-', $fecha);
    $anio= $fecha_publicacion[0];
    $mes= $this->formatoMes($fecha_publicacion[1]);
    $dia= $fecha_publicacion[2];
    $fechaconFormato=$dia." de ".$mes." de ".$anio;
    return $fechaconFormato;
  }

  public function daformatoaHora($hora){
    $horaconFormato = explode(':', $hora);
    $horaconFormato=$horaconFormato[0].":".$horaconFormato[1];
    return $horaconFormato;
  }

  public function formatoMes ($mes,$formato){
    switch ($formato){
      case "min":
        switch ($mes){
          case "00": $mes="";break;
          case "01": $mes="Ene";break;
          case "02": $mes="Feb";break;
          case "03": $mes="Mar";break;
          case "04": $mes="Abr";break;
          case "05": $mes="May";break;
          case "06": $mes="Jun";break;
          case "07": $mes="Jul";break;
          case "08": $mes="Ago";break;
          case "09": $mes="Sep";break;
          case "10": $mes="Oct";break;
          case "11": $mes="Nov";break;
          case "12": $mes="Dic";break;
        }
      break;
      default:
        switch ($mes){
          case "00": $mes="Por Confirmar";break;
          case "01": $mes="Enero";break;
          case "02": $mes="Febrero";break;
          case "03": $mes="Marzo";break;
          case "04": $mes="Abril";break;
          case "05": $mes="Mayo";break;
          case "06": $mes="Junio";break;
          case "07": $mes="Julio";break;
          case "08": $mes="Agosto";break;
          case "09": $mes="Septiembre";break;
          case "10": $mes="Octubre";break;
          case "11": $mes="Noviembre";break;
          case "12": $mes="Diciembre";break;
        }
    }

    return $mes;
  }

  public function explodeDateTime($datetime){
    $datetime=explode(' ', $datetime);
    return $datetime;
  }

  public function explodeFecha($fecha,$string){
    $fecha=explode('-', $fecha);
    switch ($string){
      case "Y": $dato=$fecha[0];break;
      case "m": $dato=$fecha[1];break;
      case "d": $dato=$fecha[2];break;
    }
    return $dato;
  }

  public function explodeTime($time,$string){
    $time=explode(':', $time);
    //var_dump($time);
    switch ($string){
      case "H": $dato=$time[0];break;
      case "i": $dato=$time[1];break;
      case "s": $dato=$time[2];break;
    }
    return $dato;
  }

  public function daformatoaTels($string){
    	$string = trim(utf8_encode($string));
  		$acentos=array('(',')','-',' ');
  	  $vocales=array('','','','');

      $string = str_replace($acentos,$vocales,$string);

  	return $string;
  }
  public function quitacentos($string){
      //$string = trim(utf8_encode($string));
      $string = trim($string);
      $acentos=array('á','é','í','ó','ú','Á','É','Í','Ó','Ú');
      $vocales=array('a','e','i','o','u','a','e','i','o','u');

      $string = str_replace($acentos,$vocales,$string);
      //$string = strtolower(str_replace(" ","-",$string));

    return $string;
  }
  public function quitapalabras_busqueda($string){
      $string=explode(' ',utf8_encode($string));
      /*var_dump($string);
      echo '<br /><br /><br />';*/
      foreach($string as $string)
      {
        $palabras=array('a', 'ha', 'han', 'al', 'con','de', 'del', 'desde', 'para','por', 'el', 'la', 'lo', 'los', 'las', 'y', 'en', 'por', 'entre', 'sin', 'le', 'desde', 'ante', 'bajo', 'cabe', 'sobre', 'su', 'un', 'una', 'ese', 'esta', 'este', 'estas', 'estos', 'se', 'es', 'que', 'si' , 'esa', 'esas','no', 'nos', 'cual', 'hasta', 'muy', 'ya', 'cuanto', 'como', 'son', 'mas', 'su', 'sus', 'todas', 'todos', 'tras', 'hace', 'porque', 'o');
        $total_palabras=count($palabras);
        /*echo '<br /><br /><br />';
        echo 'Comparando: '.$string.'<br />';*/
        $no_hay=0;
        foreach($palabras as $palabras)
        {
          if (preg_match("/\b".$palabras."\b/i", $string)) {
            /*echo 'SI HAY: ';
            echo $palabras.' = '.$string.'<br />';*/
            $no_hay=0;
            break;
          } else {
            /*echo 'NO HAY: ';
            echo $palabras.' = '.$string.'<br />';*/
            $no_hay+=1;
          }
          //echo $no_hay.' = '.$total_palabras.'<br />';
          if ($no_hay==$total_palabras)
            $search[]=$string;
        }
      }
      //var_dump(array_unique($search));
    return $search;
  }

  public function nombre_altas_bajas($string){
    $string=utf8_encode($string);
    $string=strtolower($string);
    $palabras_minusculas=array('de','del','los','la','el');
    $totalpalabras_minus=count($palabras_minusculas);
    $encontrado_minusculas=0;
    foreach($palabras_minusculas as $palabras_minusculas)
    {
        if (preg_match("/\b".$palabras_minusculas."\b/i", $string)) {
            $encontrado_minusculas=1;
            break;
        }
    }
    $palabras_silabas=array('ma');
    $totalpalabras_silabas=count($palabras_silabas);
    $encontrado_silabas=0;
    foreach($palabras_silabas as $palabras_silabas)
    {
        if (preg_match("/\b".$palabras_silabas."\b/i", $string)) {
            if (!preg_match("/\b.\b/i", $string))
                $encontrado_silabas=1;
            break;
        }
    }
    if($encontrado_minusculas==1)
        echo $string;
    else
        echo ucwords($string);
    if($encontrado_silabas==1)
        echo ".";
        $encontrado_silabas=0;
  }

  public function explode_nombre($string){
     $string=explode(' ',$string);
     foreach($string as $string)
     {
       echo nombre_altas_bajas($string)." ";
     }
  }

  public function formatoRichContent($string){
      $acentos=array('"',',',';');
      $vocales=array('','','');

      $string = str_replace($acentos,$vocales,$string);
      //$string = strtolower(str_replace(" ","-",$string));

    return $string;
  }

  public function prettyUrl($string){

    	$string = trim(utf8_encode($string));
  		$acentos=array('á','é','í','ó','ú','ñ','”','&','%','(',')','“','°','|',',','Á','É','Í','Ó','Ú','?','¿','.',',',':','Ñ','!','¡',';','"','  ','®','$','+','-','  ');
      $vocales=array('a','e','i','o','u','n','','','','','','','',' ','','A','E','I','O','U','','','','','','n','','','','','-','','','-','','');
  	  //$vocales=array('a','e','i','o','u','n','','','','','','','',' ','','a','e','i','o','u','','','','','n','','','','');

      $string = str_replace($acentos,$vocales,$string);
      $string = strtolower(str_replace(" ","-",$string));

  	return $string;
  }
  public function desprettyUrl($string){
      /*$string = trim(utf8_encode($string));
      $acentos=array('á','é','í','ó','ú','ñ','”','&','%','(',')','“','°','|',',','Á','É','Í','Ó','Ú','?','¿','.','  ',':','Ñ');
      $vocales=array('a','e','i','o','u','n','','','','','','','','','','a','e','i','o','u','','','','','n');*/
      $string = str_replace($acentos,$vocales,$string);
      $string = strtolower(str_replace("-"," ",$string));
      $string=ucfirst($string);
      //$string=ucwords($string);


    return $string;
  }

  public function hashtag($string){
      $string=utf8_encode($string);
      $string = trim($string);
      $string = ucwords($string);
      $acentos=array('á','é','í','ó','ú','ñ','”','&','%','(',')','“','°','|',',','Á','É','Í','Ó','Ú','?','¿','.',' ',':','/');
      $vocales=array('a','e','i','o','u','n','','','','','','','',' ','','a','e','i','o','u','','','','','','');
      $string = str_replace($acentos,$vocales,$string);
    return $string;
  }

  public function trim_string($trim_string, $texto_size){
  	$texto = strip_tags($trim_string);
  	$texto_size=$texto_size;
  	$texto=substr($texto, 0, $texto_size);
  	$index=strrpos($texto, " ");
  	$texto=substr($texto, 0,$index);
  	if($trim_string!='')
  	  $texto.=" ...";
  	return $texto;
  }

  public function trim_string_html($trim_string, $texto_size){
  	$texto = $trim_string;
  	$texto_size=$texto_size;
  	$texto=substr($texto, 0, $texto_size);
  	/*$index=strrpos($texto, " ");
  	$texto=substr($texto, 0, $index);
  	if($trim_string!='' && (strlen($trim_string)>=$texto_size))
  	  $texto.=" ...";*/
  	return $texto;
  }
  public function trim_string_html_cont($trim_string, $texto_size_cont){
  	$texto = $trim_string;
  	$texto_size_cont=$texto_size_cont;
  	$texto=substr($texto, $texto_size_cont);
  	/*$index=strrpos($texto, " ");
  	$texto=substr($texto, 0, $index);
  	if($trim_string!='')
  	  $texto.=" ...";*/
  	return $texto;
  }

  public function convierte_segs($time){
  	$H=$this->explodeTime($time,'H');
    $i=$this->explodeTime($time,'i');
    $s=$this->explodeTime($time,'s');
    $time=($H*3600)+($i*60)+$s;
  	return $time;
  }

  public function enviar_email($origen_nombre, $email, $subject, $destino, $message, $metodo_mail, $respuesta_ajax){
    include ("config.php");
    $message_html_inicio='<html><body>';
    $message_style='<style> body{ background-color:#f1f1f1; color:#575757; }</style>';
    $message_header='<img src="'.$url_logo.'"><br><br>';
    $message_header.='Mensaje Enviado el '.date("d/m/y")." a las ".date("H:i")."<br><br>";
    $message_html_fin='</body></html>';
    $message=$message_html_inicio.$message_style.$message_header.$message.$message_html_fin;

    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $headers .= 'Date: '.date(DATE_RFC822);

    $headers .= 'To: ' .$destino. "\r\n";
    $headers .= "From: =?utf-8?b?".base64_encode($origen_nombre)."?= <".$origen_mail.">\r\n";
    //CABECERAS ADICIONALES (SI QUIEREN COPIAS)
    //$headers .= 'Cc: cc@dominio.com' . "\r\n";
    //$headers .= 'Bcc: bcc@dominio.com' . "\r\n";
    if ($destino!='' && $subject!='' && $message!='') {
      switch ($metodo_mail) {
        case 'php_mail':
          if (mail($destino, '=?utf-8?B?'.base64_encode($subject).'?=', $message, $headers)) {
            if($respuesta_ajax=='1') echo '1';
            //echo "ENVIADO php_mail";
          }
          break;
        case 'SMTP':
        $origen_nombre='=?utf-8?B?'.base64_encode($origen_nombre).'?=';
        $subject='=?utf-8?B?'.base64_encode($subject).'?=';

          require_once('class.smtp.inc');

          //$password_mail="QZqEzgAVRF5r46CUua6u4g"; //PRINCIPAL ODONTOSERVER
          $password_mail="_gZrS0Xg1GXK8wtsWcw_Yw"; //G13 SERVER
          $params['host'] = 'smtp.mandrillapp.com';
          $params['port'] = 587;
          $params['helo'] = 'odontologos.mx';
          $params['auth'] = TRUE;
          $params['user'] = $origen_mail;
          $params['pass'] = $password_mail;

          $send_params['recipients'] = array($destino);
          $send_params['headers']	   = array(
                  'Content-Type: text/html; charset=utf-8\r\n',
                  'From: "'.$origen_nombre.'" <'.$origen_mail.'>',
                  //'To: '.$destino,
                  'To: '.$destino,
                  'Subject: '.$subject,
                  //'Disposition-Notification-To: contacto@odontologos.com.mx',
                  //'Disposition-Notification-To: '.$origen_mail,
                  //'Return-Receipt-To: '.$origen_mail,
                  'Date: '.date(DATE_RFC822),
                  'X-Mailer: PHP/' . phpversion(),
                  'MIME-Version: 1.0',
                  'Reply-To: '.$origen_nombre.'" <'.$origen_mail.'>',
                  'Return-Path: '.$origen_nombre.'" <'.$origen_mail.'>',
                  'Envelope-To:'.$destino
                    );
          $send_params['from']		= $origen_mail;
          $send_params['body']		= $message;

          if(is_object($smtp = smtp::connect($params)) AND $smtp->send($send_params)){
            //echo "ENVIADO SMTP";
            if($respuesta_ajax=='1') echo '1';
          }
        break;
      }
    }
  }

  public function insert_log_gc($id_cliente,$fuente,$medio,$contenido_fuente,$nombre,$email,$telefono,$ciudad,$estado,$especialidad,$accion,$message,$adicional1,$adicional2,$avance_contacto,$idcatdist) {
    $fecha_registro=date("Y-m-d H:i:s");
    $query =  "INSERT INTO `gc_logs` (`ID`, `fecha_registro`, `id_cliente`, `id_fuente`, `id_medio`, `contenido_fuente`, `nombre`, `email`, `telefono`, `celular`, `ciudad`, `estado`, `especialidad`, `id_accion`, `contenido_accion`, `adicional1`, `adicional2`, `id_avance_contacto`, `status`,`id_cat_dist`)
    VALUES (NULL, '$fecha_registro', '$id_cliente', '$fuente', '$medio', '$contenido_fuente', '$nombre', '$email', '$telefono', '', '', '', '', '$accion', '$message', '', '', '$avance_contacto', '1','$idcatdist')";
    $mysqlBD=$this->conexion();
    $res=$mysqlBD->ejecutarQuery($query);
    if($res==true){ echo "";}
      else{ echo "<br> Error ".mysql_error();}
      $liberar=$mysqlBD->liberarResultado($res);
  }
}

?>
