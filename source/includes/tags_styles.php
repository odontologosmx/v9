<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
<meta property="fb:pages" content="<?=$fb_page_id;?>"/>
<meta property="fb:app_id" content="<?=$fb_app_id;?>"/>

<meta name="author" content="<?=$nombre_sitio?>">
<meta name="subject" content="<?=$subject_sitio;?>">
<meta http-equiv="Expires" content="never">
<meta name="Revisit-After" content="1">
<meta name="distribution" content="Global">
<meta name="Robots" content="INDEX,FOLLOW">
<meta name="country" content="México">
<meta name="Language" content="ES">

<?php if($favicon==true) { ?>
  <link rel="icon" type="image/jpeg" href="<?=$avatar;?>">
<?php } else { ?>

<!-- https://realfavicongenerator.net/ -->
<link rel="apple-touch-icon" sizes="180x180" href="<?=$path;?>/img/native/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?=$path;?>/img/native/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?=$path;?>/img/native/favicon/favicon-16x16.png">
<link rel="manifest" href="<?=$path;?>/img/native/favicon/site.webmanifest">
<link rel="mask-icon" href="<?=$path;?>/img/native/favicon/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">
<!-- <link rel="shortcut icon" href="<?=$path;?>/img/native/favicon/favicon.png"> -->
<?php } ?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
<link rel="stylesheet" href="<?=$path;?>/css/animations.min.css">
<link href="<?=$link_fonts;?>" rel="stylesheet">

<link href="<?=$path;?>/libs/owl-carousel/owl.carousel.min.css" rel="stylesheet">
<link href="<?=$path;?>/libs/owl-carousel/owl.theme.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?=$path;?>/custom/css/style.css?v=2">

<!--[if lt IE 9]>
<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
<![endif]-->

<!--[if lt IE 9]>
<script src="//cdn.jsdelivr.net/css3-mediaqueries/0.1/css3-mediaqueries.min.js"></script>
<![endif]-->
