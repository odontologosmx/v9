$(document).ready(function() {

  /*************** BANNER SLIDER HOME ***************/
  var owl2 = $("#slider_home");

    owl2.owlCarousel({
		/*slideSpeed : 8000,*/
		paginationSpeed : 1500,
		singleItem:true,
		autoPlay: 5500,
		navigation:false,
		pagination:true,
		stopOnHover:true,
		items : 1,
		lazyLoad : true,
    autoHeight : true
 	  });

	  $("#next2").click(function(){
        owl2.trigger('owl.next');
      })
    $("#prev2").click(function(){
        owl2.trigger('owl.prev');
    })

});

/*************** STICKY ASIDE ***************/
$(window).ready(function() {
  if($(window).width()>=768)
	{
    $("#menu").sticky({topSpacing:0});
  }

  var related = $('.related');

  if (related.length==1){
    var footer_height=$("footer").height()+$(".related").height()+80;
  } else {
    var footer_height=$("footer").height()+40;
  }

	if($(window).width()>=992)
	{
    	$("#banners_right").sticky({topSpacing:80, bottomSpacing: footer_height});
    	$("#banners_left").sticky({topSpacing:80, bottomSpacing: footer_height});
      $("#datos_perfil").sticky({topSpacing:130, bottomSpacing: footer_height});
      $("#datos_landing").sticky({topSpacing:20, bottomSpacing: footer_height});
	}
});

/*************** SCROLL A SECCIÓN ***************/
// function scroll_hash(){
//   url=window.location.href;
//   var filter=new RegExp("#");
//   if (filter.test(url)){
//     //alert(url);
//     hash=window.location.hash;
//     //alert(hash);
//     $(hash).animatescroll({scrollSpeed:1000, padding:60});
//   }
// }
// $(window).load(function() {
//   scroll_hash();
// });

$(document).ready(function(){
  $('p').each(function(){
    
    
      if ( $(this).text() == '\xa0' ) {
        // alert('empty: ');
        $(this).remove();
      }
      
  });

});