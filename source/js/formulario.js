function enviar_email() {
  //alert('function enviar_email ok');
  //$("#btnEnviar").attr("disabled", true);
  $(".mensaje_error").remove(); //quita la clase de textos rojos

  var filter=/^[-a-zA-z0-9~!$%^&*_=+}{\'?]+(\.[-a-zA-z0-9~!$%^&*_=+}{\'?]+)*@[A-Za-z0-9_]+.[A-Za-z0-9_.]+[A-za-z]$/; //expresion regular que valida si el campo email tienes una estructura de correo
  //aqui van los campos del formulario, debe ir con el mismo nombre del formulario html
  var email = $('#email').val();
  var nombre = $('#nombre').val();
  var telefono = $('#telefono').val();
  var mensaje = $('#mensaje').val();

  //fin de los campos del formulario
  var sendMail = ""; //variable que se inicia para hacer la validaacion "NUNCA MODIFICAR ESTA VARIABLE"
  //validaciones de campos
  if (filter.test(email)){
    sendMail = "true";
  }else{
    $('#email').after("<span class='mensaje_error' id='c_error_mail'>Ingrese e-mail valido.</span>");
    //escribimos un mensaje de error debajo del campo para notificar un error, podemos editar el texto que aparecera
    sendMail = "false";
  }
  if (nombre.length == 0 ){
    $('#nombre').after( "<span class='mensaje_error' id='c_error_name'>Ingrese su nombre.</span>" );
    sendMail = "false";
  }
  if (telefono.length == 0 ){
    $('#telefono').after( "<span class='mensaje_error' id='c_error_msg'>Ingrese teléfono.</span>" );
    sendMail = "false";
  }
  //mandamos los valores al formulario, meter aqui todos los campos que quieres mandar*/
  if(sendMail == "true"){
    datos=$( ".datos" ).serializeArray();
    datos = JSON.stringify(datos);
    //alert("Listo para enviar datos...");

    $.ajax({
        data: "datos=" + datos,
        url: '../includes/enviar_email.php',
        type: 'get',
        beforeSend: function () {
        $("#btnEnviar").val("ENVIANDO...");
      },
      success: function (result) {
        if(result == 1){
          //alert("Datos recibidos...");
          //jQuery(".result").html(result);
          $("#respuesta_form_ok").slideDown().delay(5000).slideUp();
          $("#form").slideUp().delay(5000).slideDown();
          $('#form')[0].reset();//limpiamos el formulario
          grecaptcha.reset();
          $("#btnEnviar").val("ENVIAR");//ponemos la palabra Enviar en el boton
          $("#btnEnviar").hide();
  			}
  			else
  			{
          $("#btnEnviar").val("ENVIAR");//ponemos la palabra Enviar en el boton
          $("#respuesta_form_error").slideDown().delay(5000).slideUp();
            //$("#btnEnviar").removeAttr("disabled");
        }
  		}
    });
  }
}
