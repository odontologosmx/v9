<?php
include ("includes/inicio.php");
include ("includes/config.php");
include ("includes/modelo.php");
include ("includes/modeloGlobal.php");
$obj = new modelo();
$objGlobal=new modeloGlobal();
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
<?php
$busqueda=$_GET['q'];
?>
<title>Buscador: <?=$busqueda;?></title>
<meta name="description" content="<?=utf8_encode($SEO['Descripcion']); ?>" />
<meta name="keywords" content="<?=utf8_encode($SEO['Keywords']); ?>" />
<? include ("includes/tags_styles.php"); ?>
</head>

<?php include ('custom/header.php');?>

      <!-- SECCIÓN NOTICIAS -->

      <div class="container fondo-blanco">
        <div class="row">
          <section class="col-md-9 margin100botton">
              <?php $directorio=$obj->getDirectorio('buscador',$busqueda,$limit,$paginador,false); $print_view='list'; include ("custom/prints/directorio.php"); ?>
              <?php $parametro=array("id_cat_info"=>'1', "parametro"=>$busqueda); $informacion=$obj->getInformacion('buscador',$parametro,$limit,$paginador,false); $print_view='list'; include ("custom/prints/informacion.php"); ?>
              <?php $parametro=array("id_cat_info"=>'2', "parametro"=>$busqueda); $informacion=$obj->getInformacion('buscador',$parametro,$limit,$paginador,false); $print_view='infografia'; include ("custom/prints/informacion.php"); ?>
              <?php $calendario=$obj->getCalendario('buscador',$busqueda,$limit,$paginador,false); $print_view='list'; include ("custom/prints/calendario.php"); ?>
              <?php $productos=$obj->getProductos('buscador',$busqueda,$limit,$paginador,false); $print_view='list'; include ("custom/prints/productos.php"); ?>
              <?php $productos=$obj->getPromociones('buscador',$busqueda,$limit,$paginador,false); $print_view='list'; include ("custom/prints/promociones.php"); ?>
              <?php $webinar=$obj->getWebinars('list',$parametro,$limit,$paginador, false); $print_view='list'; include ("custom/prints/webinars.php"); ?>
              <?php if (count($directorio)==0 && count($informacion==0) && count($calendario==0) && count($productos==0) && count($webinar)==0){ ?>
                <h1 class="text-center">No se encontraron resultados<br>con la palabra: <b><?=$busqueda;?></b></h1>
              <?php } ?>
          </section>
          <aside class="col-md-3">
            <?php include ('custom/aside.php');?>
          </aside>
        </div>
      </div>

    <!-- SECCIÓN NOTICIAS -->

<?php $fondo_footer='img-footer'; include ('custom/footer.php');?>
<?php include ("includes/tags_functions.php"); ?>
</body>
<?php
include ("includes/fin.php");
?>
</html
