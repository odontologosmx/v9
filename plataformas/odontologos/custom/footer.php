<footer id="footer" class="footer text-white">
	<div class="container-fluid fondo-color-especial-6">
		<div class="container menu-top">
			<div class="row">
				<div class="col-md-12 border-width-bottom margin0">
					<div class="row">
						<div class="col col-md-6 col-lg-8">
							<img src="<?=$nivel_ruta;?>img/native/logo-posa-bco-01.png" alt="" style="width: 200px;">
						</div>
						<div class="col col-md-6 col-lg-4">
							<ul class="text-right text-white">
								<li><strong>Conecta con nocotros</strong></li>
								<li><?php include ($nivel_ruta."custom/prints/social_icons.php"); ?></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-8 col-lg-8 bloque-a margin0">
					<div class="row">
						<div class="col-sm-12 col-md-6 col-lg-4">
						<h4>Contenido</h4>
						<ul>
							<li><a href="">Noticias</a></li>
							<li><a href="">Proveedores</a></li>
							<li><a href="">Promociones</a></li>
							<li><a href="">Capacitacion</a></li>
							<li><a href="">Sesiones Online</a></li>
							<li><a href="">Bolsa de Trabajo</a></li>
							
						</ul>
					</div>
					<div class="col-sm-12 col-md-6 col-lg-4">
						<h4>Acerca de</h4>
						<ul>
							<li><a href="">Perfiles Portal Odontologos</a></li>
							<li><a href="">Etiquetas</a></li>
							<li><a href="">Especialidades</a></li>
							<li><a href="">Prolíticas de Privacidad</a></li>
							<li><a href="">Políticas de Uso</a></li>
							<li><a href="">Términos y Condiciones</a></li>
						</ul>
					</div>
					<div class="col-sm-12 col-md-6 col-lg-4">
						<h4>Contáctanos</h4>
						<ul>
							<li><a href="">Envíanos un mail</a></li>
							<li><a href="">¿Quienes Somos?</a></li>
							<li><a href="">Servicios</a></li>
							<li><a href="">Anúnciate con Nosotros</a></li>
							<li><a href="">Mail:contacto@odontologos.mx</a></li>
							<li><a href="">Teléfono (55)3449-2468</a></li>
						</ul>
					</div>
					</div>
				</div>
				<div class="col col-md-4 col-lg-4 border-width-left margin0"><br>
					<p class="$margin10-0">Ya somos más de <strong>341,265 odontólogos conectados.</strong>Gracias por formar parte de la mayoria red de profesionales en odontología.</p>
					<p>Recibe Información en tu correo aqui</p>
					<form action="footer_submit" method="get" accept-charset="utf-8">
						<input type="text" placeholder="Tu correo Aquí" style="width: 100%;">
					</form>
				</div>
			</div>		
		</div>
	</div>
	<div class="container-fluid fondo-color-especial-3">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-6 col-lg-8">
					<small>Portal Odontológos es una marca registrada y es propiedad de Portal Odontologos S.A. de C.V. Derecho Reservados</small>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-4">
					<a href="">Desarrollado por Portal Odontólogos</a>
				</div>
			</div>
		</div>
	</div>
</footer>
