<header>
  <div class="container-fluid fondo-color-especial-3 padding0">
    <div class="container menu-top">
      <div class="row padding0">
      <div class="col-md-8 text-left">
        <ul class="list-inline texto-small">
          <!-- <li class="list-inline-item"><a class="" href="">Portal Odontológos</a></li> -->
          <li class="list-inline-item"><a class="" href="">Asistente Digital</a></li>
          <li class="list-inline-item"><a class="" href="">Denta Tips</a></li>
          <li class="list-inline-item"><a class="" href="">Inter-medio</a></li>
          <li class="list-inline-item"><a class="" href="">Odontoticket</a></li>
          <li class="list-inline-item"><a class="" href="">Sesiones On-line</a></li>
          <li class="list-inline-item"><a class="" href="">Marketplace</a></li>
        </ul>
      </div>
      <div class="col-md-4 text-right">
           <?php include ($nivel_ruta."custom/prints/social_icons.php"); ?>
      </div>
      </div>
    </div>
  </div>
  <div class="container-fluid fondo-color-especial-4">
    <div class="container menu-top">
      <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light">
          <div class="col-md-3 col-ms">
            <a class="navbar-brand" href="<?=$path;?>/index.php"><img src="<?=$nivel_ruta;?>img/native/logo-posa-bco-01.png" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          </div>
          <div class="col-md-9">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <form class="form-inline my-2 my-lg-0 float-derecha">
                <input class="form-control mr-sm-2" type="search" placeholder="Buscar" aria-label="Search">
                <ul class="list-group none-style">
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <img src="<?=$nivel_ruta;?>img/native/logo-mexico.png" alt="" style="width: 20px;"> México
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <!-- <a class="dropdown-item" href="#">México</a> -->
                      <a class="dropdown-item" href="#" style="color:black"><img src="<?=$nivel_ruta;?>img/native/logo-argentina.png" alt="" style="width: 20px;"> Argentina</a>
                    </div>
                  </li>
                </ul>
              </form>
            </div>
          </div>
        </nav>
    </div>
  </div>
  <div class="container-fluid fondo-color-especial-5" id="menu" style="z-index: 1;">
    <div class="container menu-top">
      <nav class="navbar navbar-expand-md navbar-light">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="<?=$path;?>/index.php">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?=$path;?>/directorio">Directorios</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?=$path;?>/informacion">Publicaciones</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?=$path;?>/calendario">Eventos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?=$path;?>/productos">Productos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Clasificados</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Sesiones On-Line</a>
                </li>
            </ul>
        </div>
      </nav>
    </div>
  </div>
</header>
