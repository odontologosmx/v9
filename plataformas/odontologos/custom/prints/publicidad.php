<?php
$totalrowspublicidad=count($publicidad);
if($totalrowspublicidad!=0) { ?>

  <!-- - - - - - - - - - - - - - - - - - BANNER SLIDER HOME  - - - - - - - - - - - - - - - - -  -->
  <?php if($print_view=='slider_home') { ?>

    <section class="padding0">
      <div class="owl-carousel" id="slider_home">
        <div class="item">
          <div class="container-fluid padding0">
              <div class="container-slider">
                  <img src="img/native/publicidad/billboard.jpg" alt="">
                  <div class="overlay">
                    <div class="texto-slider container">
                      <a href="#" target="_blank">
                        <h1>Lorem ipsum dolor sit amet</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                      </a>
                    </div>
                  </div>
              </div>
          </div>
        </div>
        <div class="item">
          <div class="container padding0">
              <div class="container-slider">
                  <img src="img/native/publicidad/billboard.jpg" alt="">
                  <div class="overlay">
                    <div class="texto-slider container">
                      <a href="#" target="_blank">
                        <h1>Lorem ipsum dolor sit amet</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                      </a>
                    </div>
                  </div>
              </div>
          </div>
        </div>
        <?php
        foreach ($publicidad as $publicidad) {
        include ("data_publicidad.php");
        ?>
          <!-- <div class="item">
            <div class="container padding0">
                <div class="container-slider">
                    <img src="<?=$ruta_banner;?>" alt="">
                    <div class="overlay">
                      <div class="texto-slider container">
                        <a href="<?=$link_banner;?>" target="<?=$link_target;?>">
                          <h1><?=$titulo_banner;?></h1>
                          <p><?=$descripcion_banner;?></p>
                        </a>
                      </div>
                    </div>
                </div>
            </div>
          </div> -->
      <?php } ?>
      </div>
    </section>

  <!-- - - - - - - - - - - - - - - - - - BANNER LATERAL  - - - - - - - - - - - - - - - - -  -->
<?php } else if($print_view=='side') { ?>

    <!-- <section> -->
      <div class="">
        <div class="publicidad">
          <span>PUBLICIDAD</span>
          <?php
          foreach ($publicidad as $publicidad) {
          include ("data_publicidad.php");
          ?>
          <a href="<?=$link_banner;?>" target="<?=$link_target;?>">
          <img src="<?=$ruta_banner;?>" alt="">
          </a>
        <?php } ?>
        </div>
        <hr>
      </div>
   <!--  </section> -->

  <!-- - - - - - - - - - - - - - - - - - BANNER LEADERBOARD  - - - - - - - - - - - - - - - - -  -->
  <?php } else if($print_view=='leaderboard') { ?>
  <section>
    <div class="container">
      <div class="row">
        <div class="col">
          <span style="color: #616161; font-size: .75em; background-color: #4e4e4e14; padding: 2px 15px; font-weight: 100; letter-spacing: 2px;">PUBLICIDAD</span>
          <?php
          foreach ($publicidad as $publicidad) {
          include ("data_publicidad.php");
          ?>
          <a href="<?=$link_banner;?>" target="<?=$link_target;?>">
          <img src="<?=$ruta_banner;?>" alt="">
          </a>
        <?php } ?>
        </div>
      </div>
    </div>
  </section>

  <?php } ?>
<?php } ?>
