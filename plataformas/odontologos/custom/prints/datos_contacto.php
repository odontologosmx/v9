
<span class="tamaño-iconos-perfiles">
  <?php $totalrowsdatos=count($datos_contacto); if($datos_contacto!=0) { //var_dump($datos_contacto);?>
    <?php foreach ($datos_contacto as $datos_contacto) { ?>
    <?php switch (utf8_encode($datos_contacto['medio_contacto'])) {
      case 'Email':
        //echo '<a href="mailto:'.$datos_contacto['datos_medio_contacto'].'"><i class="fas fa-envelope-square"></i></a>';
        break;
      case 'Teléfono':
        echo '<a data-toggle="tooltip" title="'.$datos_contacto['datos_medio_contacto'].'<br><span>Click para marcar</span>" href="tel:'.$objGlobal->daformatoaTels($datos_contacto['datos_medio_contacto']).'"><i class="fa fa-phone"></i></a>';
        break;
      case 'Celular':
        echo '<a data-toggle="tooltip" title="'.$datos_contacto['datos_medio_contacto'].'<br><span>Click para marcar</span>" href="tel:'.$objGlobal->daformatoaTels($datos_contacto['datos_medio_contacto']).'"><i class="fas fa-mobile-alt"></i></a>';
        break;
      case 'Facebook':
        echo '<a href="'.$datos_contacto['datos_medio_contacto'].'" target="_blank"><i class="fab fa-facebook-square"></i></a>';
        break;
      case 'Twitter':
        echo '<a href="'.$datos_contacto['datos_medio_contacto'].'" target="_blank"><i class="fab fa-twitter-square"></i></a>';
        break;
      case 'Instagram':
        echo '<a href="'.$datos_contacto['datos_medio_contacto'].'" target="_blank"><i class="fab fa-instagram"></i></a>';
        break;
      case 'Página Web':
        echo '<a href="'.$datos_contacto['datos_medio_contacto'].'" target="_blank"><i class="fas fa-link"></i></a>';
        break;
      case 'You Tube':
        echo '<a href="'.$datos_contacto['datos_medio_contacto'].'" target="_blank"><i class="fab fa-youtube"></i></a>';
        break;
    }
    ?>
    <?php } ?>
  <?php } ?>
</span>
<style>
.tooltip.bottom {
  padding: 5px 0;
  margin-top: 6px;
}
.tooltip.bottom .tooltip-arrow {
    top: 0;
    left: 50%;
    margin-left: -5px;
    border-width: 0 5px 5px;
    border-bottom-color: #61b1df;
}
.tooltip-inner {
    max-width: 250px;
    font-size: 1.5em;
    padding: 14px 38px;
    color: #2d2d2d;
    text-align: center;
    /* text-decoration: none; */
    background-color: #f9f9f9;
    border: solid 2px #61b1df;
    border-radius: 9px;
}
.tooltip span {
    font-size: .8em;
    font-weight: bold;
    margin-top: -10px;
}
</style>
