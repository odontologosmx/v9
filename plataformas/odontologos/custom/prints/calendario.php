<?php
$totalrowscalendario=count($calendario);
if($totalrowscalendario!=0) { ?>

  <!-- - - - - - - - - - - - - - - - - - VISTA LISTADO  - - - - - - - - - - - - - - - - - -->
  <?php if($print_view=='home' || $print_view=='list') { ?>


  <!-- - - - - - - - - - - - - - - - - - VISTA SECCIÓN  - - - - - - - - - - - - - - - - - -->
<?php } else if($print_view=='section') { ?>

    <section class="padding0 margin0">
      <div class="container-fluid img-seccion-interna">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <h1>Eventos</h1>
            </div>
          </div>
        </div>
      </div>
      <div class="container padding-top20">
        <div class="row">
          <div class="col-md-12">
              <ol class="breadcrumb">
                <li><small><a href="#"><i class="fas fa-home"></i> /</a></small></li>
                <li><small><a href="#"> Calendario /</a></small></li>
                <li class="active"><small>Todos</small></li>
              </ol>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-9">
            <div class="col-sm-6 col-md-12 col-lg-12">
              <form>
                <div class="form-row">
                  <div class="col-lg-4 col-md-12">
                    <a class="menu-bar" data-toggle="collapse" href="#menu1">
                      Elige una fecha
                     </a>
                  </div>
                  <div class="col-lg-4 col-md-12">
                      <label for="inputState">Clasificación</label>
                      <select id="inputState" class="form-control">
                        <option selected>Selecciona Clasificación</option>
                        <option>...</option>
                        <option>...</option>
                      </select>
                  </div>
                  <div class="col-lg-4 col-md-12 text-right">
                    <label for="inputState"></label>
                     <button  type="button" class="btn con-fondo text-uppercase margin300 no-border-circulo">Buscar Evento</button>
                  </div>
                </div>
                <!-- menu de fecha -->
                <div class="col-lg-12 fondo-color-especial-3">
                  <div class="collapse menu" id="menu1">
                    <div class="col-md-12">
                      <div class="row date-calendar">
                        <div class="col-md-6 text-center">
                          <div title="titulo-calendario" class="col-lg-12 text-white text-center">
                            <h2>2020</h2>
                          </div>
                          <div class="row text-white">
                            <div class="col desabilitado">ENE</div>
                            <div class="col avilitado">FEB</div>
                            <div class="col">MAR</div>
                          </div>
                          <div class="row text-white">
                            <div class="col">ABR</div>
                            <div class="col">MAY</div>
                            <div class="col">JUN</div>
                          </div>
                          <div class="row text-white">
                            <div class="col">JUL</div>
                            <div class="col">AGO</div>
                            <div class="col">SEP</div>
                          </div>
                          <div class="row text-white">
                            <div class="col">OCT</div>
                            <div class="col">NOV</div>
                            <div class="col">DIC</div>
                          </div>
                        </div>
                        <div class="col-md-6 text-center">
                          <div title="titulo-calendario" class="col-lg-12 text-white text-center">
                            <h2>2021</h2>
                          </div>
                          <div class="row text-white">
                            <div class="col">ENE</div>
                            <div class="col">FEB</div>
                            <div class="col">MAR</div>
                          </div>
                          <div class="row text-white">
                            <div class="col">ABR</div>
                            <div class="col">MAY</div>
                            <div class="col">JUN</div>
                          </div>
                          <div class="row text-white">
                            <div class="col">JUL</div>
                            <div class="col">AGO</div>
                            <div class="col">SEP</div>
                          </div>
                          <div class="row text-white">
                            <div class="col">OCT</div>
                            <div class="col">NOV</div>
                            <div class="col">DIC</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- menu de fecha -->
              </form>
            </div>
            <!-- INICIO DE BUCLE  -->
            <?php
              $i_calendario=0;
              foreach ($calendario as $calendario) {
              include ("data_calendario.php");
              $i_calendario++;
            ?>
            <?php if ($mes_tag!=$mes_inicio) { ?>
              <div class="">
                <div class="col-lg-12">
                    <div class="row">
                      <h3 class="titulo-proximos"><?=$mes_inicio;?> <?=$anio_inicio;?></h3>
                    </div>
                </div>
              </div>
            <?php } ?>
            <div class="col-lg-12">
              <div class="row">
                <div class="col-lg-4">
                  <img class="img-responsive" src="<?=$thumb;?>" alt="Poster <?=$titulo;?>">
                </div>
                <div class="col-lg-8">
                  <article>
                      <div class="informacion-all">
                        <h1 name="titulo-proximo" title="titulo-subseccion" class="text-left"><?=$titulo;?></h1>
                        <ul class="padding0 interna-list">
                          <li><img src="<?=$organizador_logo;?>" alt="" style="border-radius: 50%; width: 20px; height: 20px;"><?=$organizador;?></li>
                          <li><i class="fas fa-calendar-alt"></i> <?=$fecha_evento;?></li>
                          <li><i class="fas fa-map-marker-alt"></i> <?=$ubicacion;?></li>
                          <li><div class="informacion-all"><h1><i class="fas fas fa-circle blue"> </i> <?=$clasificacion;?></h1></div></li>
                        </ul>
                        <a href="<?=$link_calendario;?>"><button  type="button" class="btn con-fondo text-uppercase margin300 no-border-circulo bot-ms">Ver Evento</button></a>
                      </div>
                  </article>
                </div>
              </div>
              <hr>
            </div>
          <?php
            $mes_tag=$mes_inicio;
            }
          ?>
          <!-- FINAL DE BUCLE -->
          </div>
          <div class="col-md-3 fondo-color-especial-7">
            <?php include ($nivel_ruta."custom/aside.php"); ?>
          </div>
        </div>
    </div>
    </section>

  <!-- - - - - - - - - - - - - - - - - - VISTA INTERNA  - - - - - - - - - - - - - - - - - -->
  <?php } else if($print_view=='info') { ?>

    <section class="container-fluid padding20">
      <div class="container">
              <ol class="breadcrumb padding10">
                <li><small><a href="#"><i class="fas fa-home"></i> /</a></small></li>
                <li><small><a href="#"> Eventos /</a></small></li>
                <li class="active"><small>Todos</small></li>
              </ol>
      </div>
      <div class="container">
        <div class="row pad-left">
          <div class="col-md-9">
              <div class="padding30 fondo-color-especial-7">
                  <div class="col-sm-6 col-md-4 col-lg-12">
                    <article>
                        <div class="informacion-all">
                          <h1 name="titulo" title="titulo-subseccion"><?=$titulo;?></h1>
                                <div class="row margin0 padding0">
                                  <div class="col-lg-12 margin0 padding0">
                                    <ol class="breadcrumb">
                                      <li><small><i class="fas fa-calendar-alt"></i> <?=$fecha_evento;?> </small></li>
                                      <li><small> | </small></li>
                                      <li><small><i class="fas fa-map-marker-alt"> </i> <?=$ubicacion;?> </small></li>
                                    </ol>
                                  </div>
                                  <div class="col-lg-12 margin0 padding0 "><!--float-right-->
                                    <h1><a href="" class="text-uppercase"><?=$clasificacion;?></a></h1>
                                  </div>
                                </div>
                          </div>
                    </article>
                  </div>
                  <div class="col-sm-6 col-md-4 col-lg-12">
                    <div class="row">
                      <a href="">
                        <img  class="no-border-circulo img-responsive" title="primera" src="../../img/contents/informacion/noticias/noticias/tecnologia-salud-inter.jpg" alt="">
                      </a>
                      <br>
                      <button  type="button" class="fondo-color-especial-8 btn con-fondo text-uppercase tam-completo margin30 no-border-circulo">Link de Registro</button>
                      <!-- <h3 class="text-left">Expomed te invita a nuestra exposición líder del Sector Salud en México y Centroamérica.</h3> -->
                      <p class="text-left p-font-medio"><?=$sinopsis;?></p><br>
                      <p><strong class="text-uppercase">datos de contacto</strong></p><br>
                      <p>E-mail:<a href="mailto:expomed@ubm.com">expomed@ubm.com</a></p>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-4 col-lg-12">
                    <div class="row">
                       <div class="col-lg-12 text-center">
                          <button type="button" class="btn con-fondo btn-sm"><i class="fas fa-calendar-alt"></i> Enviar E-mail </button>
                       </div>
                       <hr>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <ul class="list-inline icon-sociales ">
                      <li class="list-inline-item color-contrario">
                        Compartir
                      </li>
                      <li class="list-inline-item color-contrario">
                        <a href="//facebook.com/odontologosmx/" class="nounderline" target="_blank">
                          <i class="fab fa-facebook"></i>                  </a>
                      </li>
                      <li class="list-inline-item color-contrario">
                        <a href="//twitter.com/odontologosmx" class="nounderline" target="_blank">
                          <i class="fab fa-twitter"></i>                  </a>
                      </li>
                      <li class="list-inline-item color-contrario">
                        <a href="contacto@odontologos.mx" class="nounderline" target="_blank">
                          <i class="far fa-envelope"></i>                  </a>
                      </li>
                    </ul>
                  </div>
                  <div class="col-lg-12">
                      <div class="row">
                            <div class="">
                              TAGS
                            </div>
                            <ul class="list-inline">
                              <li href="#">
                                <button type="button" class="btn sin_fondo">Cáncer Bucal</button>
                              </li>
                            </ul>
                      </div>
                  </div>
                  <div class="col-lg-12 border rounded fondo-color-especial-5">
                      <div class="row">
                        <div class="col-md-3">
                          <img class="rounded-circle" src="http://lorempixel.com/60/60/" alt="">
                        </div>
                        <div class="col-md-9">
                          <div class="informacion-all">
                              <a href="//portalodontologos.com.mx/sections/informacion/contenido.php?id=3219">
                                  <h2>Federación Odontologica de Buenos Aires</h2>
                              </a>
                              <p></p>
                              <ul class="list-inline icon-sociales ">
                                  <li class="list-inline-item color-contrario">
                                    Contáctanos
                                  </li>
                                  <li class="list-inline-item color-contrario">
                                    <a href="//facebook.com/odontologosmx/" class="nounderline" target="_blank">
                                      <i class="fab fa-facebook"></i></a>
                                  </li>
                                  <li class="list-inline-item color-contrario">
                                    <a href="//twitter.com/odontologosmx" class="nounderline" target="_blank">
                                      <i class="fab fa-twitter"></i></a>
                                  </li>
                                  <li class="list-inline-item color-contrario">
                                    <a href="contacto@odontologos.mx" class="nounderline" target="_blank">
                                      <i class="far fa-envelope"></i></a>
                                  </li>
                              </ul>
                                           <!-- <p><small>13 de Noviembre de 2019</small></p> -->
                            </div>
                        </div>
                      </div>
                    </div>

            </div>
          </div>
          <div class="col-md-3 fondo-color-especial-7">
            <?php include ($nivel_ruta."custom/aside.php"); ?>
          </div>
        </div>
      </div><!--termina seccion-->
    </section>

  <?php } ?>
<?php } ?>
