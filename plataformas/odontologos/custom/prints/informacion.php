<?php
$totalrowsinformacion=count($informacion);
if($totalrowsinformacion!=0) { ?>

  <!-- - - - - - - - - - - - - - - - - - VISTA HOME  - - - - - - - - - - - - - - - - - -->
  <?php if($print_view=='home') { ?>

    <section>
      <div class="container">
        <!-- <div class="row" name="titulo">
          <div class="col-md-9">
            <h1 name="titulo">Noticias</h1>
          </div>
          <div class="col-md-3">
            <?php if($print_view!='section') { ?>
                <a href="<?=$path;?>/sections/informacion/index.php">
                  <button type="button" class="btn con-fondo float-right der">Ver Todos</button>
                </a>
              <?php } ?>
          </div>
        </div> -->
      <h1 name="titulo">Noticias</h1>
      <?php if($print_view!='section') { ?>
        <a href="<?=$path;?>/sections/informacion/index.php">
          <button type="button" class="btn con-fondo float-right der">Ver Todos</button>
        </a>
      <?php } ?>
        <div class="row padding10">
         <!-- INICIO DE BUCLE  -->
         <?php
           $i_informacion=0;
           foreach ($informacion as $informacion) {
           include ("data_informacion.php");
           $i_informacion++;
         ?>
           <div class="col-sm-12 <?php if($print_view=="home" && ($i_informacion==1 || $i_informacion==2)) echo 'col-md-6 col-lg-6'; else echo "col-md-4 col-lg-4"; ?>">
             <article>
              <div class="container-informacion img-hover-zoom"><!-- style="background-image: url(<?=$imagen;?>);"-->
                 <img class="img-responsive" src="<?=$imagen;?>" alt="" >
                 <div class="overlay">
                    <div class="texto-informacion">
                     <h1><?=$tema;?></h1>
                     <a href="<?=$link_informacion;?>">
                       <h2><?=$titulo_list;?></h2>
                     </a>
                     <ul class="list-inline">
                       <li><small> <i class="fas fa-eye"></i> <?=$views_informacion;?></small> </li>
                       <li><small> <i class="fas fa-calendar-alt"> </i> <?=$fecha_publicacion;?> </small></li>
                     </ul>
                     <br>
                   </div>
                 </div>
               </div>
             </article>
           </div>
         <?php } ?>
         <!-- FINAL DE BUCLE -->
        </div>
      </div>
    </section>

    <!-- - - - - - - - - - - - - - - - - - VISTA LISTADO  - - - - - - - - - - - - - - - - - -->
  <?php } else if($print_view=='section' || $print_view=='list') { ?>

    <section class="padding0 margin0">
      <!-- <div class="col img-seccion-interna margin0">
            <div class="container">
              <h1>Publicaciones</h1>
            </div>
      </div> -->
      <div class="container-fluid img-seccion-interna">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <h1>Publicaciones</h1>
            </div>
          </div>
        </div>
      </div>
      <div class="container padding-top20">
        <div class="row">
          <div class="col-md-12">
              <ol class="breadcrumb">
                <li><small><a href="#"><i class="fas fa-home"></i> /</a></small></li>
                <li><small><a href="#"> Publicaciones /</a></small></li>
                <li class="active"><small>Todos</small></li>
              </ol>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-9">
            <?php if($print_view!='section') { ?>
                    <a href="<?=$path;?>/sections/informacion/index.php">
                      <button type="button" class="btn con-fondo float-right der">Ver Todos</button>
                    </a>
            <?php } ?>
            <div class="padding20 fondo-color-especial-7">
              <div class="col-sm-6 col-md-4 col-lg-12">
                  <div class="row">
                    <?php
                       $i_informacion=0;
                       foreach ($informacion as $informacion) {
                       include ("data_informacion.php");
                       $i_informacion++;
                     ?>
                     <div class="col-sm-6 fondo-color-especial-7 <?php if($print_view=="home" && ($i_informacion==1 || $i_informacion==2)) echo 'col-md-6 col-lg-6'; else echo "col-md-4 col-lg-6"; ?>">
                         <article>
                          <div class="container-informacion img-hover-zoom1 shadow">
                             <img class="img-responsive" src="<?=$imagen;?>" alt="">
                           </div>
                           <div class="informacion-all">
                                 <h1><?=$tema;?></h1>
                                 <a href="<?=$link_informacion;?>">
                                   <h2><?=$titulo_list;?></h2>
                                 </a>
                                 <ol class="breadcrumb">
                                   <li><small> <i class="fas fa-eye"></i> <?=$views_informacion;?></small> </li>
                                   <li><small> <i class="fas fa-calendar-alt"> </i> <?=$fecha_publicacion;?> </small></li>
                                 </ol>
                                 <p><?=$descripcion;?></p>
                            </div>
                         </article>
                         <hr>
                      </div>
                    <?php } ?>
                  </div>
                </div>
                <!-- PAGINADOR -->
                <?php
                $paginador_informacion=$obj->getInformacion($bd_site,'total',$parametro,"",$paginador,false);
                //echo count($paginador_informacion);
                if($paginador==1) {
                if (empty($_GET['p'])){
                  $p=1;
                }
                else {
                  $p=$_GET['p'];
                }
                  $num_botones=count($paginador_informacion)/$limit;
                ?>
                <ul class="pagination justify-content-center">
                  <?php if ($p>1) { ?>
                  <li style="width:35px">
                      <a class="page-link" href="?p=<?=$p-1;?>" aria-label="Previous">
                      <span aria-hidden="true"><i class="fa fa-chevron-left"></i></span>
                    </a>
                  </li>
                  <?php } ?>
                  <?php
                  for($i = $p-3; $i <= $p+3; $i++){
                      if($i==$p)
                      $class_bot='class="page-item active"';
                      if($i>=1 && $i<=$num_botones) {

                      ?>
                        <li <?=$class_bot;?> style="width:35px">
                          <a class="page-link" href="?p=<?=$i;?>"><?=$i;?></a>
                        </li>
                      <?
                      $rutafinal="";
                      }
                      $class_bot='';
                  } ?>
                  <?php if ($p<$num_botones) { ?>
                  <li style="width:35px">
                    <a class="page-link" href="?p=<?=$p+1;?>" aria-label="Next">
                      <span aria-hidden="true"><i class="fa fa-chevron-right"></i></span>
                    </a>
                  </li>
                  <?php } ?>
                </ul>
                <?php } ?>
                <!-- /PAGINADOR -->
            </div>
          </div>
          <div class="col-md-3 fondo-color-especial-7">
            <?php include ($nivel_ruta."custom/aside.php"); ?>
          </div>
        </div>
    </div>
    </section>

  <!-- - - - - - - - - - - - - - - - - - VISTA INTERNA  - - - - - - - - - - - - - - - - - -->
  <?php } else if($print_view=='info') { ?>
    <section class="container-fluid padding20">
      <div class="container">
              <ol class="breadcrumb padding10">
                <li><small><a href="#"><i class="fas fa-home"></i> /</a></small></li>
                <li><small><a href="#"> Publicaciones /</a></small></li>
                <li class="active"><small>Todos</small></li>
              </ol>
      </div>
      <div class="container">
        <div class="row pad-left">
          <div class="col-md-9">
              <img title="primera" src="<?=$imagen;?>" alt="">
              <div class="padding30 fondo-color-especial-7">
                <div class="col-sm-6 col-md-4 col-lg-12">
                    <article>
                        <div class="informacion-all">
                                <h1>Seminario</h1>
                                <h2 title="titulo-subseccion"><?=$titulo;?></h2>
                                <div class="row margin0 padding0">
                                  <div class="col-lg-6 float-left margin0 padding0">
                                    <ul class="padding0">
                                         <li>
                                            <img src="<?=$avatar;?>" alt="" style="border-radius: 50%; width: 20px; height: 20px;">
                                         </li>
                                         <li><?=$fuente;?>
                                         </li>
                                    </ul>
                                  </div>
                                  <div class="col-lg-6 margin0 padding0">
                                    <ol class="breadcrumb float-right">
                                      <li><small> <i class="fas fa-eye"></i> <?=$views_informacion;?> </small> </li>
                                      <li><small> <i class="fas fa-calendar-alt"> </i> <?=$fecha_publicacion;?> </small></li>
                                    </ol>
                                  </div>
                                </div>
                                <hr>
                                <style>
                                  .info_content img[width="900"] {
                                      display: none;
                                  }
                                </style>
                                <div class="info_content"><?=$contenido;?></div>

                        </div>
                      </article>
                  </div>
                  <div class="col-lg-12">
                    <ul class="list-inline icon-sociales ">
                      <li class="list-inline-item color-contrario">
                        Compartir
                        <!-- <a href="//portalodontologos.com.mx/productos/index.php">
                          <button type="button" class="btn con-fondo float-right der">Compartirt</button>
                        </a> -->
                      </li>
                      <li class="list-inline-item color-contrario">
                        <a href="//facebook.com/odontologosmx/" class="nounderline" target="_blank">
                          <i class="fab fa-facebook"></i>                  </a>
                      </li>
                      <li class="list-inline-item color-contrario">
                        <a href="//twitter.com/odontologosmx" class="nounderline" target="_blank">
                          <i class="fab fa-twitter"></i>                  </a>
                      </li>
                      <li class="list-inline-item color-contrario">
                        <a href="contacto@odontologos.mx" class="nounderline" target="_blank">
                          <i class="far fa-envelope"></i>                  </a>
                      </li>
                    </ul>
                  </div>
                  <div class="col-lg-12">
                      <div class="row">
                          <?php $tags=$obj->getTags($bd_site,'informacion',$id_cat_tags,$limit,$paginador,$debug);
                          if(count($tags[0])>=1) { ?>
                            <div class="">
                              TAGS
                            </div>
                          <?php foreach ($tags as $tags){
                          ?>
                          <ul class="list-inline">
                            <li href="#">
                              <button type="button" class="btn sin_fondo"><?=utf8_encode($tags['etiqueta']);?></button>
                            </li>
                          </ul>
                        <?php }
                        } ?>
                      </div>
                  </div>
                  <div class="col-lg-12 border rounded fondo-color-especial-5">
                      <div class="row">
                        <div class="col-md-3">
                          <img class="rounded-circle" src="<?=$avatar;?>" alt="">
                        </div>
                        <div class="col-md-9">
                          <div class="informacion-all">
                              <a href="//portalodontologos.com.mx/sections/informacion/contenido.php?id=3219">
                                  <h2><?=$fuente;?></h2>
                              </a>
                              <p><?=$cv_empresarial;?></p>
                              <ul class="list-inline icon-sociales ">
                                  <li class="list-inline-item color-contrario">
                                    Contáctanos
                                  </li>
                                  <li class="list-inline-item color-contrario">
                                    <a href="//facebook.com/odontologosmx/" class="nounderline" target="_blank">
                                      <i class="fab fa-facebook"></i></a>
                                  </li>
                                  <li class="list-inline-item color-contrario">
                                    <a href="//twitter.com/odontologosmx" class="nounderline" target="_blank">
                                      <i class="fab fa-twitter"></i></a>
                                  </li>
                                  <li class="list-inline-item color-contrario">
                                    <a href="contacto@odontologos.mx" class="nounderline" target="_blank">
                                      <i class="far fa-envelope"></i></a>
                                  </li>
                              </ul>
                                           <!-- <p><small>13 de Noviembre de 2019</small></p> -->
                            </div>
                        </div>
                      </div>
                    </div>

            </div>
          </div>
          <div class="col-md-3 fondo-color-especial-7">
            <?php include ($nivel_ruta."custom/aside.php"); ?>
          </div>
        </div>
      </div><!--termina seccion-->

        <div class="related">
          <div class="container"><!--fondo-color-especial-7-->
            <div class="col-md-12 fondo-color-especial-7">
              <div class="row">
                <div class="col-lg-12">
                  <h2>Publicaciones relacionadas</h2>
                </div>
                <?php
                $parametro=array("id_cat"=>$id_cat_tema, "id"=>$id);
                $debug_informacion=false;
                $informacion=$obj->getInformacion($bd_site,'related',$parametro,'3','0',$debug_informacion);
                   foreach ($informacion as $informacion) {
                   include ("data_informacion.php");
                   $i_informacion++;
                 ?>
                   <div class="col-sm-4">
                       <article>
                        <div class="container-informacion img-hover-zoom1 shadow">
                           <img class="img-responsive" src="<?=$imagen;?>" alt="">
                         </div>
                         <div class="informacion-all">
                               <h1><?=$tema;?></h1>
                               <a href="<?=$link_informacion;?>">
                                 <h2><?=$titulo_list;?></h2>
                               </a>
                               <ol class="breadcrumb">
                                 <li><small> <i class="fas fa-eye"></i> <?=$views_informacion;?></small> </li>
                                 <li><small> <i class="fas fa-calendar-alt"> </i> <?=$fecha_publicacion;?> </small></li>
                               </ol>
                               <p><?=$descripcion;?></p>
                          </div>
                       </article>
                       <hr>
                    </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
</section>
  <?php } else if($print_view=='infografia') { ?>

  <?php } else if($print_view=='infografia_interna') { ?>

  <?php } ?>
<?php } ?>
