<?php
include ("includes/inicio.php");
include ("includes/config.php");
require ("custom/configAPP.php");
include ("includes/modelo.php");
include ("includes/modeloGlobal.php");
$obj = new modelo();
$objGlobal=new modeloGlobal();
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
<?php
$id_seccion="1";
$SEO=$obj->SEO($bd_site,$id_seccion,false);
foreach($SEO as $SEO){ ?>
<title><?=utf8_encode($SEO['Titulo']); ?></title>
<meta name="description" content="<?=utf8_encode($SEO['Descripcion']); ?>" />
<meta name="keywords" content="<?=utf8_encode($SEO['Keywords']); ?>" />
<?php  } ?>
<? include ("includes/tags_styles.php"); ?>
</head>
<body>
<?php include ('custom/header.php');?>

  <!-- SLIDER BANNER HOME_1 -->
  <?php
  $posicion='5';
  $limit='10';
  $debug_publicidad=false;
  $publicidad=$obj->getPublicidad($bd_site,$posicion,$limit,$debug_publicidad);
  $print_view='slider_home';
  include ('custom/prints/publicidad.php');
  ?>

  <!-- SECCIÓN NOTICIAS -->
  <?php
  $id_cat='1';
  $parametro=array("id_cat"=>$id_cat, "parametro"=>$parametro);
  $debug_informacion=false;
  $informacion=$obj->getInformacion($bd_site,'list',$parametro,'8',$paginador,$debug_informacion);
  $print_view='home';
  include ('custom/prints/informacion.php');
  ?>

  <!-- LEADERBOARD_1 -->
  <?php
  $posicion='6';
  $limit='1';
  $debug_publicidad=false;
  $publicidad=$obj->getPublicidad($bd_site,$posicion,$limit,$debug_publicidad);
  $print_view='leaderboard';
  include ('custom/prints/publicidad.php');
  ?>

  <!-- SECCIÓN PRODUCTOS -->
  <?php
  $debug_productos=false;
  $productos=$obj->getProductos($bd_site,'list',$parametro,'4',$paginador,$debug_productos);
  $print_view='list';
  include ('custom/prints/productos.php');
  ?>

  <!-- BANNER HOME -->
  <?php
  $print_view='home';
  include ('custom/prints/publicidad.php');
  ?>

  <!-- SECCIÓN SESIONES -->
  <?php
  $print_view='list';
  include ('custom/prints/webinars.php');
  ?>

  <!-- SECCIÓN PERFILES -->
  <?php
  $debug_directorio=false;
  $directorio=$obj->getDirectorio($bd_site,'perfiles_home',$parametro,'10',$paginador,$debug_directorio);
  $print_view='perfiles_home';
  include ('custom/prints/directorio.php');
  ?>

  <!-- LEADERBOARD PRODUCTOS -->
  <?php
  $print_view='leaderboard';
  include ('custom/prints/publicidad.php');
  ?>

  <!-- CIERRE EXCLUSIVO -->
  <?php
  $print_view='personalizado';
  include ('custom/prints/extra.php');
  ?>

<?php include ('custom/footer.php');?>
<?php include ("includes/tags_functions.php"); ?>
</body>
<?php
include ("includes/fin.php");
?>
</html>
