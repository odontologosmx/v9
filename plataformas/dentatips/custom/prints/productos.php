<?php
$totalrowsproductos=count($productos);
if($productos!=0) { ?>

  <!-- - - - - - - - - - - - - - - - - - VISTA LISTADO  - - - - - - - - - - - - - - - - -  -->
  <?php if($print_view=='section' || $print_view=='list') { ?>

    <section>
      <div class="container">
          <h1 name="titulo">Productos</h1>
          <?php if($print_view!='section') { ?>
            <a href="<?=$path;?>/productos/index.php">
              <button type="button" class="btn con-fondo float-right der">Ver Todos</button>
            </a>
          <?php } ?>
          <div class="row">
            <!-- INICIO DE BUCLE  -->
            <?php foreach ($productos as $productos) { include ("data_productos.php"); ?>
              <div class="col-sm-12 col-md-6 col-lg-3">
                <a href="<?=$link_productos;?>">
                  <img src="<?=$path;?>/img/contents/productos/1.jpg" alt="">
                </a>
              </div>
            <?php } ?>
            <!-- FINAL DE BUCLE -->
          </div>
      </div>
    </section>

  <!-- - - - - - - - - - - - - - - - - - VISTA INTERNA  - - - - - - - - - - - - - - - - -  -->
  <?php } else if($print_view=='info') { ?>

    <section>
      <div class="container">
         <!-- INICIO DE BUCLE  -->
         <?php include ("data_productos.php"); ?>
           <div class="col">
             <h1><?=$titulo;?></h1>
             <p><small>Publicado: <?=$fecha_publicacion;?></small></p>
             <img class="img-responsive" src="<?=$imagen;?>" alt="">
             <p>Publicado por: <?=$fuente;?></p>
           </div>
         <!-- FINAL DE BUCLE -->
    </section>

  <?php } ?>
<?php } ?>
