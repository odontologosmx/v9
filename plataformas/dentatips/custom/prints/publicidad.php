<?php
$totalrowspublicidad=count($publicidad);
if($totalrowspublicidad!=0) { ?>

  <!-- - - - - - - - - - - - - - - - - - BANNER SLIDER HOME  - - - - - - - - - - - - - - - - -  -->
  <?php if($print_view=='slider_home') { ?>
    
    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="col-lg-10 col-lg-offset-1">
              <h1>¿Buscas un odontólogo cercano?</h1>
              <h3>¿un blanqueamiento ó limpieza dental?</h3>
              <h4>Lo que necesitas encuéntralo aqui</h4>
            </div>
            <div class="col-lg-10 col-lg-offset-1">
              <form action="publicidad_submit" method="get" accept-charset="utf-8">
                <div class="form-row">
                    <div class="col-lg-3">
                      <select id="inputState" class="form-control">
                          <option selected="">Estado</option>
                          <option>uno</option>
                          <option>dos</option>
                          <option>tres</option>
                      </select>
                    </div>
                    <div class="col-lg-3">
                      <select id="inputState" class="form-control">
                          <option selected="">Ciudad</option>
                          <option>uno</option>
                          <option>dos</option>
                          <option>tres</option>
                      </select>
                    </div>
                    <div class="col-lg-3">
                      <input type="Codigo" placeholder="Codigo portal" name="">
                    </div>
                    <div class="col">
                      <a href="">
                        <button class="btn btn-lg">
                           <i class="fa fa-pencil"></i>Buscar
                        </button>
                      </a>
                    </div>
                </div>
              </form> 
            </div>
          </div> 
        </div>
      </div>
    </section>      
    <section>
      <div class="container">
        <div class="col-md-12">
          <div class="row">
              <div class="col">
                <img src="http://lorempixel.com/100/100" alt="">
                <p class="text-center">Buscas Tu <br>Odontólogo</p>
              </div>
              <div class="col">
                <img src="http://lorempixel.com/100/100" alt="">
                <p class="text-center">Promociones</p>
              </div>
              <div class="col">
                <img src="http://lorempixel.com/100/100" alt="">
                <p class="text-center">Noticias</p>
              </div>
              <div class="col">
                <img src="http://lorempixel.com/100/100" alt="">
                <p class="text-center">Infografía</p>
              </div>
              <div class="col">
                <img src="http://lorempixel.com/100/100" alt="">
                <p class="text-center">Publicaciones</p>
              </div>
          </div>
        </div>
      </div>
    <section>
    <!-- <section class="padding0">
      <div class="owl-carousel" id="slider_home">
        <?php
        foreach ($publicidad as $publicidad) {
        include ("data_publicidad.php");
        ?>
          <div class="item">
            <div class="container padding0">
                <div class="container-slider">
                    <img src="<?=$ruta_banner;?>" alt="">
                    <div class="overlay">
                      <div class="texto-slider container">
                        <a class="text-white" href="<?=$link_banner;?>" target="<?=$link_target;?>">
                          <h1><?=$titulo_banner;?></h1>
                          <p><?=$descripcion_banner;?></p>
                        </a>
                      </div>
                    </div>
                </div>
            </div>
          </div>
      <?php } ?>
      </div>
    </section> -->

  <!-- - - - - - - - - - - - - - - - - - BANNER LATERAL  - - - - - - - - - - - - - - - - -  -->
<?php } else if($print_view=='side') { ?>

    <section>
      <div class="row">
        <div class="col">
          <span style="color: #616161; font-size: .75em; background-color: #4e4e4e14; padding: 2px 15px; font-weight: 100; letter-spacing: 2px;">PUBLICIDAD</span>
          <?php
          foreach ($publicidad as $publicidad) {
          include ("data_publicidad.php");
          ?>
          <a href="<?=$link_banner;?>" target="<?=$link_target;?>">
          <img src="<?=$ruta_banner;?>" alt="">
          </a>
        <?php } ?>
        </div>
      </div>
    </section>

  <!-- - - - - - - - - - - - - - - - - - BANNER LEADERBOARD  - - - - - - - - - - - - - - - - -  -->
  <?php } else if($print_view=='leaderboard') { ?>
  <section>
    <div class="container">
      <div class="row">
        <div class="col">
          <span style="color: #616161; font-size: .75em; background-color: #4e4e4e14; padding: 2px 15px; font-weight: 100; letter-spacing: 2px;">PUBLICIDAD</span>
          <?php
          foreach ($publicidad as $publicidad) {
          include ("data_publicidad.php");
          ?>
          <a href="<?=$link_banner;?>" target="<?=$link_target;?>">
          <img src="<?=$ruta_banner;?>" alt="">
          </a>
        <?php } ?>
        </div>
      </div>
    </div>
  </section>

  <?php } ?>
<?php } ?>
