<?php
$totalrowscalendario=count($calendario);
if($totalrowscalendario!=0) { ?>

  <!-- - - - - - - - - - - - - - - - - - VISTA LISTADO  - - - - - - - - - - - - - - - - - -->
  <?php if($print_view=='home' || $print_view=='section' || $print_view=='list') { ?>

    <section>
      <div class="container">
      <h1 name="titulo">Calendario</h1>
      <?php if($print_view!='section') { ?>
        <a href="<?=$path;?>/sections/calendario/index.php">
          <button type="button" class="btn con-fondo float-right der">Ver Todos</button>
        </a>
      <?php } ?>
        <div class="row padding10">
         <!-- INICIO DE BUCLE  -->
         <?php
           $i_calendario=0;
           foreach ($calendario as $calendario) {
           include ("data_calendario.php");
           $i_calendario++;
         ?>
         <div class="row">
           <h1><a href="<?=$link_calendario;?>"><?=$titulo;?></a></h1>
           <h3>Fecha: <?=$fecha_evento;?></h3>
           <h3>Sede: <?=$ubicacion;?></h3>
           <h3>Categoría:  <?=$clasificacion;?></h3>
           <h4>Organizador:</h4>
           <a href="<?=$permalink;?>">
             <img class="logo-empresas-list" src="<?=$organizador_logo;?>" alt="<?=$organizador;?>">
           </a>
           <a href="<?=$link_calendario;?>">
             <button class=" margin30top">Más información</button>
           </a>
         </div>
         <?php } ?>
         <!-- FINAL DE BUCLE -->
        </div>
      </div>
    </section>

  <!-- - - - - - - - - - - - - - - - - - VISTA INTERNA  - - - - - - - - - - - - - - - - - -->
  <?php } else if($print_view=='info') { ?>
    TEST

    <section>
      <div class="container">
         <?php include ("data_calendario.php"); ?>
         <div class="row padding10">
             <div class="col-sm-12 col-md-6 col-lg-9">
               <div class="row">
                 <h1><a href="<?=$link_calendario;?>"><?=$titulo;?></a></h1>
                 <h3>Fecha: <?=$fecha_evento;?></h3>
                 <h3>Sede: <?=$ubicacion;?></h3>
                 <h3>Categoría:  <?=$clasificacion;?></h3>
                 <h4>Organizador:</h4>
                 <a href="<?=$permalink;?>">
                   <img class="logo-empresas-list" src="<?=$organizador_logo;?>" alt="<?=$organizador;?>">
                 </a>
                 <a href="<?=$link_calendario;?>">
                   <button class=" margin30top">Más información</button>
                 </a>
               </div>
             </div>
             <div class="col-sm-12 col-md-6 col-lg-3">
                <?php include ($nivel_ruta."custom/aside.php"); ?>
             </div>
         </div>
       </div>
    </section>

  <?php } ?>
<?php } ?>
