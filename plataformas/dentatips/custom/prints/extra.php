<!-- - - - - - - - - - - - - - - - - - PERSONALIZADO  - - - - - - - - - - - - - - - - -  -->
<?php if($print_view=='personalizado') { ?>

  <section class="fondo-color-especial">
   <div class="container">
     <h1 name="titulo">Portal Odontólogos Coberturas</h1>
     <button type="button" class="btn con-fondo float-right der">Ver Todos</button>
     <div class="row cpberturas">
       <div class="col-sm col-md-6 col-lg-4">
         <img class="img-responsive" src="img/contents/coberturas/1.jpg" alt="">
         <div class="col fondo-color-especial-2">
           <a href="">
              <h3>colgate PerioGard <br>y OrthoGard</h3>
           </a>
         </div>
       </div>
       <div class="col-sm col-md-6 col-lg-4">
         <img class="img-responsive" src="img/contents/coberturas/1.jpg" alt="">
         <div class="col fondo-color-especial-2">
           <a href="">
              <h3>colgate PerioGard <br>y OrthoGard</h3>
           </a>
         </div>
       </div>
       <div class="col-sm col-md-6 col-lg-4">
         <img class="img-responsive" src="img/contents/coberturas/1.jpg" alt="">
         <div class="col fondo-color-especial-2">
           <a href="">
              <h3>colgate PerioGard <br>y OrthoGard</h3>
           </a>
         </div>
       </div>
      <div class="col-sm col-md-6 col-lg-4">
          <img class="img-responsive" src="img/contents/coberturas/1.jpg" alt="">
          <div class="col fondo-color-especial-2">
              <a href="">
              <h3>colgate PerioGard <br>y OrthoGard</h3>
           </a>
          </div>
      </div>
     </div>
   </div>
  </section>
  <section>
   <div class="container">
     <h1 name="titulo">Nuestras Marcas Dentales</h1>
     <button type="button" class="btn con-fondo float-right der">Ver Todos</button>
     <div class="row">
       <div class="col-sm-12 col-md-6 col-lg-3">
         <a href="#">
           <img src="img/contents/marcas/1.jpg" alt="">
         </a>
       </div>
       <div class="col-sm-12 col-md-6 col-lg-3">
         <a href="#">
           <img src="img/contents/marcas/1.jpg" alt="">
         </a>
       </div>
       <div class="col-sm-12 col-md-6 col-lg-3">
         <a href="#">
           <img src="img/contents/marcas/1.jpg" alt="">
         </a>
       </div>
       <div class="col-sm-12 col-md-6 col-lg-3">
         <a href="#">
           <img src="img/contents/marcas/1.jpg" alt="">
         </a>
       </div>
     </div>
     <div class="row">
       <div class="col-sm-12 col-md-6 col-lg-3">
         <a href="#">
           <img src="img/contents/marcas/2.jpg" alt="">
         </a>
       </div>
       <div class="col-sm-12 col-md-6 col-lg-3">
         <a href="#">
           <img src="img/contents/marcas/2.jpg" alt="">
         </a>
       </div>
       <div class="col-sm-12 col-md-6 col-lg-3">
         <a href="#">
           <img src="img/contents/marcas/2.jpg" alt="">
         </a>
       </div>
       <div class="col-sm-12 col-md-6 col-lg-3">
         <a href="#">
           <img src="img/contents/marcas/2.jpg" alt="">
         </a>
       </div>
     </div>
   </div>
   <!-- <div class="container">
     <div class="row">
       <div class="col">
         <img src="img/banner/4.jpg" alt="">
       </div>
     </div>
   </div> -->
  </section>
  <section>
   <div class="container">
     <div class="row">
       <div class="col">
         <img src="img/contents/banner/4.jpg" alt="">
       </div>
     </div>
   </div>
  </section>

<?php } else if($print_view=='info') { ?>

<?php } ?>
