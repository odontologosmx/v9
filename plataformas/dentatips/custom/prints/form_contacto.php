<form id="form">
  <input class="inputs datos" name="nombre" id="nombre" type="text" placeholder="Nombre">
  <input class="inputs margin20top datos" name="telefono" id="telefono" type="text" placeholder="Teléfono">
  <input class="inputs margin20top datos" name="email" id="email" type="text" placeholder="E-Mail">
  <textarea name="comentarios" id="comentarios" class="inputs_multiline margin20top datos" placeholder="Comentarios" ></textarea>
  <div class="g-recaptcha" data-sitekey="6LcnHVkUAAAAAIDbIceN95_Twt8OsFEHg5IG8smc" data-callback="onReturnCallback" data-theme="light"></div>
  <input name="fuente" id="fuente" type="hidden" class="datos" value="<?=$contenido_fuente;?>">
  <input name="empresa" id="empresa" type="hidden" class="datos" value="<?=$empresa;?>">
  <input name="contacto" id="contacto" type="hidden" class="datos" value="<?=$contacto?>">
  <!--ESTOS DATOS NO SE ENVIAN-->
  <input name="log_gc" id="log_gc" type="hidden" value="<?=$log_gc;?>">
  <input name="respuesta_email" id="respuesta_email" type="hidden" value="<?=$respuesta_email;?>">
  <input name="id_cliente" id="id_cliente" type="hidden" value="<?=$id_cliente;?>">
  <input name="origen_nombre" id="origen_nombre" type="hidden" value="<?=$origen_nombre;?>">
  <input name="subject" id="subject" type="hidden" value="<?=$subject;?>">
  <input name="destino" id="destino" type="hidden" value="<?=$destino;?>">
  <input name="fuente_origen" id="fuente_origen" type="hidden" value="<?=$fuente;?>">
  <input name="medio" id="medio" type="hidden" value="<?=$medio;?>">
  <input name="accion" id="accion" type="hidden" value="<?=$accion;?>">
  <input name="avance_contacto" id="avance_contacto" type="hidden" value="<?=$avance_contacto;?>">
  <input style="display:none;" name="button" type="button" class="button_submit margin20top" value="ENVIAR" onClick="enviar_email();" id="btnEnviar">
</form>
<div  class="text-center" id="respuesta_form_ok" style="display:none;">
  <h1>Gracias el mensaje se envió correctamente.</h1>
  <h2>
    Un asesor de <?=$nombre_comercial;?> te contactará en breve
    Menciona que viste la publicidad en <?=$nombre_sitio;?>
  </h2>
</div>
<div  class="text-center"  id="respuesta_form_error" style="display:none;">Lo sentimos el mensaje no se pudo enviar correctamente, intente más tarde.</div>
