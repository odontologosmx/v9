
        <ul class="list-inline icon-sociales">
              <?php foreach ($data_contact_site as $key => $value) { ?>
                <li class="list-inline-item">
                  <a href="<?=$value?>" class="nounderline" target="_blank">
                    <?php
                    switch ($key) {
                      case 'Facebook':
                        echo '<i class="fab fa-facebook-f"></i>';
                      break;
                      case 'Twitter':
                        echo '<i class="fab fa-twitter"></i>';
                      break;
                      case 'YouTube':
                        echo '<i class="fab fa-youtube"></i>';
                      break;
                      case 'Instagram':
                        echo '<i class="fab fa-instagram"></i>';
                      break;
                      case 'Pinterest':
                        echo '<i class="fab fa-pinterest-p"></i>';
                      break;
                      case 'Periscope':
                        echo '<i class="fab fa-periscope"></i>';
                      break;
                      case 'Email':
                        echo '<i class="far fa-envelope"></i>';
                      break;
                    } ?>
                  </a>
                </li>
              <?php } ?>
        </ul>
       
