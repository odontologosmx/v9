<?php
$totalrowsdirectorio=count($directorio);
if($directorio!=0) { ?>

  <!-- - - - - - - - - - - - - - - - - - VISTA PERFILES HOME  - - - - - - - - - - - - - - - - -  -->
  <?php if($print_view=='perfiles_home') { ?>

    <section>
     <div class="container">
      <h1 name="titulo">Perfiles más visitados</h1>
      <button type="button" class="btn con-fondo float-right der">Ver Todos</button>
       <div class="row">
         <!-- INICIO DE BUCLE  -->
        <?php
        $i_directorio=0;
        foreach ($directorio as $directorio) {
        include ("data_directorio.php");
        $i_directorio++;
        ?>
        <?php if(($i_directorio%5)==1) { echo '<div class="row">'; } ?>
         <div class="col"><!--col-lg-3-->
           <a href="<?=$link_directorio;?>">
             <img class="img-responsive rounded-circle" src="img/contents/sesiones-on-line/1.jpg" alt="">
           </a>
             <h4 class="text-center no-link">3M México</h4>
           <a href="<?=$link_directorio;?>">
             <p class="text-center">Ver mas</p>
           </a>
         </div>
         <?php if(($i_directorio%5)==0) { echo '</div>'; } ?>
         <?php } ?>
         <!-- FINAL DE BUCLE -->
       </div>
     </div>
    </section>

  <?php } ?>
<?php } ?>
