<?php
$totalrowsinformacion=count($informacion);
if($totalrowsinformacion!=0) { ?>

  <!-- - - - - - - - - - - - - - - - - - VISTA LISTADO  - - - - - - - - - - - - - - - - - -->
  <?php if($print_view=='home' || $print_view=='section' || $print_view=='list') { ?>

    <section>
      <div class="container">
        <!-- <div class="row" name="titulo">
          <div class="col-md-9">
            <h1 name="titulo">Noticias</h1>
          </div>
          <div class="col-md-3">
            <?php if($print_view!='section') { ?>
                <a href="<?=$path;?>/sections/informacion/index.php">
                  <button type="button" class="btn con-fondo float-right der">Ver Todos</button>
                </a>
              <?php } ?>
          </div>
        </div> -->
      <h1 name="titulo">Noticias</h1>
      <?php if($print_view!='section') { ?>
        <a href="<?=$path;?>/sections/informacion/index.php">
          <button type="button" class="btn con-fondo float-right der">Ver Todos</button>
        </a>
      <?php } ?>
        <div class="row padding10">
         <!-- INICIO DE BUCLE  -->
         <?php
           $i_informacion=0;
           foreach ($informacion as $informacion) {
           include ("data_informacion.php");
           $i_informacion++;
         ?>
           <div class="col-sm-12 <?php if($print_view=="home" && ($i_informacion==1 || $i_informacion==2)) echo 'col-md-6 col-lg-6'; else echo "col-md-4 col-lg-4"; ?>">
             <article>
              <div class="container-informacion img-hover-zoom"><!-- style="background-image: url(<?=$imagen;?>);"-->
                 <img class="img-responsive" src="<?=$imagen;?>" alt="" >
                 <div class="overlay">
                    <div class="texto-informacion">
                     <!-- <h1><?=$tema;?></h1> -->
                     <a href="<?=$link_informacion;?>">
                       <h2><?=$titulo_list;?></h2>
                     </a>
                     <p><small><?=$fecha_publicacion;?></small></p>
                   </div>
                 </div>
               </div>
             </article>
           </div>
         <?php } ?>
         <!-- FINAL DE BUCLE -->
        </div>
      </div>
    </section>

  <!-- - - - - - - - - - - - - - - - - - VISTA INTERNA  - - - - - - - - - - - - - - - - - -->
  <?php } else if($print_view=='info') { ?>

    <section class="padding0">
      <div class="container fondo-color-especial-7">
         <?php include ("data_informacion.php"); ?>
         <div class="row padding10">
           <div class="col-sm-12">
             <h1 class="texto-color-azul"><?=$titulo;?></h1>
             <p style="color:blue"><?=$especialidad;?></p>
             <p><small><?=$fecha_publicacion;?></small></p>
             <p><?=$contenido;?></p>
             <p>Publicado por: <?=$fuente;?></p>
           </div>
         </div>
       </div>
    </section>

  <?php } else if($print_view=='infografia') { ?>

  <?php } else if($print_view=='infografia_interna') { ?>

  <?php } ?>
<?php } ?>
