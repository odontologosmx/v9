<?php
$calendario['fecha_hora_inicio'];
$dia_inicio=$objGlobal->explodeDateTime($calendario['fecha_hora_inicio']);
$dia_inicio=$objGlobal->explodeFecha($dia_inicio[0],'d');
$mes_inicio=$objGlobal->explodeDateTime($calendario['fecha_hora_inicio']);
$mes_inicio=$objGlobal->explodeFecha($mes_inicio[0],'m');
$mes_inicio=$objGlobal->formatoMes($mes_inicio,'');
$anio_inicio=$objGlobal->explodeDateTime($calendario['fecha_hora_inicio']);
$anio_inicio=$objGlobal->explodeFecha($anio_inicio[0],'Y');
$dia_final=$objGlobal->explodeDateTime($calendario['fecha_hora_final']);
$dia_final=$objGlobal->explodeFecha($dia_final[0],'d');
$mes_final=$objGlobal->explodeDateTime($calendario['fecha_hora_final']);
$mes_final=$objGlobal->explodeFecha($mes_final[0],'m');
$mes_final=$objGlobal->formatoMes($mes_final,'');
$anio_final=$objGlobal->explodeDateTime($calendario['fecha_hora_final']);
$anio_final=$objGlobal->explodeFecha($anio_final[0],'Y');
$hora_inicio=$objGlobal->explodeDateTime($calendario['fecha_hora_inicio']);
$hora_inicio=$objGlobal->explodeTime($hora_inicio[1],'H').":".$objGlobal->explodeTime($hora_inicio[1],'i');
$hora_final=$objGlobal->explodeDateTime($calendario['fecha_hora_final']);
$hora_final=$objGlobal->explodeTime($hora_final[1],'H').":".$objGlobal->explodeTime($hora_final[1],'i');
if($calendario['fecha_hora_inicio']==$calendario['fecha_hora_final']) {
  $fecha_evento=$dia_inicio." de ".$mes_inicio." de ".$anio_inicio;
} else {
  if($mes_inicio==$mes_final) {
    if($dia_inicio==$dia_final) {
      $fecha_evento=$dia_inicio." de ".$mes_final." de ".$anio_final;
    } else {
      $fecha_evento=$dia_inicio." - ".$dia_final." de ".$mes_final." de ".$anio_final;
    }
  }
  else {
    $fecha_evento=$dia_inicio." de ".$mes_inicio." de ".$anio_inicio." - ".$dia_final." de ".$mes_final." de ".$anio_final;
  }
}
$titulo=utf8_encode($calendario['titulo']);
$organizador=utf8_encode($calendario['nombre_comercial']);
$id_organizador=$calendario['id_organizador'];
$organizador_logo='../img/perfiles/logos/'.$calendario['avatar'];
if ($calendario['lat']=='' && $calendario['long']=='' && $calendario['placeID']=='') {
    $ubicacion=utf8_encode($calendario['nombre_ubicacion']);
} else {
  if ($calendario["placeID"]=='') {
    $ubicacion="<a href='http://maps.google.com/maps?ll=".$calendario['lat'].",".$calendario['long']."' target='_blank'>".utf8_encode($calendario['nombre_ubicacion'])."</a>";
  } else {
    $ubicacion="<a href='https://www.google.com/maps/search/?api=1&query=".$calendario['lat'].",".$calendario['long']."&query_place_id=".$calendario['placeID']."' target='_blank'>".utf8_encode($calendario['nombre_ubicacion'])."</a>";
  }
  $ubicacion_dom=utf8_encode($calendario['calle'])." ".utf8_encode($calendario['numero'])." ".utf8_encode($calendario['colonia'])." ".utf8_encode($calendario['ciudad']).",".utf8_encode($calendario['estado']);
}
$horario=$hora_inicio." - ".$hora_final." hrs";
$sinopsis=utf8_encode($calendario['sinopsis']);
$clasificacion=utf8_encode($calendario['clasificacion_evento']);
if ($calendario['permalink']!=''){
  $permalink=$path."/".$calendario['permalink'];
} else {
  $permalink="";
}
//$link_calendario=$path."/calendario/".$calendario['ID']."/".$objGlobal->prettyUrl($calendario['titulo']);
$link_calendario=$path."/sections/calendario/evento.php?id=".$calendario['ID'];

?>
