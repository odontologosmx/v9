<?php

/*SEO y Social Media*/
$nombre_sitio='Portal Odontólogos S.A. de C.V:';
$subject_sitio='Rediseñando tu imagen';

$data_contact_site = array(
  "Facebook"=>"//facebook.com/odontologosmx/",
  "Twitter"=>"//twitter.com/odontologosmx",
  "YouTube"=>"//youtube.com/user/odontologosmx",
  "Instagram"=>"//instagram.com/odontologosmx/",
  "Pinterest"=>"//pinterest.es/odontologosmx/",
  "Periscope"=>"//periscope.tv/odontologosmx",
  "Email"=>"contacto@odontologos.mx"
);

$link_suscribe_bd="https://odontologos.us7.list-manage.com/subscribe?u=5b140bc2eac72b4028ac10f26&id=b5bb79db5f&EMAIL=&MERGE1=&button=Reg%C3%ADstrate";
$info_medio="Todo lo relacionado al medio dental";
$nombre_sitio_empresa="";
$tel_sitio="5349-2468";
$fb_page_id="";
$fb_app_id="";

/*Formularios*/
$origen_nombre=$nombre_sitio;
$origen_mail='contacto@odontologos.mx';
$destino='contacto@odontologos.mx';
$subject='Envío de Formulario de Contacto '.$nombre_sitio;
$url_logo="http://odontologos.mx/img/native/logo.png";
$bd_site='g13_dentatips';

/*Tracking Google Analytics*/
$UA_analytics='UA-11455291-7';

/*API Google Maps*/
$api_map="AIzaSyCJsRJyULiv4KITU7C596vQK_icj8bLGEQ";

/*API Google Captcha*/
$captchaSecret="6LcnHVkUAAAAANDkDhbOIpx2q5AYxYWR1v_2zkQI";

/*Link Fonts*/
$link_fonts="https://fonts.googleapis.com/css?family=Poppins:300,500,600,700&display=swap";
?>
