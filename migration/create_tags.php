<?php
include ("../includes/inicio.php");
include ("../includes/config.php");
require ("../custom/configAPP.php");
include ("../includes/modelo.php");
include ("modeloMigration.php");
include ("../includes/modeloGlobal.php");
$obj = new modelo();
$objMigration = new modeloMigration();
$objGlobal=new modeloGlobal();
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
<? include ("../includes/tags_styles.php"); ?>
</head>
<body>

<?php
//TABLA A REEMPLAZAR
$tabla_destino='informacion';
$tabla_consulta=$tabla_destino;
$campo_destino='id_cat_tags';
$campo_comparacion=array('titulo','contenido');
//TABLA CATÁLOGO A CONSULTAR
$tabla_consulta_catalogo=' s_cat_tags';
$campo_consulta_catalogo='etiqueta';
$generateTags=$objMigration->generateTags($bd_site,$tabla_destino,$campo_destino,$campo_comparacion,$tabla_consulta_catalogo,$campo_consulta_catalogo,false);

//TABLA A REEMPLAZAR
$tabla_destino='informacion';
$tabla_consulta=$tabla_destino;
$campo_destino='id_cat_especialidades';
$campo_comparacion=array('titulo','contenido');
//TABLA CATÁLOGO A CONSULTAR
$tabla_consulta_catalogo=' s_cat_especialidades';
$campo_consulta_catalogo='especialidad';
$generateEspecialidades=$objMigration->generateTags($bd_site,$tabla_destino,$campo_destino,$campo_comparacion,$tabla_consulta_catalogo,$campo_consulta_catalogo,false);
?>

<?php include ("../includes/tags_functions.php"); ?>
</body>
<?php
include ("../includes/fin.php");
?>
</html>
