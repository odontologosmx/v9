<?php
class modeloMigration{

  private function getobjGlobal(){
    return $getobjGlobal = new modeloGlobal();
  }

  public function generateTags($bd_site,$tabla_destino,$campo_destino,$campo_comparacion,$tabla_consulta_catalogo,$campo_consulta_catalogo,$debug){
    $getobjGlobal=$this->getobjGlobal();

    $id_query=$_GET['id'];
    if($id_query!=''){
      $query_consulta = "SELECT * FROM $bd_site.$tabla_destino WHERE $tabla_destino.status='1' AND $tabla_destino.ID='$id_query' ORDER BY `ID` DESC";
    } else {
      $query_consulta = "SELECT * FROM $bd_site.$tabla_destino WHERE $tabla_destino.status='1' ORDER BY `ID` DESC";
    }
    echo $query_consulta."<br><br>";
    $datos_consulta=$getobjGlobal->ejecutaConsulta($query_consulta,'selectmultiple',$debug);
    // echo "<pre>";
    // print_r($datos_consulta);
    // echo "</pre>";

    $i=0;
    foreach ($datos_consulta as $datos_consulta) {
      $contenido='';
      // echo "<pre>";
      // print_r($campo_comparacion);
      // echo "</pre>";
      for ($i=0; $i < count($campo_comparacion); $i++) {
        $i_campo=$campo_comparacion[$i];
        $contenido.=strip_tags(utf8_encode($datos_consulta[$i_campo]))." ";
      }
      $contenido=strtolower($contenido);
      $contenido=$getobjGlobal->quitacentos($contenido);
      $find_contenido = array(",","?",".","%",":",";");
      $replace_contenido = array("");
      $contenido=str_replace($find_contenido,$replace_contenido,$contenido);
      $contenido=$getobjGlobal->quitapalabras_busqueda($contenido);
      $contenido=array_unique($contenido);
      //print_r($contenido);
      $id_consulta=$datos_consulta['ID'];
      echo "<br>ID: ".$id_consulta;

      $array_tags=array();
      $array_tags_string='';

      foreach ($contenido as $contenido) {
        $contenido_word=trim($contenido, ',');
        $query_consulta_catalogo = "SELECT * FROM $bd_site.$tabla_consulta_catalogo WHERE $tabla_consulta_catalogo.status='1' ORDER BY $campo_consulta_catalogo ASC";
        $datos_consulta_catalogo=$getobjGlobal->ejecutaConsulta($query_consulta_catalogo,'selectmultiple',$debug);
        // echo "<pre>";
        // print_r($datos_consulta_catalogo);
        // echo "</pre>";
        foreach ($datos_consulta_catalogo as $datos_consulta_catalogo) {
          $tags=utf8_encode($datos_consulta_catalogo[$campo_consulta_catalogo]);
          $tags=strtolower($tags);
          $tags=$getobjGlobal->quitacentos($tags);
          similar_text($tags,$contenido_word,$percent);
            //if(preg_match('/\b' . preg_quote($tags) . '\b/i', $contenido_word) || $percent>=90) {
            if (preg_match("/\b{$tags}\b/i", $contenido_word) || $percent>=85) {
              if($percent>=85){
                echo "<br>COMPARACIÓN: ".$tags." / ".$contenido_word.": ".$percent."%";
              }
              echo "<br>ENCONTRADO: <strong>".$tags."</strong>";
              $array_tags[]=$datos_consulta_catalogo['ID'];
            }
        }
      }
      $array_tags=array_unique($array_tags);
      foreach ($array_tags as $array_tags ) {
        $array_tags_string.=$array_tags.",";
      }
      $array_tags_string=trim($array_tags_string, ',');

      if ($array_tags_string!='') {
        $query_update="UPDATE $bd_site.$tabla_destino SET $campo_destino = '$array_tags_string' WHERE $tabla_destino.`ID` = $id_consulta";
      } else {
        $query_update="UPDATE $bd_site.$tabla_destino SET $campo_destino = '0' WHERE $tabla_destino.`ID` = $id_consulta";
      }
      if($getobjGlobal->ejecutaConsulta($query_update,'update',$debug)) {
        echo "<br>ACTUALIZADO: ".$array_tags_string."<br>";
        echo $query_update."<br>";
      }
      unset($array_tags);
    }
    echo "PROCESO FINALIZADO<br>";
  }

}
?>
